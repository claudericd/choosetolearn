<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'chooseto_db');

/** MySQL database username */
define('DB_USER', 'chooseto_access');

/** MySQL database password */
define('DB_PASSWORD', '@&{V8&KbG-$M');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'NI-}l:ZA)2XJ+9[?{^tnH@& W!k.H_9u5,U,-xp@i+CjprJr3HJ].ftunsuT|)kW');
define('SECURE_AUTH_KEY',  'k;i[H|yh4q>V>aNtUBb6o[|&K?.jDj-%^-+fec}UG<[L{qiT7*v~bw/I)ot~P#B[');
define('LOGGED_IN_KEY',    '%|bMkl*g2HxVlsG4)C2:LC@>SGN?ZMj7JL]t}O!~/3qx|eCWKzd7jutPh}9,@bj&');
define('NONCE_KEY',        'BbY@>^yg#k)~1IFVAip*Ov$QaF5C8|^jchmd;M#P2Ofp)gT0}[?ZPYiRC;7Ki$wW');
define('AUTH_SALT',        '@{/#,z]H3l<9n:Fa6sY=0)e>owTsXLV2M-*=^QSz(S(~94:]S@|RGK;%i#Z-~{uS');
define('SECURE_AUTH_SALT', 'y|}K*X.vVeYHod=ZBO}7VLOt6[aoYT6]_A|d^#h^>/&6#o{qr$K%7WuG5t]rM~k|');
define('LOGGED_IN_SALT',   '38n]1,%G)UWFk+t_26-Ugz~VW`xo}:O0H[C~v0k{V?6$Xn-zxFg7x#5F#{h8I0#g');
define('NONCE_SALT',       ';]m6@LkP`OkP KX$4io1]b=d!-) %3f [+5-%9V&2[j4C7`JLd-kSqUqF*9h/PEn');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', $_GET['debug']);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
