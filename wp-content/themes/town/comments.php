<?php


// Do not delete these lines
    if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
        die ('Please do not load this page directly. Thanks!');

    if ( post_password_required() ) { ?>
        <p class="nocomments">This post is password protected. Enter the password to view comments.</p>
    <?php
        return;
    }
?>
<div>
<?php wp_link_pages( ); ?>
<!-- You can start editing here. -->


<?php if ( have_comments() ) : ?>
     <div class="entry">       

        <div class="clear"></div>
                       
        <ul id="comment-list">
        <?php wp_list_comments('callback=xiara_cust_comment'); ?>
        </ul><!--END comment-list-->
            
    </div><!--END entry-->

 <?php else : // this is displayed if there are no comments so far ?>

    <?php if ( comments_open() ) : ?>
        <!-- If comments are open, but there are no comments. -->

     <?php else : // comments are closed ?>
        <!-- If comments are closed. -->
        <p class="nocomments"><?php _e('Comments are closed.', 'wp_town'); ?></p>

    <?php endif; ?>
<?php endif; ?>


<?php if ( comments_open() ) : ?>
           <div class="entry" id="response">    
                <div class="post-widget">                    
                    <div class="post-meta">    
                        
                        <h3 class="title leave_a_comment"><?php comment_form_title( __( 'Leave a Reply', 'wp_town'), __( 'Leave a Reply to %s', 'wp_town') ); ?></h3>
                    </div><!--END POST-META-->                
                </div><!--END POST-WIDGET-->

                <a name="respond" style="position:relative; top:-150px"></a> 
                <div class="comment-form-wrapper">
                    <div class="cancel-comment-reply">
                        <small><?php cancel_comment_reply_link(); ?></small>
                    </div>
              
<?php if ( get_option('comment_registration') && !is_user_logged_in() ) : ?>
            <p> <?php _e('You must be', 'wp_town'); ?><a href="<?php echo wp_login_url( get_permalink() ); ?>"><?php _e('logged in', 'wp_town'); ?></a><?php _e('to post a comment.', 'wp_town'); ?> </p>
<?php else : ?>        


<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="comment-form" class="form">
<ul class="reply_">                  
<?php if ( is_user_logged_in() ) : ?>

<p> <?php _e('Logged in as ', 'wp_town'); ?><a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="Log out of this account"><?php _e('Log out &raquo;', 'wp_town'); ?></a></p>

<?php else : ?>
  
                
                    <li>
                        <input name="author" required="" type="text" class="<?php if ($req) echo "requiredField"; ?>" value="<?php echo esc_attr($comment_author); ?>" />
                        <label for="comment-name" class="label_comment"><strong><?php _e('Name', 'wp_town'); ?></strong> <?php if ($req) echo "<em>(<?php _e('Required', 'wp_town'); ?>)</em>"; ?></label>
                    </li>
                                        
                    <li>    
                     
                        <input name="email" type="email" class="<?php if ($req) echo "requiredField email"; ?> email" required="" value="<?php echo esc_attr($comment_author_email); ?>" />
                        <label for="comment-email" class="label_comment"><strong><?php _e('Email', 'wp_town'); ?></strong>  <?php if ($req) echo "<em>(<?php _e('Required, but not published', 'wp_town'); ?>)</em>"; ?></label>
                    </li>
                                        
                    <li>
                         
                        <input name="url" type="text" class="last" value="<?php echo esc_attr($comment_author_url); ?>"/>
                        <label for="comment-url" class="label_comment"><strong><?php _e('Website', 'wp_town'); ?></strong> (<?php _e('Opcional', 'wp_town'); ?>)</label>
                    </li>

<?php endif; ?> 
                    <li class="textarea">
                        
                        <textarea name="comment" required="" rows="10" cols="20" class="<?php if ($req) echo "requiredField"; ?>"></textarea>
                        <label for="comment-message" class="label_comment"><strong><?php _e('Your Comment', 'wp_town'); ?></strong> <?php if ($req) echo "<em>(<?php _e('Required', 'wp_town'); ?>)</em>"; ?></label>       
                    </li>
    
                                        
                    <li class="submit-button">
                        <input value="Post Comment" type="submit"  class="submit" />        
                    </li> 
               </ul>                                   

<?php comment_id_fields(); ?>

<?php do_action('comment_form', $post->ID); ?>


          </form><!-- end comment-form -->
          


<?php endif; // If registration required and not logged in ?>


<?php endif; // if you delete this the sky will fall on your head 
?>
            </div> <!-- end of comment-form-wrapper -->
        
        </div><!--END ENTRY--> 
        </div>
<?php


function xiara_cust_comment($comment, $args, $depth) {
$GLOBALS['comment'] = $comment; ?>

<li>                        
                    <div class="comment">
                    <article>
                        
                        <div class="comment-head">
                            <div class="post-info">
                                <a href="<?php comment_author_url(); ?>" class="post-avatar"><?php echo get_avatar($comment, 45); ?></a>
                                <a href="#" class="post-author"><?php comment_author(); ?></a> 
                                <time datetime="2013-10-30T18:55:25+00:00"><?php the_time('F j, Y'); ?></time>
                                <span class="ago"><?php echo human_time_diff( get_comment_time('U'), current_time('timestamp') ) . ' ago'; ?></span>
                                <div class="clear"></div>
                            </div>
                        </div>


                        <div class="comment-body">
                        <?php comment_text() ?> 

                        <a href="#" class="m-btn red-stripe replybutton"><?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))); ?></a>
                        <div class="clear"></div>
                        </div><!-- end .comment-body -->
                    </article>
                </div><!--comment-->

 <?php paginate_comments_links(); ?>  
                
<?php if ($comment->comment_approved == '0') : ?>
<p><em> <?php _e('Your comment is awaiting moderation.', 'wp_town'); ?></em></p>
<?php endif; ?>                

<?php 
}


