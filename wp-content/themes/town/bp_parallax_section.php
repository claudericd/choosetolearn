<?php 

  $background_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full'); 
												
 ?>

	<!-- *** PARALLAX START *** -->
	<div class="parallax <?php echo the_slug(); ?>"  <?php if(!empty($background_url[0])) {?> style="background:url('<?php echo $background_url[0] ?> ')  <?php }?> ">
		<div class="parallax_over">
			<div class="container">
				<div class="row">
					<div class="span12">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		</div><!--parallax_over-->
	</div>
	<!-- *** PARALLAX END*** -->
