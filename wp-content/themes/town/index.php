<?php get_header(); ?>

<?php if ( ! isset( $content_width ) ) 
    $content_width = 1170;
?>



<div class="blog_container home_">
				<div class="span8">


	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php $image_attributes = get_post_meta($post->ID, 'post_main_img', true);
$thumbnail_object_blog = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large');
$user_email = get_the_author_meta('user_email'); ?>

					<div class="blog-item home_">

						<?php if(!empty($thumbnail_object_blog[0])) {?> 


							<div class="img-container-blog" style="background-image: url(<?php echo $thumbnail_object_blog[0] ?>);">
								<div class="the-author-img">
									 <?php echo get_avatar( $user_email, $size = '50'); ?>
								</div>
							</div>


						<?php } ?>
							
							<div class="blog-boddy">
								<div class="the-title"><h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1></div>
								<p><?php the_excerpt(); ?></p>
								<div class="metas">
									<div class="the-date"><a href="#"><span class="calendar gray icon"></span><?php the_time('F j, Y'); ?></a></div>
									<div class="the-comments"><a href="#"><span class="comments gray icon"></span><?php comments_popup_link( '0', '1', '%', 'wp_comments_xiara' )?></a></div>
									<a href="<?php the_permalink(); ?>" class="read_more_small"><i class="fa_icon icon-plus"></i></a>
									<div class="clear"></div>
								</div><!--metas-->
							</div>
						</div><!--blog-item-->



		

	<?php endwhile; ?>

	<?php else : ?>

		<h2><?php _e('No post were found','wp_town') ?></h2>

	<?php endif; ?>

 </div>  <!--span-->
 <div class="clear"></div>
 

		</div><!--blog_container-->

<?php get_footer(); ?>	