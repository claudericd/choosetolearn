
<?php 
$bp_myOptions = get_option('bp_framework');
$meta = get_post_meta($post->ID,'metas',true);
$thumbnail_object = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large');

?>



<div id="<?php echo the_slug(); ?>" class="temoignages" style="<?php if(!empty($thumbnail_object[0])) {?> background:url('<?php echo $thumbnail_object[0] ?> ');  <?php }?> background-color:#94a9bc;">
	<div class="container">
		<div class="row">
        	<h1 class="page_title"><?php the_title(); ?></h1>
			<div class="temoignages-liste">
            	<a class="prev"></a>
                <a class="next"></a>
                <div class="temoignages-slider-holder">
                    <div class="temoignages-slider">
                <?php 
                
                $t = get_the_content(); 
                $t = apply_filters('the_content', $t);
                $t = explode('<p>',$t);
                
                $i = 1;
                $r = 3;
                
                foreach($t as $k => $v){
                    if(!$v){continue;}
                    $class = '';
                    if($i==1){
                        $class = "first";
                        echo '<div class="clear"></div>';
                    }
                    $v2 = preg_replace('#</p>#s','',$v);
                    echo '<p class="temoignage '.$class.'">'.$v2.'</p>';
                    if($k==count($t)-1||$i==$r){
                        //echo '</div>';
                        $i = 1;
                    }else{
                        $i++;
                    }
                }
                
                ?>
                </div>
                </div>
            </div>
		</div><!--row-->
	</div><!--container-->
</div><!--section-->






