<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

<?php $bp_myOptions = get_option('bp_framework'); ?>
<?php $separate_page = get_post_meta($post->ID, 'separate', true); ?>

<title><?php wp_title('|', true, 'right');  ?> <?php bloginfo('name') ?> | <?php bloginfo('description') ?></title>

<meta charset="<?php bloginfo( 'charset' ); ?>">

<?php if(isset($bp_myOptions['switch_responsive']) && $bp_myOptions['switch_responsive'] == '1') { ?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<?php } ?>

<link rel="stylesheet" href="<?php get_stylesheet_uri(); ?>">
<link rel="shortcut icon" href="<?php if(!empty($bp_myOptions['upload_favicon'])) {   echo $bp_myOptions['upload_favicon']; }?>">

<?php if(!empty($bp_myOptions['analytics_track'])) { 
  echo $bp_myOptions['analytics_track']; 
}
 ?>


<?php wp_head(); ?>

</head>

<body <?php body_class('blog_post'); ?>>


 
<div id="header">
  <div class="row-fluid">
    <div class="logo_container_fix">
        <?php 
            $header_image ='';
            if(!empty($bp_myOptions['upload_logo'])) { 
            $header_image = $bp_myOptions['upload_logo'];  
            }

            if ( !empty( $header_image ) ) : ?>
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>#home" id="logo"><img src="<?php echo esc_url( $header_image ); ?>" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="" /></a>  
            <?php else : ?>
             <a href="<?php echo esc_url( home_url( '/' ) ); ?>" id="logo_second">Logo</a>
        <?php endif; ?>
        </div><!--logo end-->



   <div class="main_nav_single">
            <ul>
                <li><a href="<?php echo home_url(); ?>"><?php _e('Go back home', 'wp_town'); ?></a></li>
            </ul><!--End UL--> 
          </div>

    

  </div><!--row-fluid-->
</div><!--header-->
       


  
<div class="wrapper gray">

<div class="container">

<div class="row">
<div class="divider big"></div>

<div class="span8">

<div class="article-container">

<?php 




if (have_posts()) : ?>

<div class="search-results">

<?php global $post; ?>


        <div>
        <?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>

        <?php /* If this is a category archive */ if (is_category()) { ?>
          <h4><?php _e('Archive for the', 'wp_town'); ?> &#8216;<?php single_cat_title(); ?>&#8217; <?php _e('Category', 'wp_town'); ?></h4>
  
        <?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
          <h4><?php _e('Posts Tagged', 'wp_town'); ?> &#8216;<?php single_tag_title(); ?>&#8217;</h4>
  
        <?php /* If this is a daily archive */ } elseif (is_day()) { ?>
          <h4><?php _e('Archive for', 'wp_town'); ?> <?php the_time(get_option('date_format')); ?></h4>
  
        <?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
          <h4><?php _e('Archive for', 'wp_town'); ?> <?php the_time(get_option('date_format')); ?></h4>
  
        <?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
          <h4><?php _e('Archive for', 'wp_town'); ?> <?php the_time(get_option('date_format')); ?></h4>
  
        <?php /* If this is an author archive */ } elseif (is_author()) { ?>
          <h4><?php _e('Archive Archive', 'wp_town'); ?></h4>
  
        <?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
          <h4><?php _e('Blog Archives', 'wp_town'); ?></h4>
        
        <?php } ?>
        <p class="titleSep"></p>
      </div>
      <?php
    
    ?> 
    

</div> <!-- end search-results -->

<hr class="fancy-hr" />

<?php while(have_posts()) : the_post(); ?>

<?php 

$image_attributes = get_post_meta($post->ID, 'post_main_img', true);
$bp_myOptions = get_option('bp_framework');
$thumbnail_object_blog = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large');
$user_email = get_the_author_meta('user_email');
$comments = comments_open() && get_option("default_comment_status ") == "open";

 ?>


<div class="blog-item">
    <?php if(!empty($thumbnail_object_blog[0])) {?> 


              <div class="img-container-blog" style="background-image: url(<?php echo $thumbnail_object_blog[0] ?>);">
                <div class="the-author-img">
                   <?php echo get_avatar( $user_email, $size = '50'); ?>
                </div>
              </div>


            <?php } ?>
  <div class="blog-boddy">
    <div class="the-title"><h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1></div>
    <p><?php the_excerpt(); ?></p>
    <div class="metas">
      <div class="the-date"><a href="#"><span class="calendar gray icon"></span><?php the_time('F j, Y'); ?></a></div>
      <div class="the-comments"><a href="#"><span class="comments gray icon"></span><?php comments_popup_link( '0', '1', '%', 'wp_comments_xiara' )?></a></div>
      <a href="<?php the_permalink(); ?>" class="read_more_small"><i class="fa_icon icon-plus"></i></a>
      <div class="clear"></div>
    </div><!--metas-->
  </div>
</div><!--blog-item-->







<?php endwhile; else : ?>
<div class="span12">
  <article class="no-posts">

<h1><?php _e('No posts were found.', 'wp_town'); ?></h1>

</article>
</div>

<?php endif; ?>


<div class="article-nav clearfix">

  <?php my_pagination(); ?> <?php // PAGINATION  ?>

</div> <!-- end clearfix -->

</div> <!-- end article-container -->


</div> <!-- end span8 -->

<?php get_sidebar(); ?>

</div> <!-- end row -->

</div> <!-- end container -->


</div><!--wrapper section-->

   
         


<!--FOOTER START -->
<div id="footer">
  <div class="container">
    <div class="row">
      
    <?php  
    if(!empty($bp_myOptions['footer_content'])) { ?>
         <?php echo $bp_myOptions['footer_content']; ?>
    <?php }?>
         

    <a class="map" href=" <?php  if(!empty($bp_myOptions['map_url'])) { ?>   <?php echo $bp_myOptions['map_url']; ?><?php }?>" target="_blank" title="Map">

    <?php  if(!empty($bp_myOptions['upload_map_img'])) { ?><div id="map_google"></div> <?php }?>
    
    <div class="clear"></div></a>
        
        
      
    </div>
  </div>
</div>  <!--FOOTER END -->



<div class="loading_icon"></div>

<?php wp_footer(); ?>


</body>
</html>


