<?php 

function conference_form($p,$m){
$ok = false;

foreach($m['wpcf-prix-conference'] as $ck => $cv){
	$prix = explode("\n",$cv);
	if($prix[0]+0>0){
	  $ok = true;
	}
}
if(!$ok){
  echo $prix[0];
  return;
}
	?>
    <form enctype="multipart/form-data" method="post" class="conference-form" onsubmit="return _CHOOSETOLEARN._FORM.validate(this);">
    	<input type="hidden" id="form-error" value="<?php _e('Some required fields are not valid.', 'my_theme'); ?>" />
        <input type="hidden" name="item_name_1" value="<?=$p->post_title;?>" />
        <input type="hidden" name="item_number_1" value="CONF-<?=$p->ID;?>" />
        <input type="hidden" name="langue" value="<?=ICL_LANGUAGE_CODE;?>" />
        <input type="hidden" name="invoice" value="CTL-<?=time();?>" />
        <input type="hidden" name="conference_date" value="CONF-<?=$m['wpcf-date-de-la-conference'][0];?>" />
        
        <div class="inputs">
        <input class="require" style="width:97%;" type="text" name="name" value="<?php _e('Name', 'my_theme'); ?>" alt="<?php _e('Name', 'my_theme'); ?>" onfocus="if(this.value=='<?php _e('Name', 'my_theme'); ?>')this.value=''" onblur="if(this.value=='')this.value='<?php _e('Name', 'my_theme'); ?>'"/>
        <input class="require" style="width:46%; margin-right:2%;" type="text" name="address" value="<?php _e('Address', 'my_theme'); ?>" alt="<?php _e('Address', 'my_theme'); ?>" onfocus="if(this.value=='<?php _e('Address', 'my_theme'); ?>')this.value=''" onblur="if(this.value=='')this.value='<?php _e('Address', 'my_theme'); ?>'"/>
        <input class="require" style="width:24%; margin-right:2%;" type="text" name="city" value="<?php _e('City/Province', 'my_theme'); ?>" alt="<?php _e('City/Province', 'my_theme'); ?>" onfocus="if(this.value=='<?php _e('City/Province', 'my_theme'); ?>')this.value=''" onblur="if(this.value=='')this.value='<?php _e('City/Province', 'my_theme'); ?>'"/>
        <input class="require" style="width:17%;" type="text" name="zip" value="<?php _e('Postal Code', 'my_theme'); ?>" alt="<?php _e('Postal Code', 'my_theme'); ?>" onfocus="if(this.value=='<?php _e('Postal Code', 'my_theme'); ?>')this.value=''" onblur="if(this.value=='')this.value='<?php _e('Postal Code', 'my_theme'); ?>'"/>
        <input class="require" style="width:46%; margin-right:2%;" type="text" name="phone1" value="<?php _e('Phone', 'my_theme'); ?>" alt="<?php _e('Phone', 'my_theme'); ?>" onfocus="if(this.value=='<?php _e('Phone', 'my_theme'); ?>')this.value=''" onblur="if(this.value=='')this.value='<?php _e('Phone', 'my_theme'); ?>'"/>
        <input style="width:46%;" type="text" name="phone2" value="<?php _e('Phone Work', 'my_theme'); ?>" alt="<?php _e('Phone Work', 'my_theme'); ?>" onfocus="if(this.value=='<?php _e('Phone Work', 'my_theme'); ?>')this.value=''" onblur="if(this.value=='')this.value='<?php _e('Phone Work', 'my_theme'); ?>'"/>
        <input class="require email" style="width:97%;" type="text" name="email" value="<?php _e('Email', 'my_theme'); ?>" alt="<?php _e('Email', 'my_theme'); ?>" onfocus="if(this.value=='<?php _e('Email', 'my_theme'); ?>')this.value=''" onblur="if(this.value=='')this.value='<?php _e('Email', 'my_theme'); ?>'"/>
        <input style="width:97%;" type="text" name="organisation" value="<?php _e('Organisation', 'my_theme'); ?>" alt="<?php _e('Organisation', 'my_theme'); ?>" onfocus="if(this.value=='<?php _e('Organisation', 'my_theme'); ?>')this.value=''" onblur="if(this.value=='')this.value='<?php _e('Organisation', 'my_theme'); ?>'"/>
        <input style="width:97%;" type="text" name="profession" value="<?php _e('Profession', 'my_theme'); ?>" alt="<?php _e('Profession', 'my_theme'); ?>" onfocus="if(this.value=='<?php _e('Profession', 'my_theme'); ?>')this.value=''" onblur="if(this.value=='')this.value='<?php _e('Profession', 'my_theme'); ?>'"/>
        </div>
        <div class="inputs">
        <?php foreach($m['wpcf-prix-conference'] as $ck => $cv){
				$prix = explode("\n",$cv);
				?>
				<input type="radio" name="amount_1" value="<?php echo $prix[0];?>" /> <?php echo $prix[1];?> - <?php echo $prix[0];?>$<br />
		<?php }?>
        </div>
        <div>
        <input type="checkbox" name="newsletter" value="<?php _e("Yes, please add me to Choose to Learn's mailing list.", 'my_theme'); ?>" /> <?php _e("Yes, please add me to Choose to Learn's mailing list.", 'my_theme'); ?>
        </div>
        
        <a onclick="jQuery(this).parent().submit();" class="more"><?php _e("I WANT TO REGISTER", 'my_theme'); ?></a>
    </form>
	<?php
}


$args=array(
    'post_type' => 'conferences',
    'order' => 'ASC',
    'orderby' => 'menu_order',
    'posts_per_page' => '-1',
    'tax_query' => array(
		array(
			'taxonomy' => 'category',
			'field' => 'slug',
			'terms' => (ICL_LANGUAGE_CODE=='fr'?'prochaine-conference':'next-conference')
		)
	)
);
$prochaine_conference = new WP_Query($args); 
while ($prochaine_conference->have_posts()) : $prochaine_conference->the_post();
	$prochaine_conference  = $post;
	break;
endwhile;
$meta_prochaine = get_post_custom($prochaine_conference->ID);

$args=array(
    'post_type' => 'conferences',
    'order' => 'ASC',
    'orderby' => 'menu_order',
    'posts_per_page' => '-1',
    'tax_query' => array(
		array(
			'taxonomy' => 'category',
			'field' => 'slug',
			'terms' => (ICL_LANGUAGE_CODE=='fr'?'conference-a-venir':'conference-to-come')
		)
	)
);
$conference_avenir = new WP_Query($args); 
while ($conference_avenir->have_posts()) : $conference_avenir->the_post();
	$conference_avenir  = $post;
	break;
endwhile;
$meta_avenir = get_post_custom($conference_avenir->ID);

$args=array(
    'post_type' => 'conferences',
    'order' => 'ASC',
    'orderby' => 'meta_value',
	'meta_key' => 'wpcf-date-de-la-conference',
	/*'meta_query' => array(
		array(
			'key'     => 'job_title',
			'value'   => null,
			'compare' => '!=' // Make sure the user's job title field has a value (i.e. job_title != null)
		)

	), // End of meta_query*/
    'posts_per_page' => '-1',
    'tax_query' => array(
		array(
			'taxonomy' => 'category',
			'field' => 'slug',
			'terms' => (ICL_LANGUAGE_CODE=='fr'?'conferences-precedentes':'conferences-previous')
		)
	)
);
$conference_precedente = new WP_Query($args);
global $cslug;


$args=array(
    'post_type' => 'conferences',
    'order' => 'ASC',
    'orderby' => 'menu_order',
    'posts_per_page' => '-1',
    'tax_query' => array(
		array(
			'taxonomy' => 'category',
			'field' => 'slug',
			'terms' => (ICL_LANGUAGE_CODE=='fr'?'conference-vedette':'feature-conference')
		)
	)
);
$conference_vedette = new WP_Query($args); 
while ($conference_vedette->have_posts()) : $conference_vedette->the_post();
	$conference_vedette  = $post;
	break;
endwhile;
$meta_vedette = get_post_custom($conference_vedette->ID);
?>





<div id="<?php echo $cslug;?>" class="conferences" style="background-color:#f3f3f3;">
	<div class="container">
		<div class="row">
        	<? if($conference_vedette->ID){?>
            	<? $href_vedette = $meta_vedette['wpcf-external-link'][0];?>
            	<a href="<?=($href_vedette?$href_vedette:'');?>" target="<?=($href_vedette?'_blank':'');?>" class="feature-conference">
                	<h3><?php _e('FEATURE CONFERENCE', 'my_theme'); ?></h3>
                    <div class="clear"></div>
                    <h5><?php echo $conference_vedette->post_title; ?></h5>
                    <h4><?php echo $meta_vedette['wpcf-date-de-la-conference'][0];?></h4>
                    <p>
					<?php 
					$t = $conference_vedette->post_content;
					echo $t;
					
					?>
                    </p>
                    <span class="more"><?php _e('READ MORE', 'my_theme'); ?></span>
                </a>
            <? }?>
        	<div class="conferences-boxes">
            	<? $href_next = $meta_prochaine['wpcf-external-link'][0];?>
            	<a href="<?=($href_next?$href_next:'#conferencecontent1');?>" target="<?=($href_next?'_blank':'');?>" class="conference-box prochaine-conference conference-active" id="<?=($href_next?'':'prochaine-conference');?>">
                	<h3><?php _e('NEXT<br>CONFERENCE', 'my_theme'); ?></h3>
                    <img src="/wp/wp-content/themes/town/images/icons/chapeau.png" />
                    <div class="clear"></div>
                    <p><?php echo $prochaine_conference->post_title; ?></p>
                    <h4><?php echo $meta_prochaine['wpcf-date-de-la-conference'][0];?></h4>
                    <span class="more"><?php _e('READ MORE', 'my_theme'); ?></span>
                </a>
                <? $href_come = $meta_avenir['wpcf-external-link'][0];?>
                <a href="<?=($href_come?$href_come:'#conferencecontent2');?>" target="<?=($href_come?'_blank':'');?>" class="conference-box conference-avenir" id="<?=($href_come?'':'conference-avenir');?>">
                	<h3><?php _e('CONFERENCE<br>TO COME', 'my_theme'); ?></h3>
                    <img src="/wp/wp-content/themes/town/images/icons/horn.png" />
                    <div class="clear"></div>
                    <? if($conference_avenir->ID){?>
                    <!--<?php echo $conference_avenir->ID; ?>-->
                    <p><?php echo $conference_avenir->post_title; ?></p>
                    <h4><?php echo $meta_avenir['wpcf-date-de-la-conference'][0];?></h4>
                    <span class="more"><?php _e('READ MORE', 'my_theme'); ?></span>
                    <? }else{?>
                    	<p><?php _e('NO CONFERENCE TO COME', 'my_theme'); ?></p>
                    <? }?>
                </a>
                <a href="#conferencecontent3" class="conference-box conference-archive" id="conference-archive">
                	<h3><?php _e('PREVIOUS<br>CONFERENCES', 'my_theme'); ?></h3>
                    <img src="/wp/wp-content/themes/town/images/icons/filiere.png" />
                    <div class="clear"></div>
                    <span class="more"><?php _e('READ MORE', 'my_theme'); ?></span>
                </a>
            </div>
            
		</div><!--row-->
	</div><!--container-->
    
</div><!--section-->

<div class="conferences conferences-contents-holder" style="background-color:#ffffff;">
	<div class="container">
		<div class="row">
        	<div class="conferences-contents">
            	<a name="conferencecontent1"></a>
                <a name="conferencecontent2"></a>
                <a name="conferencecontent3"></a>
            	<div class="conferences-content prochaine-conference" id="conferencecontent1">
                    <div class="conference-description conference-title">
                    	<h3><?php echo $prochaine_conference->post_title; ?></h3>
                    </div>                    
                    <div class="conference-description">
                    	<h3>DESCRIPTION</h3>
                    	<?php 
						$t = get_the_content($prochaine_conference->ID);
						$t = $prochaine_conference->post_content;
						$t = apply_filters('the_content', $t);
						echo $t;
						?>
                    </div>
                    <div class="conference-3columns conference-contenu">
                    	<h3><?php _e('CONTENT', 'my_theme'); ?></h3>
						<?php 
						$c = $meta_prochaine['wpcf-contenu-de-la-conference'][0];
						if(preg_match('/\-/',$c)){
							$c = preg_replace('/\-/','</li><li>',$c);
							$c = preg_replace('#</li>#','</p><ul>',$c,1);
							$c = '<p>'.$c.'</li></ul>';
							echo $c;
						}else{
							echo '<p>'.$c.'</p>';	
						}
						?>
                        </p>
                    </div>
                    <div class="conference-3columns conference-thematique">
                    	<h3><?php _e('THEMES', 'my_theme'); ?></h3>
						<?php 
						$c = $meta_prochaine['wpcf-thematique-de-la-conference'][0];
						if(preg_match('/\-/',$c)){
							$c = preg_replace('/\-/','</li><li>',$c);
							$c = preg_replace('#</li>#','</p><ul>',$c,1);
							$c = '<p>'.$c.'</li></ul>';
							echo $c;
						}else{
							echo '<p>'.$c.'</p>';	
						}
						?>
                        </p>
                    </div>
                    <div class="conference-3columns conference-objectifs">
                    	<h3><?php _e('GOALS', 'my_theme'); ?></h3>
						<?php 
						$c = $meta_prochaine['wpcf-objectif-de-la-conference'][0];
						if(preg_match('/\-/',$c)){
							$c = preg_replace('/\-/','</li><li>',$c);
							$c = preg_replace('#</li>#','</p><ul>',$c,1);
							$c = '<p>'.$c.'</li></ul>';
							echo $c;
						}else{
							echo '<p>'.$c.'</p>';	
						}
						?>
                        </p>
                    </div>
                    <div class="conference-date">
                    	<h3>DATE</h3>
                    	<p><?php echo nl2br($meta_prochaine['wpcf-detail-sur-la-date'][0]);?></p>
                    </div>
                    <div class="conference-lieu">
                    	<h3><?php _e('LOCATION', 'my_theme'); ?></h3>
                    	<p><?php echo nl2br($meta_prochaine['wpcf-lieu-de-la-conference'][0]);?></p>
                    </div>
                    <div class="conference-cout">
                    	<h3><?php _e('RATES', 'my_theme'); ?></h3>
                    	<p><?php echo nl2br($meta_prochaine['wpcf-cout-de-la-conference'][0]);?></p>
                    </div>
                    <div class="conference-politique">
                    	<h3><?php _e('CANCELLATION POLICY', 'my_theme'); ?></h3>
                    	<p><?php echo nl2br($meta_prochaine['wpcf-politique-dannulation'][0]);?></p>
                    </div>
                    <div class="conference-information">
                    	<h3>INFORMATION</h3>
                    	<p><?php echo nl2br($meta_prochaine['wpcf-information'][0]);?></p>
                    </div>
                    <div class="conference-inscription">
                    	<h3><?php _e('REGISTRATION', 'my_theme'); ?></h3>
                    <?php  conference_form($prochaine_conference,$meta_prochaine);?>
                    <p><a class="more" style="clear:both; float:left;" onclick="_CHOOSETOLEARN.print('.conferences-contents .prochaine-conference');"><?php _e("PRINT THIS CONFERENCE", 'my_theme'); ?></a></p>
                    </div>
                </div>
                
                <!--a venir-->
                
                <div class="conferences-content conference-avenir" id="conferencecontent2">
					<?php if($conference_avenir->ID){?>
                	<div class="conference-description conference-title">
                    	<h3><?php echo $conference_avenir->post_title; ?></h3>
                    </div>
                    <div class="conference-description">
                    	<h3>DESCRIPTION</h3>
                    	<?php 
						$t = get_the_content($conference_avenir->ID); 
						$t = $conference_avenir->post_content;
						$t = apply_filters('the_content', $t);
						echo $t;
						?>
                    </div>
                    <div class="conference-3columns conference-contenu">
                    	<h3><?php _e('CONTENT', 'my_theme'); ?></h3>
						<?php 
						$c = $meta_avenir['wpcf-contenu-de-la-conference'][0];
						if(preg_match('/\-/',$c)){
							$c = preg_replace('/\-/','</li><li>',$c);
							$c = preg_replace('#</li>#','</p><ul>',$c,1);
							$c = '<p>'.$c.'</li></ul>';
							echo $c;
						}else{
							echo '<p>'.$c.'</p>';	
						}
						?>
                        </p>
                    </div>
                    <div class="conference-3columns conference-thematique">
                    	<h3><?php _e('THEMES', 'my_theme'); ?></h3>
						<?php 
						$c = $meta_avenir['wpcf-thematique-de-la-conference'][0];
						if(preg_match('/\-/',$c)){
							$c = preg_replace('/\-/','</li><li>',$c);
							$c = preg_replace('#</li>#','</p><ul>',$c,1);
							$c = '<p>'.$c.'</li></ul>';
							echo $c;
						}else{
							echo '<p>'.$c.'</p>';	
						}
						?>
                        </p>
                    </div>
                    <div class="conference-3columns conference-objectifs">
                    	<h3><?php _e('GOALS', 'my_theme'); ?></h3>
						<?php 
						$c = $meta_avenir['wpcf-objectif-de-la-conference'][0];
						if(preg_match('/\-/',$c)){
							$c = preg_replace('/\-/','</li><li>',$c);
							$c = preg_replace('#</li>#','</p><ul>',$c,1);
							$c = '<p>'.$c.'</li></ul>';
							echo $c;
						}else{
							echo '<p>'.$c.'</p>';	
						}
						?>
                        </p>
                    </div>
                    <div class="conference-date">
                    	<h3>DATE</h3>
                    	<p><?php echo nl2br($meta_avenir['wpcf-detail-sur-la-date'][0]);?></p>
                    </div>
                    <div class="conference-lieu">
                    	<h3><?php _e('LOCATION', 'my_theme'); ?></h3>
                    	<p><?php echo nl2br($meta_avenir['wpcf-lieu-de-la-conference'][0]);?></p>
                    </div>
                    <div class="conference-cout">
                    	<h3><?php _e('RATES', 'my_theme'); ?></h3>
                    	<p><?php echo nl2br($meta_avenir['wpcf-cout-de-la-conference'][0]);?></p>
                    </div>
                    <div class="conference-politique">
                    	<h3><?php _e('CANCELLATION POLICY', 'my_theme'); ?></h3>
                    	<p><?php echo nl2br($meta_avenir['wpcf-politique-dannulation'][0]);?></p>
                    </div>
                    <div class="conference-information">
                    	<h3>INFORMATION</h3>
                    	<p><?php echo nl2br($meta_avenir['wpcf-information'][0]);?></p>
                    </div>
                    <div class="conference-inscription">
                    	<h3><?php _e('REGISTRATION', 'my_theme'); ?></h3>
                    	<?php  conference_form($conference_avenir,$meta_avenir);?>
                    	<p><a class="more" style="clear:both; float:left;" onclick="_CHOOSETOLEARN.print('.conferences-contents .conference-avenir');"><?php _e("PRINT THIS CONFERENCE", 'my_theme'); ?></a></p>
                    </div>
					<?php }else{?>
                    <div class="conference-information">
                    	<h3><?php _e('NO CONFERENCE TO COME', 'my_theme'); ?></h3>
                    	<p></p>
                    </div>
					<?php }?>
                </div>
                
                <?php
				/*
                while ($conference_precedente->have_posts()) : $conference_precedente->the_post();
					break;
				endwhile;
				$meta_avenir = get_post_custom($conference_avenir->ID);
				*/
				?>
                <div class="conferences-content conference-archive" id="conferencecontent3">
					<?php if($conference_precedente->posts){?>
                    	<?php foreach($conference_precedente->posts as $cf => $cv){?>
                			<?php $meta_precedente = get_post_custom($cv->ID);?>
                        	<h3 class="conference-archived" id="conference-archived-<?php echo $cv->ID?>"><a><span class="year"><?php echo $meta_precedente['wpcf-date-de-la-conference'][0];?></span><span class="ctitle"><?php echo $cv->post_title;?></span><span class="more"><?php _e('READ MORE', 'my_theme'); ?></span></a></h3>
                        	<div class="conference-archived-info conference-archived-<?php echo $cv->ID?>">
                            <?php 
							$t = $cv->post_content;
							$t = apply_filters('the_content', $t);
							echo $t;
							?>
                            </div>
						<?php };?>
					<?php }else{?>
                    <div class="conference-information">
                    	<h3><?php _e('NO PREVIOUS CONFERENCE', 'my_theme'); ?></h3>
                    	<p></p>
                    </div>
					<?php }?>
                </div>
                
            </div>
		</div><!--row-->
	</div><!--container-->
</div><!--section-->






