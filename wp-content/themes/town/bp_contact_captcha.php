<?php 
$bp_myOptions = get_option('bp_framework');
$meta = get_post_meta($post->ID,'metas',true);
$thumbnail_object = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large');

?>
<?php
//session_start(); // this MUST be called prior to any output including whitespaces and line breaks!

$GLOBALS['DEBUG_MODE'] = 0;
// CHANGE TO 0 TO TURN OFF DEBUG MODE
// IN DEBUG MODE, ONLY THE CAPTCHA CODE IS VALIDATED, AND NO EMAIL IS SENT
$emailTo = $bp_myOptions['contact_email'];
$GLOBALS['ct_recipient']   = $emailTo ;// Change to your email address!  Make sure DEBUG_MODE above is 0 for mail to send!
$GLOBALS['ct_msg_subject'] = 'Contact Form';
?>



<?php	if(!empty($meta['page_title'])) { ?>  
		
		<div id="<?php echo the_slug(); ?>"class="page_title">
			<div class="container">
				<div class="row">
					<div class="span12">
						<h1><?php echo $meta['page_title']; ?></h1>
					</div>
				</div><!--row-->
			</div><!--container-->
		</div><!--page_title-->

<?php }?>

<div <?php if(empty($meta['page_title'])) { ?> id="<?php echo the_slug(); ?>" <?php }?>class="section" <?php if(!empty($thumbnail_object[0])) {?> style="background:url('<?php echo $thumbnail_object[0] ?> ')  <?php }?> ">
	<div class="container">
		<div class="row">

		<h1 class="page_title"><?php the_title(); ?></h1>
		
		<?php the_content(); ?>

<?php // ***************** VALIDATION ************************************ ?>

<?php process_si_contact_form(); // Process the form, if it was submitted

if (isset($_SESSION['ctform']['error']) &&  $_SESSION['ctform']['error'] == true): /* The last form submission had 1 or more errors */ ?>

       <script>
          alert('<?php _e('There was a problem with your submission.  Errors are displayed below in red.', 'my_theme'); ?>');
       </script>


<?php elseif (isset($_SESSION['ctform']['success']) && $_SESSION['ctform']['success'] == true): /* form was processed successfully */ ?>

    <script>
        alert('<?php _e('Your message has been sent successfully, We will contact you shortly.', 'my_theme'); ?>');
        window.location.href="";
     </script>
    
<?php endif; ?>

<?php // ***************************************************** ?>




<div id="response">

<form method="post" action="<?php echo htmlspecialchars($_SERVER['REQUEST_URI'] . $_SERVER['QUERY_STRING']) ?>" id="contact_form">
  <input type="hidden" name="do" value="contact" />

			<div class="span6"> <p>
				<label for="ct_name"><?php _e('Name', 'my_theme'); ?>*:</label>
				<?php echo @$_SESSION['ctform']['name_error'] ?>
				<input type="text" required="" name="ct_name" size="35" value="<?php echo htmlspecialchars(@$_SESSION['ctform']['ct_name']) ?>" />
				</p>

				<p>
				<label for="ct_email"><?php _e('Email', 'my_theme'); ?>*:</label>
				<?php echo @$_SESSION['ctform']['email_error'] ?>
				<input type="email" required="" name="ct_email" size="35" value="<?php echo htmlspecialchars(@$_SESSION['ctform']['ct_email']) ?>" />
				</p>

				<p>
				<label for="ct_URL">URL:</label>
				<?php echo @$_SESSION['ctform']['URL_error'] ?>
				<input type="text" required="" name="ct_URL" size="35" value="<?php echo htmlspecialchars(@$_SESSION['ctform']['ct_URL']) ?>" />
				</p>

					</div>
			<div class="span6">
				
				  <p>
    <label for="ct_message">Message*:</label>
    <?php echo @$_SESSION['ctform']['message_error'] ?>
    <textarea name="ct_message" rows="12" cols="60"><?php echo htmlspecialchars(@$_SESSION['ctform']['ct_message']) ?></textarea>
  </p>


<?php // ********************** CAPTCHA FIELD ******************************* ?>
  <p>
    <?php
      // show captcha HTML using Securimage::getCaptchaHtml()
      require_once 'captcha/securimage.php';
      $options = array();
      $options['input_name'] = 'ct_captcha'; // change name of input element for form post

      if (!empty($_SESSION['ctform']['captcha_error'])) {
        // error html to show in captcha output
        $options['error_html'] = $_SESSION['ctform']['captcha_error'];
      }

      echo Securimage::getCaptchaHtml($options);
    ?>
  </p>

<?php // ***************************************************** ?>

  <p>
    <br />
    <input type="submit" value="Submit Message" class="submit_buttom"/>
  </p>


			</div><!--span6-->
			</form>
		</div>
	

			<?php  

				if(!empty($bp_myOptions['contact_sidebar'])) { echo $bp_myOptions['contact_sidebar'];}
					
			 ?>
		</div><!--row-->
		</div><!--container-->
		</div><!--section-->
















<?php

// The form processor PHP code
function process_si_contact_form()
{
  $_SESSION['ctform'] = array(); // re-initialize the form session data

  if ($_SERVER['REQUEST_METHOD'] == 'POST' && @$_POST['do'] == 'contact') {
    // if the form has been submitted

    foreach($_POST as $key => $value) {
      if (!is_array($key)) {
        // sanitize the input data
        if ($key != 'ct_message') $value = strip_tags($value);
        $_POST[$key] = htmlspecialchars(stripslashes(trim($value)));
      }
    }

    $name    = @$_POST['ct_name'];    // name from the form
    $email   = @$_POST['ct_email'];   // email from the form
    $URL     = @$_POST['ct_URL'];     // url from the form
    $message = @$_POST['ct_message']; // the message from the form
    $captcha = @$_POST['ct_captcha']; // the user's entry for the captcha code
    $name    = substr($name, 0, 64);  // limit name to 64 characters

    $errors = array();  // initialize empty error array

    if (isset($GLOBALS['DEBUG_MODE']) && $GLOBALS['DEBUG_MODE'] == false) {
      // only check for errors if the form is not in debug mode

      if (strlen($name) < 3) {
        // name too short, add error
        $errors['name_error'] = 'Your name is required';
      }

      if (strlen($email) == 0) {
        // no email address given
        $errors['email_error'] = 'Email address is required';
      } else if ( !preg_match('/^(?:[\w\d]+\.?)+@(?:(?:[\w\d]\-?)+\.)+\w{2,4}$/i', $email)) {
        // invalid email format
        $errors['email_error'] = 'Email address entered is invalid';
      }

      if (strlen($message) < 20) {
        // message length too short
        $errors['message_error'] = 'Your message must be longer than 20 characters';
      }
    }


//***********************************************************************

    // Only try to validate the captcha if the form has no errors
    // This is especially important for ajax calls
    if (sizeof($errors) == 0) {
      require_once dirname(__FILE__) . '/captcha/securimage.php';
      $securimage = new Securimage();

      if ($securimage->check($captcha) == false) {
        $errors['captcha_error'] = 'Incorrect security code entered<br />';
      }
    }
//***********************************************************************

    if (sizeof($errors) == 0) {
      // no errors, send the form
      $time       = date('r');
      $message = "A message was submitted from the contact form.  The following information was provided.<br /><br />"
                    . "<p>Name: $name</p><br />"
                    . "<p>Email: $email</p><br />"
                    . "<p>URL: $URL</p><br />"
                    . "<p>Message:</p><br />"
                    . "<p>$message</p>";

      $message = wordwrap($message, 70);

      if (isset($GLOBALS['DEBUG_MODE']) && $GLOBALS['DEBUG_MODE'] == false) {
        // send the message with mail()
        mail($GLOBALS['ct_recipient'], $GLOBALS['ct_msg_subject'], $message, "From: {$GLOBALS['ct_recipient']}\r\nReply-To: {$email}\r\nContent-type: text/html; charset=UTF-8\r\nMIME-Version: 1.0");
      }

      $_SESSION['ctform']['timetosolve'] = $securimage->getTimeToSolve();
      $_SESSION['ctform']['error'] = false;  // no error with form
      $_SESSION['ctform']['success'] = true; // message sent
    } else {
      // save the entries, this is to re-populate the form
      $_SESSION['ctform']['ct_name'] = $name;       // save name from the form submission
      $_SESSION['ctform']['ct_email'] = $email;     // save email
      $_SESSION['ctform']['ct_URL'] = $URL;         // save URL
      $_SESSION['ctform']['ct_message'] = $message; // save message

      foreach($errors as $key => $error) {
        // set up error messages to display with each field
        $_SESSION['ctform'][$key] = "<span class=\"error\">$error</span>";
      }

      $_SESSION['ctform']['error'] = true; // set error floag
    }
  } // POST
}

$_SESSION['ctform']['success'] = false; // clear success value after running
