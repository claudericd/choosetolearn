<?php
/*
Template Name: Contact Form
*/
?>

<?php $bp_myOptions = get_option('bp_framework'); ?>

<?php 
//If the form is submitted
if(isset($_POST['submitted'])) {

	//Check to see if the honeypot captcha field was filled in
	if(trim($_POST['checking']) !== '') {
		$captchaError = true;
	} else {
	
		//Check to make sure that the name field is not empty
		if(trim($_POST['contactName']) === '') {
			$nameError = _('You forgot to enter your name.', 'my_theme');
			$hasError = true;
		} else {
			$name = trim($_POST['contactName']);
		}


			$subject_ = trim($_POST['subject']);
		
		
		//Check to make sure sure that a valid email address is submitted
		if(trim($_POST['email']) === '')  {
			$emailError = _('You forgot to enter your email address.', 'my_theme');
			$hasError = true;
		} else if (!eregi("^[A-Z0-9._%-]+@[A-Z0-9._%-]+\.[A-Z]{2,4}$", trim($_POST['email']))) {
			$emailError = _('You entered an invalid email address.', 'my_theme');
			$hasError = true;
		} else {
			$email = trim($_POST['email']);
		}
			
		//Check to make sure comments were entered	
		if(trim($_POST['comments']) === '') {
			$commentError = _('You forgot to enter your comments.', 'my_theme');
			$hasError = true;
		} else {
			if(function_exists('stripslashes')) {
				$comments = stripslashes(trim($_POST['comments']));
			} else {
				$comments = trim($_POST['comments']);
			}
		}
			
		//If there is no error, send the email
		if(!isset($hasError)) {
			$bp_myOptions = get_option('bp_framework');
			$emailTo = $bp_myOptions['contact_email'];
			$subject = trim($_POST['subject']);
			$sendCopy = trim($_POST['sendCopy']);
			$body = _('Name:', 'my_theme')." $name <br>"._('Email:', 'my_theme')." $email <br>"._('Subject:', 'my_theme')." $subject <br>"._('Comments:', 'my_theme')." $comments";
			$headers = 'From: '.$name.' <'.$email.'>' . "\r\n" . 'Reply-To: ' . $email;
			
			define('DOCUMENT_ROOT',$_SERVER['DOCUMENT_ROOT']);
			include_once('php/class.mail.php');
			$MAIL = new _MAIL();
			
			$body = '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title></title>
				<style type="text/css">
				td{
					font-family:Arial, Helvetica, sans-serif;
					font-size:12px;
				}
				p{
					margin:0px;
					margin-bottom:6px;
				}
				</style>
			</head>
			<body>
			'.$body.'
			</body>
			</html>
			';
			$template = $body;
			
			$params = array();
			$params['message'] = $template;
			$params['sujet'] = $subject;
			$params['to'] = array($emailTo,'Choose To Learn');
			//$params['to'] = array('info@tsansfacon.com','Test');
			
			$b = $MAIL->send($params);
			
			//mail($emailTo, $subject, $body, $headers);

			if($sendCopy == true) {
				$subject = _('You emailed Choose to learn', 'my_theme');
				$headers = 'From: Choose to learn <'.$emailTo.'>';
				
				$params = array();
				$params['message'] = $template;
				$params['sujet'] = $subject;
				$params['to'] = array($email,$name);
				$b = $MAIL->send($params);
				//mail($email, $subject, $body, $headers);
			}

			$emailSent = true;

		}
	}
} ?>

