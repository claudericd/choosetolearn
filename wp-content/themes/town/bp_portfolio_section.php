<?php

$bp_myOptions = get_option('bp_framework');
$meta = get_post_meta($post->ID,'metas',true);
$thumbnail_object = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large');


if ( ! isset( $content_width ) ) 
$content_width = 1170;
?>
<?php $background=get_post_meta($post->ID, 'background', true); ?>



	<div id="<?php echo the_slug(); ?>" class="section" <?php if(!empty($thumbnail_object[0])) {?> style="background:url('<?php echo $thumbnail_object[0] ?> ')  <?php }?> ">
		<div class="portfolio-top"></div>
			<div class="container">
				<div class="row-fluid">
				<div class="span12">
				<?php the_content(); ?>
				</div>
			
</div> 
				</div>
				
				<div class="clear"></div>
				<div class="container">
				<div class="row-fluid">
				<div id="portfoliod">			
					<div id="portfolioAjaxwrap"> 
						<div id="portfolioAjax" class="clearfix">
							<div id="portfolioAjaxControlls" class="clearfix">
								<a id="ajax_close" href="#"><span class="outer"></span><span class="inner"></span></a>
							</div><div class="clear"></div>
							<div id="portfolioData"></div>
						</div><!--portfolioAjax-->
					</div>
				</div><!--portfoliod-->
			</div>
		</div>

				<section id="options" class="clearfix">
					<ul id="filters" class="clearfix">
							<li class="active"><a href="#" data-filter="*">All</a></li>
							<?php
							$args = array(  'post_type' => 'portfolio', ); 
							$bp_myOptions = get_option('bp_framework');
							// Get the taxonomy

							$taxonomies = array( 
    'filters',
);
							$terms = get_terms($taxonomies, $args);

							
							$term_list = '';
						

							// set a count to the amount of categories in our taxonomy
							$count = count($terms); 
							// set a count value to 0
							$i=0;
							// test if the count has any categories
							if ($count > 0) {
								// break each of the categories into individual elements
								foreach ($terms as $term) {
									// increase the count by 1
									$i++;
									// rewrite the output for each category
									$term_list .= '<li><a href="#" data-filter=".'. $term->slug .'">' . $term->name . '</a></li>';
									// if count is equal to i then output blank
									if ($count != $i)
									{
										$term_list .= '';
									}
									else 
									{
										$term_list .= '';
									}
								}
								// print out each of the categories in our new format
								echo $term_list;
							}
							?>
					</ul>
				</section>


					<div class="row-fluid">
					<div id="container_portfolio" class="clickable variable-sizes clearfix">
					
					<?php $wpbp = new WP_Query(array( 'post_type' => 'portfolio', 'posts_per_page' => $bp_myOptions['number_portfolio'], 'paged' => $paged, 'order' => 'DES' ) ); ?>

						<?php if ($wpbp->have_posts()) : while ($wpbp->have_posts()) : $wpbp->the_post(); ?>
						<?php $terms = get_the_terms( get_the_ID(), 'filters' ); ?>
					 							
						<?php 
							$meta_portfolio = get_post_meta($post->ID,'metas_portfolio',true);
							$large_image =  wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'fullsize', false, '' ); 
							$large_image = $large_image[0]; 
						?>								
						

						<div class="portfolio_item <?php  if (is_array($terms)){ foreach ($terms as $term) { echo  strtolower(preg_replace('/\s+/', '-', $term->name)). ' '; }}?>">
							<div class="loop">
								<?php if ( (function_exists('has_post_thumbnail')) && (has_post_thumbnail()) ) : ?>
								<?php $background_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large'); ?>
								
								<div class="img-container" <?php if(!empty($background_url)) ?> style="background-image:url(<?php echo $background_url[0] ?>)">

									<div class="over">
										<div class="details">
										
											<h4 class="title"><?php echo the_title();?><a class="ajax-trigger" href="<?php the_permalink(); ?>"></a></h4>
										</div>
										<?php endif; ?>	
									</div>

								</div>
							</div><!--loop-->
						</div>
								
						<?php $count++; // Increase the count by 1 ?>		
						<?php endwhile; endif; // END the Wordpress Loop ?>
			</div> <!-- portfolio container -->
			</div><!--row fluid-->
</div>
