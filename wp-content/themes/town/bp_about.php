
<?php 
$bp_myOptions = get_option('bp_framework');
$meta = get_post_meta($post->ID,'metas',true);
$thumbnail_object = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large');

?>



<div id="<?php echo the_slug(); ?>" class="about" style="background-color:#e6dfb6;">
	<div class="container">
		<div class="row">
        	<?php 
			$t = get_post_field('post_content', (ICL_LANGUAGE_CODE=='fr'?267:264));
			//get_the_content(); 
			$t = apply_filters('the_content', $t);
			echo $t;
			
			?>
        	<div class="about-pic"><img src="<?php echo $thumbnail_object[0] ?>" /></div>
			<div class="about-text">
			<?php 
			
			$t = get_the_content(); 
			$t = apply_filters('the_content', $t);
			echo $t;
			
			?>
            </div>
		</div><!--row-->
	</div><!--container-->
</div><!--section-->






