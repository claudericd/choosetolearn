<!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>

<?php $bp_myOptions = get_option('bp_framework'); 
$separate_page = '';

?>
<title><?php wp_title('|', true, 'right');  ?> <?php bloginfo('name') ?> | <?php bloginfo('description') ?></title>

<meta charset="<?php bloginfo( 'charset' ); ?>">

<?php if(isset($bp_myOptions['switch_responsive']) && $bp_myOptions['switch_responsive'] == '1') { ?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<?php } ?>

<link rel="shortcut icon" href="<?php if(!empty($bp_myOptions['upload_favicon'])) {   echo $bp_myOptions['upload_favicon']; }?>">

<?php  

if(!empty($bp_myOptions['analytics_track'])) { 
  echo $bp_myOptions['analytics_track']; 
}

?>

<?php wp_head(); ?>
<script type="text/javascript" src="/wp/wp-content/themes/town/js/choosetolearn.js"></script>
</head>

<body <?php body_class('bg_skin'); ?>>

<div id="top"></div>


<div id="header">
<div class="responsive_nav white_lines">
  <div class="container">
    <div class="row">
      <div class="span12">
        <div class="mobileAreaMenu">
           <?php
          if ( has_nav_menu( 'town_nav_menu' ) ) {  ?>

            <?php wp_nav_menu( array( 'theme_location' => 'town_nav_menu', 'menu_class' => 'nav-menu' ) ); ?>

          <?php
          }

          else { ?>


          <ul>
            <?php
            if (is_front_page()) $link_prefix = "#"; else $link_prefix = get_option("siteurl")."#";

            $page_args = array('sort_order' => 'ASC', 'sort_column' => 'menu_order');
            $pages = get_pages($page_args); 
            $k = 0;
            foreach ($pages as $pagess) {
                  $separate_page = get_post_meta($pagess->ID, "menu", true);
                  $k++;
                  if ($separate_page != "hide")
                  {
                    if ($k == 1) $town_nav = '<li><a href="' . $link_prefix . 'home">' . $pagess->post_title . '</a></li> '; 
                    else  $town_nav = '<li><a href="' . $link_prefix . $pagess->post_name . '">' . $pagess->post_title . '</a></li> '; 
                  }
                  else
                  {
                      continue;
                  }
                  echo $town_nav;
            }
            ?>          
        </ul><!--END UL-->

          <?php  
          }
          ?>
      </div><!--mobileAreaMenu-->
      </div><!--span12-->
    </div><!--row-->
  </div>      
</div><!--responsive_nav-->


  <div class="row-fluid">
    <div class="logo_container">
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>#home" id="logo"><img src="/wp/wp-content/themes/town/images/logo.<?php echo ICL_LANGUAGE_CODE;?>.jpg" alt="Choose to learn" /></a> 
        </div><!--logo end-->
    <a href="#responsive_nav" class="menu_trigger"><i class="fa_icon icon-reorder icon-2x"></i></a>

    <div class="main_nav">
          <?php
          if ( has_nav_menu( 'town_nav_menu' ) ) {  ?>

            <?php wp_nav_menu( array( 'theme_location' => 'town_nav_menu', 'menu_class' => 'nav-menu' ) ); ?>

          <?php
          }

          else { ?>


          <ul>
            <?php
            if (is_front_page()) $link_prefix = "#"; else $link_prefix = get_option("siteurl")."#";

            $page_args = array('sort_order' => 'ASC', 'sort_column' => 'menu_order');
            $pages = get_pages($page_args); 
            $k = 0;
            foreach ($pages as $pagess) {
                  $separate_page = get_post_meta($pagess->ID, "menu", true);
                  $k++;
                  if ($separate_page != "hide")
                  {
                    if ($k == 1) $town_nav = '<li><a href="' . $link_prefix . 'home">' . $pagess->post_title . '</a></li> '; 
                    else  $town_nav = '<li><a href="' . $link_prefix . $pagess->post_name . '">' . $pagess->post_title . '</a></li> '; 
                  }
                  else
                  {
                      continue;
                  }
                  echo $town_nav;
            }
            ?>          
        </ul><!--END UL-->

          <?php  
          }
          ?>
          </div>

    <div class="clear"></div>
  </div><!--row-fluid-->
</div><!--header-->