<?php

if ( have_posts() ) while ( have_posts() ) : the_post();
 
$image_attributes = get_post_meta($post->ID, 'post_main_img', true);
$bp_myOptions = get_option('bp_framework');
$thumbnail_object_blog = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large');
$user_email = get_the_author_meta('user_email');
$comments = comments_open() && get_option("default_comment_status ") == "open";

?>

<!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>

<?php $bp_myOptions = get_option('bp_framework'); ?>
<?php $separate_page = get_post_meta($post->ID, 'separate', true); ?>

<title><?php wp_title('|', true, 'right');  ?> <?php bloginfo('name') ?> | <?php bloginfo('description') ?></title>

<meta charset="<?php bloginfo( 'charset' ); ?>">

<?php if(isset($bp_myOptions['switch_responsive']) && $bp_myOptions['switch_responsive'] == '1') { ?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<?php } ?>
<link rel="stylesheet" href="<?php get_stylesheet_uri(); ?>">
<link rel="shortcut icon" href="<?php if(!empty($bp_myOptions['upload_favicon'])) {   echo $bp_myOptions['upload_favicon']; }?>">

<?php if(!empty($bp_myOptions['analytics_track'])) { 
  echo $bp_myOptions['analytics_track']; 
}
 ?>


<?php wp_head(); ?>

</head>

<body <?php body_class('blog_post'); ?>>



<div id="header">

  <div class="row-fluid">
    <div class="logo_container_fix">

        <?php 
            $header_image ='';
            if(!empty($bp_myOptions['upload_logo'])) { 
            $header_image = $bp_myOptions['upload_logo'];  
            }

            if ( !empty( $header_image ) ) : ?>
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>#home" id="logo"><img src="<?php echo esc_url( $header_image ); ?>" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="" /></a>  
            <?php else : ?>
             <a href="<?php echo esc_url( home_url( '/' ) ); ?>" id="logo_second">Logo</a>
        <?php endif; ?>
        </div><!--logo end-->



   <div class="main_nav_single">
            <ul>
                <li><a href="<?php echo home_url(); ?>"><?php _e('Go back home', 'wp_town'); ?></a></li>
            </ul><!--End UL--> 
          </div>

    

  </div><!--row-fluid-->
</div><!--header-->

<div class="wrapper section gray">


        <div class="container">
          <div class="row">
        <div class="divider"></div>
        <div class="span12">
            <h1 class="page_title"><strong><?php if(!empty($bp_myOptions['blog_title'])) {   echo $bp_myOptions['blog_title']; } ?></strong></h1>
          <h2 class="page_subtitle"><?php if(!empty($bp_myOptions['blog_subtitle'])) {   echo $bp_myOptions['blog_subtitle']; } ?></h2>
          <div class="page_line"></div>
        </div><!--span12-->
      
        <div class="clear"></div>
        <div class="divider"></div>
        
        
          <div class="span8">


            
<?php get_template_part('content', get_post_format()); ?>



<section id="comments">

      <h3 class="blogpost-title"><?php comments_number( 'No comments yet', '1 comment', '% comments')?></h3>

      <div class="comments-list">


      
       <?php if ($comments) comments_template(); ?>  
 
      </div><!--comment list-->
    </section>

        
          </div><!--span8-->

          <?php get_sidebar(); ?>

      </div><!--row-->
            
        </div><!--container-->


   
<?php
endwhile;
?>            

</div><!--wrapper-->

<?php wp_link_pages( array( 'before' => '<div class="page-link">Pages:', 'after' => '</div>', 'pagelink' => 'Source %' ) ); ?>
                 

<!--FOOTER START -->
<div id="footer">
  <div class="container">
    <div class="row">
      
    <?php  
    if(!empty($bp_myOptions['footer_content'])) { ?>
         <?php echo $bp_myOptions['footer_content']; ?>
    <?php }?>
         

    <a class="map" href=" <?php  if(!empty($bp_myOptions['map_url'])) { ?>   <?php echo $bp_myOptions['map_url']; ?><?php }?>" target="_blank" title="Map">

    <?php  if(!empty($bp_myOptions['upload_map_img'])) { ?><div id="map_google"></div> <?php }?>
    
    <div class="clear"></div></a>
        
        
      
    </div>
  </div>
</div>  <!--FOOTER END -->



<div class="loading_icon"></div>

<?php wp_footer(); ?>



</body>
</html>