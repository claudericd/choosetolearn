<?php

if ( have_posts() ) while ( have_posts() ) : the_post();

$image_attributes = get_post_meta($post->ID, 'post_main_img', true);
$bp_myOptions = get_option('bp_framework');
$thumbnail_object_blog = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large');
$user_email = get_the_author_meta('user_email');
$comments = comments_open() && get_option("default_comment_status ") == "open";
?>

<!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>

<?php $bp_myOptions = get_option('bp_framework'); ?>
<?php $separate_page = get_post_meta($post->ID, 'separate', true); ?>

<title><?php wp_title('|', true, 'right');  ?> <?php bloginfo('name') ?> | <?php bloginfo('description') ?></title>

<meta charset="<?php bloginfo( 'charset' ); ?>">

<?php if(isset($bp_myOptions['switch_responsive']) && $bp_myOptions['switch_responsive'] == '1') { ?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<?php } ?>

<link rel="stylesheet" href="<?php get_stylesheet_uri(); ?>">
<link rel="shortcut icon" href="<?php echo $bp_myOptions['upload_favicon']; ?>">

<?php if(!empty($bp_myOptions['analytics_track'])) { 
  echo $bp_myOptions['analytics_track']; 
}
 ?>


<?php wp_head(); ?>

</head>

<body <?php body_class('blog_post'); ?>>



<div id="header">

  <div class="row-fluid">
    <div class="logo_container_fix">

        <?php 
            $header_image ='';
            if(!empty($bp_myOptions['upload_logo'])) { 
            $header_image = $bp_myOptions['upload_logo'];  
            }

            if ( !empty( $header_image ) ) : ?>
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>#home" id="logo"><img src="<?php echo esc_url( $header_image ); ?>" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="" /></a>  
            <?php else : ?>
             <a href="<?php echo esc_url( home_url( '/' ) ); ?>" id="logo_second">Logo</a>
        <?php endif; ?>
        </div><!--logo end-->



   <div class="main_nav_single">
            <ul>
                <li><a href="<?php echo home_url(); ?>"><?php _e('Go back home', 'wp_town'); ?></a></li>
            </ul><!--End UL--> 
          </div>

    

  </div><!--row-fluid-->
</div><!--header-->


<div class="section">
    <div class="container">
    <div class="item-data">
    <div class="helper">

    <div class="row-fluid">
      
     <?php the_content(); ?> 
        
    </div><!--item data--> 

  </div><!--helper-->
    </div><!--item-data-->
      
  </div>
  </div><!--section-->

<!--FOOTER START -->
<div id="footer">
  <div class="container">
    <div class="row">
      
    <?php  
    if(!empty($bp_myOptions['footer_content'])) { ?>
         <?php echo $bp_myOptions['footer_content']; ?>
    <?php }?>
         

    <a class="map" href=" <?php  if(!empty($bp_myOptions['map_url'])) { ?>   <?php echo $bp_myOptions['map_url']; ?><?php }?>" target="_blank" title="Map">

    <?php  if(!empty($bp_myOptions['upload_map_img'])) { ?><div id="map_google"></div> <?php }?>
    
    <div class="clear"></div></a>
        
        
      
    </div>
  </div>
</div>  <!--FOOTER END -->



<div class="loading_icon"></div>

<?php wp_footer(); ?>



</body>
</html>


<?php
endwhile;
?>   