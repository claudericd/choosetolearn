<div class="my_meta_control">
     
    <p>Here you can select the type of your page. If you want to make a parallax section choose <strong>parallax</strong>, if you page is going to be a portfolio page select <strong>portfolio</strong>.</p>
 
    <p>
        <input style="width:50%;" type="text" name="metas[name]" value="<?php if(!empty($meta['name'])) echo $meta['name']; ?>"/>
        <span style="margin-left:10px;"><strong>Available values:</strong> <code>welcome, parallax, home, home_video, blog, blog_full_page, portfolio, contact</code></span>
    </p>


</div>