<div class="my_meta_control">
     
    <p>Here you can select the type of your portfolio item. There are <strong>Three types</strong>, each one with different sizes. Select between the ones below. <span style="font-weight:bold;">Leave this field empty for a default size. </span></p>

 
    <p>
        <input style="width:50%;" type="text" name="metas_portfolio[type]" value="<?php if(!empty($meta_portfolio['type'])) echo $meta_portfolio['type']; ?>"/>
        <span style="margin-left:10px;"><strong>Available portfolio values:</strong> <code>big, vertical, horizontal</code></span>
    </p>

</div>