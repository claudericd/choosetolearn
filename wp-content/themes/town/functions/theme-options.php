<?php
/*
 *
 * Set the text domain for the theme or plugin.
 *
 */
define('Redux_TEXT_DOMAIN', 'wp_town');

$bp_myOptions = get_option('bp_framework');
/*
 *
 * Require the framework class before doing anything else, so we can use the defined URLs and directories.
 * If you are running on Windows you may have URL problems which can be fixed by defining the framework url first.
 *
 */
//define('Redux_OPTIONS_URL', site_url('path the options folder'));
if(!class_exists('Redux_Options')){
    require_once(dirname(__FILE__) . '/options/defaults.php');
}

/*
 *
 * Custom function for filtering the sections array. Good for child themes to override or add to the sections.
 * Simply include this function in the child themes functions.php file.
 *
 * NOTE: the defined constansts for URLs, and directories will NOT be available at this point in a child theme,
 * so you must use get_template_directory_uri() if you want to use any of the built in icons
 *
 */
function add_another_section($sections){
    //$sections = array();
    $sections[] = array(
        'title' => __('A Section added by hook', Redux_TEXT_DOMAIN),
        'desc' => __('<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', Redux_TEXT_DOMAIN),
        'icon' => 'paper-clip',
        'icon_class' => 'icon-large',
        // Leave this as a blank section, no options just some intro text set above.
        'fields' => array()
    );

    return $sections;
}
//add_filter('redux-opts-sections-twenty_eleven', 'add_another_section');


/*
 * 
 * Custom function for filtering the args array given by a theme, good for child themes to override or add to the args array.
 *
 */
function change_framework_args($args){
    //$args['dev_mode'] = false;
    
    return $args;
}
//add_filter('redux-opts-args-twenty_eleven', 'change_framework_args');


/*
 *
 * Most of your editing will be done in this section.
 *
 * Here you can override default values, uncomment args and change their values.
 * No $args are required, but they can be over ridden if needed.
 *
 */
function setup_framework_options(){
    $args = array();

    // Setting dev mode to true allows you to view the class settings/info in the panel.
    // Default: true
    $args['dev_mode'] = false;

    // Set the icon for the dev mode tab.
    // If $args['icon_type'] = 'image', this should be the path to the icon.
    // If $args['icon_type'] = 'iconfont', this should be the icon name.
    // Default: info-sign
    $args['dev_mode_icon'] = Redux_OPTIONS_URL . 'img/icons/dev_mode.png';;

    // Set the class for the dev mode tab icon.
    // This is ignored unless $args['icon_type'] = 'iconfont'
    // Default: null
    $args['dev_mode_icon_class'] = 'icon-large';


    // If you want to use Google Webfonts, you MUST define the api key.
    //$args['google_api_key'] = 'xxxx';

    // Define the starting tab for the option panel.
    // Default: '0';
    //$args['last_tab'] = '0';

    // Define the option panel stylesheet. Options are 'standard', 'custom', and 'none'
    // If only minor tweaks are needed, set to 'custom' and override the necessary styles through the included custom.css stylesheet.
    // If replacing the stylesheet, set to 'none' and don't forget to enqueue another stylesheet!
    // Default: 'standard'
    //$args['admin_stylesheet'] = 'standard';

    // Add HTML before the form.
    $args['intro_text'] = __('<p>This text is displayed above the options panel. It isn\'t required, but more info is always better! The intro_text field accepts all HTML.</p>', Redux_TEXT_DOMAIN);

    // Add content after the form.
    //$args['footer_text'] = __('<p>This text is displayed below the options panel. It isn\'t required, but more info is always better! The footer_text field accepts all HTML.</p>', Redux_TEXT_DOMAIN);

    // Set footer/credit line.
    //$args['footer_credit'] = __('<p>This text is displayed in the options panel footer across from the WordPress version (where it normally says \'Thank you for creating with WordPress\'). This field accepts all HTML.</p>', Redux_TEXT_DOMAIN);

    // Setup custom links in the footer for share icons
    $args['share_icons']['twitter'] = array(
        'link' => 'http://twitter.com/bypixels',
        'title' => 'Follow me on Twitter', 
        'img' => Redux_OPTIONS_URL . 'img/social/Twitter.png'
    );
    $args['share_icons']['facebook'] = array(
        'link' => 'http://www.facebook.com/bypixels',
        'title' => 'Find me on Facebook', 
        'img' => Redux_OPTIONS_URL . 'img/social/Facebook.png'
    );

    // Enable the import/export feature.
    // Default: true
    //$args['show_import_export'] = false;

    // Set the icon for the import/export tab.
    // If $args['icon_type'] = 'image', this should be the path to the icon.
    // If $args['icon_type'] = 'iconfont', this should be the icon name.
    // Default: refresh
    $args['import_icon'] = Redux_OPTIONS_URL . 'img/icons/import_export.png';


    $bp_myOptions = get_option('bp_framework');
    // Set the class for the import/export tab icon.
    // This is ignored unless $args['icon_type'] = 'iconfont'
    // Default: null
    $args['import_icon_class'] = 'icon-large';

    // Set a custom option name. Don't forget to replace spaces with underscores!
    $args['opt_name'] = 'bp_framework';

    // Set a custom menu icon.
    //$args['menu_icon'] = '';

    // Set a custom title for the options page.
    // Default: Options
    $args['menu_title'] = __('Theme Options', Redux_TEXT_DOMAIN);

    // Set a custom page title for the options page.
    // Default: Options
    $args['page_title'] = __('Admin Panel', Redux_TEXT_DOMAIN);

    // Set a custom page slug for options page (wp-admin/themes.php?page=***).
    // Default: redux_options
    $args['page_slug'] = 'town_options';

    // Set a custom page capability.
    // Default: manage_options
    //$args['page_cap'] = 'manage_options';

    // Set the menu type. Set to "menu" for a top level menu, or "submenu" to add below an existing item.
    // Default: menu
    //$args['page_type'] = 'submenu';

    // Set the parent menu.
    // Default: themes.php
    // A list of available parent menus is available at http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
    //$args['page_parent'] = 'options_general.php';

    // Set a custom page location. This allows you to place your menu where you want in the menu order.
    // Must be unique or it will override other items!
    // Default: null
    //$args['page_position'] = null;

    // Set a custom page icon class (used to override the page icon next to heading)
    //$args['page_icon'] = 'icon-themes';

    // Set the icon type. Set to "iconfont" for Font Awesome, or "image" for traditional.
    // Redux no longer ships with standard icons!
    // Default: iconfont
    $args['icon_type'] = 'image';

    // Disable the panel sections showing as submenu items.
    // Default: true
    //$args['allow_sub_menu'] = false;
        
    // Set ANY custom page help tabs, displayed using the new help tab API. Tabs are shown in order of definition.
    $args['help_tabs'][] = array(
        'id' => 'redux-opts-1',
        'title' => __('Theme Information 1', Redux_TEXT_DOMAIN),
        'content' => __('<p>This is the tab content, HTML is allowed.</p>', Redux_TEXT_DOMAIN)
    );
    $args['help_tabs'][] = array(
        'id' => 'redux-opts-2',
        'title' => __('Theme Information 2', Redux_TEXT_DOMAIN),
        'content' => __('<p>This is the tab content, HTML is allowed.</p>', Redux_TEXT_DOMAIN)
    );

    // Set the help sidebar for the options page.                                        
    $args['help_sidebar'] = __('<p>This is the sidebar content, HTML is allowed.</p>', Redux_TEXT_DOMAIN);

    $sections = array();
     
     // GENERAL OPTIONS 

     $sections[] = array(
        'icon' => trailingslashit(get_template_directory_uri()).'functions/options/img/icons/general_options.png',
        'title' => __('General Options', Redux_TEXT_DOMAIN),
        'desc' => __('<p class="description">Set the general options of the theme</p>', Redux_TEXT_DOMAIN),
        'fields' => array(
           

            array(
                'id' => 'switch_responsive',
                'type' => 'checkbox',
                'title' => __('Responsive Enabled', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('Enable/Disable responsive', Redux_TEXT_DOMAIN),
                'switch' => true,
                'std' => '1' // 1 = checked | 0 = unchecked
            ),

            
             array(
                'id' => 'upload_logo',
                'type' => 'upload',
                'title' => __('Custom Logo', Redux_TEXT_DOMAIN), 
                'sub_desc' => __("You can upload the image that will represent your website's logo", Redux_TEXT_DOMAIN),

            ),


             array(
                'id' => 'upload_favicon',
                'type' => 'upload',
                'title' => __('Custom Favicon', Redux_TEXT_DOMAIN), 
                'sub_desc' => __("You can upload the icon that will represent your website's favicon (16px x 16px)", Redux_TEXT_DOMAIN),
              
            ),

            array(
                'id' => 'analytics_track',
                'type' => 'textarea',
                'title' => __('Analytics Tracking Code', Redux_TEXT_DOMAIN), 
                'sub_desc' => __("Paste your Google Analytics tracking code here.", Redux_TEXT_DOMAIN),
              
            ),

            array(
                'id' => 'custom_css',
                'type' => 'textarea',
                'title' => __('Custom CSS', Redux_TEXT_DOMAIN), 
                'sub_desc' => __("Any custom CSS from the user should go in this field, it will override the theme CSS.", Redux_TEXT_DOMAIN),
                
            ),

        
          
        )
    );

        
    
        $sections[] = array(
        'icon' => trailingslashit(get_template_directory_uri()).'functions/options/img/icons/styling_options.png',
        'icon_class' => 'icon-large',
        'title' => __('Styling Options', Redux_TEXT_DOMAIN),
        'desc' => __('<p class="description"></p>', Redux_TEXT_DOMAIN),
        'fields' => array(

            array(
                'id' => 'color_schemes',
                'type' => 'radio_img',
                'title' => __('Predefined color schemes ', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('Change the scheme color', Redux_TEXT_DOMAIN),
                'options' => array(
                    '1' => array('img' => Redux_OPTIONS_URL . 'img/color_1.jpg'),
                    '2' => array('img' => Redux_OPTIONS_URL . 'img/color_2.jpg'),
                    '3' => array('img' => Redux_OPTIONS_URL . 'img/color_3.jpg'),
                    '4' => array('img' => Redux_OPTIONS_URL . 'img/color_4.jpg'),
                    '5' => array('img' => Redux_OPTIONS_URL . 'img/color_5.jpg'),
                    '6' => array('img' => Redux_OPTIONS_URL . 'img/color_6.jpg'),
                ), // Must provide key => value(array:title|img) pairs for radio options
                'std' => array('1' => '0', '2' => '1', '3' => '0', '4' => '0', '5' => '0', '6' => '0' ) // See how std has changed? You also dont need to specify opts that are 0.
            ),

               array(
                'id' => 'body_bg_image',
                'type' => 'upload',
                'title' => __('Body Background Image', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('Set a BG Image for the body', Redux_TEXT_DOMAIN),
                'desc' => __(' ', Redux_TEXT_DOMAIN)
            ),

            array(
                'id' => 'background_color',
                'type' => 'color',
                'title' => __('Background Color', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('Set a plain color for the body background', Redux_TEXT_DOMAIN),
                'desc' => __(' ', Redux_TEXT_DOMAIN),
               
            ),
            
            array(
                'id' => 'text_color',
                'type' => 'color',
                'title' => __('Body Text', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('Set a plain color for text. Default: Blue-Gray #818894', Redux_TEXT_DOMAIN),
                'desc' => __(' ', Redux_TEXT_DOMAIN),
                'std' => '#818894'
            ),

            array(
                'id' => 'color_nav_menu',
                'type' => 'color',
                'title' => __('Color Navigation Menu', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('Set a color for the navigation text default:#a8a8a8', Redux_TEXT_DOMAIN),
                'desc' => __(' ', Redux_TEXT_DOMAIN),
                'std' => '#a8a8a8'
            ),

             array(
                'id' => 'color_nav_hover',
                'type' => 'color',
                'title' => __('Color Navigation Hover & Active', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('Set a color for the navigation hover and active', Redux_TEXT_DOMAIN),
                'desc' => __(' ', Redux_TEXT_DOMAIN),
                'std' => '#FFFFFF'
            ),

            array(
                'id' => 'color_page_title',
                'type' => 'color',
                'title' => __('Color Page Title', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('Set a color for the page titles. Default: Gray-Blue #2f353e', Redux_TEXT_DOMAIN),
                'desc' => __(' ', Redux_TEXT_DOMAIN),
                'std' => '#2f353e'
            ),

            array(
                'id' => 'color_page_sub_title',
                'type' => 'color',
                'title' => __('Color Page Sub Title', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('Set a color for the page titles. Default: Gray #b2b2b2', Redux_TEXT_DOMAIN),
                'desc' => __(' ', Redux_TEXT_DOMAIN),
                'std' => '#b2b2b2'
            ),

            array(
                'id' => 'header_color',
                'type' => 'color',
                'title' => __('Main header color', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('Set a color for the header', Redux_TEXT_DOMAIN),
                'desc' => __(' ', Redux_TEXT_DOMAIN),
            ),

            array(
                'id' => 'footer_color',
                'type' => 'color',
                'title' => __('Footer color', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('Set a color for the footer.', Redux_TEXT_DOMAIN),
                'desc' => __(' ', Redux_TEXT_DOMAIN),
            ),
        )
    );

$sections[] = array(

        'icon' => trailingslashit(get_template_directory_uri()).'functions/options/img/icons/font.png',
        'icon_class' => 'icon-large',
        'title' => __('Font Style', Redux_TEXT_DOMAIN),
        'desc' => __('<p class="description"></p>', Redux_TEXT_DOMAIN),
        'fields' => array(



             array(
                'id' => 'switch_normal_font',
                'type' => 'checkbox_hide_below',
                'title' => __('Enable Normal Fonts', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('The current default font is <strong>Open Sans</strong>', Redux_TEXT_DOMAIN),
                'switch' => true,
                'std' => '0', // 1 = checked | 0 = unchecked
                 'next_to_hide' => 1
            ),

             array(
                'id' => 'default_font_selection',
                'type' => 'select',
                'title' => __('Main Default Font', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('Select your main font family.', Redux_TEXT_DOMAIN),
                'desc' => __(' ', Redux_TEXT_DOMAIN),
                'options' => array('Arial' => 'Arial', 'Arial Narrow' => 'Arial Narrow', 'Courier New' => 'Courier New', 'Copperplate' => 'Copperplate','Georgia' => 'Georgia','Gill Sans' => 'Gill Sans',
                'Helvetica' => 'Helvetica', 'Impact' => 'Impact', 'Lucida Sans Unicode' => 'Lucida Sans Unicode', 'Lucida Console' => 'Lucida Console', 'sans-serif' => 'sans-serif', 'Tahoma' => 'Tahoma', 'Trebuchet MS' => 'Trebuchet MS', 
                'Times New Roman' => 'Times New Roman',  'Palatino Linotype' => 'Palatino Linotype'), // Must provide key => value pairs for select options
                'std' => 'Helvetica'
            ),

             array(
                'id' => 'switch_google_font',
                'type' => 'checkbox_hide_below',
                'title' => __('Enable Google Fonts', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('On mode enables Google font selection, Off mode use default fonts. The current default font is <strong>' .$bp_myOptions['default_font_selection'].'</strong>', Redux_TEXT_DOMAIN),
                'switch' => true,
                'std' => '0',// 1 = checked | 0 = unchecked
                 'next_to_hide' => 2
            ),

            array(
                'id' => 'body_text_font',
                'type' => 'google_webfonts',
                'title' => __('Body Text Font', Redux_TEXT_DOMAIN ), 
                'sub_desc' => __('Main font family. The default font is Helvetica.', Redux_TEXT_DOMAIN),
                'desc' => __(' ', Redux_TEXT_DOMAIN)
            ),
            array(
                'id' => 'heading_text_font',
                'type' => 'google_webfonts',
                'title' => __('Heading Text Font', Redux_TEXT_DOMAIN ), 
                'sub_desc' => __('Font family for H1,H2,H3,H4,H5,H6 and titles', Redux_TEXT_DOMAIN),
                'desc' => __(' ', Redux_TEXT_DOMAIN)
            ),
              array(
                'id' => 'body_font_size',
                'type' => 'text',
                'title' => __('Body Font Size ', Redux_TEXT_DOMAIN),
                'sub_desc' => __('The default value for body font size is: 16px', Redux_TEXT_DOMAIN),
                'desc' => __(' ', Redux_TEXT_DOMAIN),
                'validate' => 'numeric',
                'msg' => 'This valor need to be a number',
                'std' => ''
            ),
            array(
                'id' => 'top_nav_font_size',
                'type' => 'text',
                'title' => __('Top Nav Font Size ', Redux_TEXT_DOMAIN),
                'sub_desc' => __('The default value for Nav is: 16px', Redux_TEXT_DOMAIN),
                'desc' => __(' ', Redux_TEXT_DOMAIN),
                'validate' => 'numeric',
                'msg' => 'This valor need to be a number',
                'std' => ''
            ),
             array(
                'id' => 'copyright_font_size',
                'type' => 'text',
                'title' => __('Copyright Font Size ', Redux_TEXT_DOMAIN),
                'sub_desc' => __('The default value for Nav is: 14px', Redux_TEXT_DOMAIN),
                'desc' => __(' ', Redux_TEXT_DOMAIN),
                'validate' => 'numeric',
                'msg' => 'This valor need to be a number',
                'std' => ''
            ),
             array(
                'id' => 'h1_font_size',
                'type' => 'text',
                'title' => __('H1 Font Size ', Redux_TEXT_DOMAIN),
                'sub_desc' => __('The default value for H1 is: 24px', Redux_TEXT_DOMAIN),
                'desc' => __(' ', Redux_TEXT_DOMAIN),
                'validate' => 'numeric',
                'msg' => 'This valor need to be a number',
                'std' => ''
            ),
             array(
                'id' => 'h2_font_size',
                'type' => 'text',
                'title' => __('H2 Font Size ', Redux_TEXT_DOMAIN),
                'sub_desc' => __('The default value for H2 is: 22px', Redux_TEXT_DOMAIN),
                'desc' => __(' ', Redux_TEXT_DOMAIN),
                'validate' => 'numeric',
                'msg' => 'This valor need to be a number',
                'std' => ''
            ),
             array(
                'id' => 'h3_font_size',
                'type' => 'text',
                'title' => __('H3 Font Size ', Redux_TEXT_DOMAIN),
                'sub_desc' => __('The default value for H3 is: 18px', Redux_TEXT_DOMAIN),
                'desc' => __(' ', Redux_TEXT_DOMAIN),
                'validate' => 'numeric',
                'msg' => 'This valor need to be a number',
                'std' => ''
            ),
             array(
                'id' => 'h4_font_size',
                'type' => 'text',
                'title' => __('H4 Font Size ', Redux_TEXT_DOMAIN),
                'sub_desc' => __('The default value for H4 is: 16px', Redux_TEXT_DOMAIN),
                'desc' => __(' ', Redux_TEXT_DOMAIN),
                'validate' => 'numeric',
                'msg' => 'This valor need to be a number',
                'std' => ''
            ),
             array(
                'id' => 'h5_font_size',
                'type' => 'text',
                'title' => __('H5 Font Size ', Redux_TEXT_DOMAIN),
                'sub_desc' => __('The default value for H5 is: 14px', Redux_TEXT_DOMAIN),
                'desc' => __(' ', Redux_TEXT_DOMAIN),
                'validate' => 'numeric',
                'msg' => 'This valor need to be a number',
                'std' => ''
            ),
             array(
                'id' => 'h6_font_size',
                'type' => 'text',
                'title' => __('H6 Font Size ', Redux_TEXT_DOMAIN),
                'sub_desc' => __('The default value for H6 is: 12px', Redux_TEXT_DOMAIN),
                'desc' => __(' ', Redux_TEXT_DOMAIN),
                'validate' => 'numeric',
                'msg' => 'This valor need to be a number',
                'std' => ''
            ),
        )
    );

 $sections[] = array(
        'icon' => trailingslashit(get_template_directory_uri()).'functions/options/img/icons/blog.png',
        'icon_class' => 'icon-large',
        'title' => __('Blog', Redux_TEXT_DOMAIN),
        'desc' => __('<p class="description"></p>', Redux_TEXT_DOMAIN),
        'fields' => array(
             array(
                'id' => 'show_blog_link_in_home',
                'type' => 'checkbox_hide_below',
                'title' => __('Enable button link to blog section', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('This button appears in you home section in the blog part, the button will link to the blog section where you have all your post and not just only the last ones.', Redux_TEXT_DOMAIN),
                'switch' => true,
                'std' => '1' // 1 = checked | 0 = unchecked
            ),
                array(
                'id' => 'name_of_blog_button',
                'type' => 'text',
                'title' => __('Text of the button', Redux_TEXT_DOMAIN), 
                'sub_desc' => __("The default value is MORE POST.", Redux_TEXT_DOMAIN),
                'std' => 'More Post' // 
                
            ),

             array(
                'id' => 'post_per_page',
                'type' => 'text',
                'title' => __('Post per page in home page', Redux_TEXT_DOMAIN), 
                'sub_desc' => __("Select the number of post in your HOME page.", Redux_TEXT_DOMAIN),
                'std' => '3' // 3 post by Default
                
            ),
               array(
                'id' => 'post_per_page_full',
                'type' => 'text',
                'title' => __('Post per page in blog page', Redux_TEXT_DOMAIN), 
                'sub_desc' => __("Select the number of post in your BLOG page.", Redux_TEXT_DOMAIN),
                'std' => '3' // 3 post by Default
                
            ),

                array(
                'id' => 'post_per_page_search',
                'type' => 'text',
                'title' => __('Post per page in archive, search, category', Redux_TEXT_DOMAIN), 
                'sub_desc' => __("Select the number of post in your Archive, Search, Category page.", Redux_TEXT_DOMAIN),
                'std' => '3' // 3 post by Default
                
            ),

             array(
                'id' => 'blog_title',
                'type' => 'text',
                'title' => __('Blog page title', Redux_TEXT_DOMAIN), 
                'sub_desc' => __("Select the title of your blog and single-blog page.", Redux_TEXT_DOMAIN),
                'std' => 'RECENT NEWS' // "RECENT NEWS" By Default
                
            ),
             array(
                'id' => 'blog_subtitle',
                'type' => 'text',
                'title' => __('Blog subtitle', Redux_TEXT_DOMAIN), 
                'sub_desc' => __("Select the subtitle of your blog and single-blog page.", Redux_TEXT_DOMAIN),
                'std' => 'Subscribe and receive the latest news' // "Subscribe and receive the latest news" By Default
                
            ),
            
        )
    );


/*
 $sections[] = array(
        'icon' => trailingslashit(get_template_directory_uri()).'functions/options/img/icons/portfolio.png',
        'icon_class' => 'icon-large',
        'title' => __('Portfolio', Redux_TEXT_DOMAIN),
        'desc' => __('<p class="description"></p>', Redux_TEXT_DOMAIN),
        'fields' => array(

            array(
                'id' => 'number_portfolio',
                'type' => 'text',
                'title' => __('Number of Portfolios', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('Default number of items: 19', Redux_TEXT_DOMAIN),
                'validate' => 'numeric',
                'msg' => 'This valor need to be a number',
                'std' => '19'
            ),
            
        )
    );
*/

 $sections[] = array(
        'icon' => trailingslashit(get_template_directory_uri()).'functions/options/img/icons/contact.png',
        'icon_class' => 'icon-large',
        'title' => __('Contact', Redux_TEXT_DOMAIN),
        'desc' => __('<p class="description"></p>', Redux_TEXT_DOMAIN),
        'fields' => array(
            array(
                'id' => 'switch_captcha',
                'type' => 'checkbox',
                'title' => __('Captcha Enabled', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('Enable/Disable Captcha', Redux_TEXT_DOMAIN),
                'switch' => true,
                'std' => '0' // 1 = checked | 0 = unchecked
            ),



            array(
                'id' => 'contact_email',
                'type' => 'text',
                'title' => __('Email Adress', Redux_TEXT_DOMAIN),
                'sub_desc' => __('Set email to get the messages', Redux_TEXT_DOMAIN),
                'desc' => __(' ', Redux_TEXT_DOMAIN),
                'msg' => '',
                'std' => ''
            ),

            
        )
    );

$sections[] = array(
        'icon' => trailingslashit(get_template_directory_uri()).'functions/options/img/icons/footer.png',
        'icon_class' => 'icon-large',
        'title' => __('Footer', Redux_TEXT_DOMAIN),
        'desc' => __('<p class="description"></p>', Redux_TEXT_DOMAIN),
        'fields' => array(


            //  array(
            //     'id' => 'switch_twitter_footer',
            //     'type' => 'checkbox',
            //     'title' => __('Twitter feed Enabled', Redux_TEXT_DOMAIN), 
            //     'sub_desc' => __('Enable/Disable twitter feed in footer', Redux_TEXT_DOMAIN),
            //     'switch' => true,
            //     'std' => '1' // 1 = checked | 0 = unchecked
            // ),

            //  array(
            //     'id' => 'twitter_footer_feed',
            //     'type' => 'text',
            //     'title' => __('Twitter username', Redux_TEXT_DOMAIN),
            //     'sub_desc' => __('Your Twitter username', Redux_TEXT_DOMAIN),
            //     'desc' => __(' ', Redux_TEXT_DOMAIN),
            //     'msg' => '',
            //     'std' => 'bypixels'
            // ),


            array(
                'id' => 'footer_content',
                'type' => 'textarea',
                'title' => __('Footer Content', Redux_TEXT_DOMAIN), 
                'sub_desc' => __('Set the Copyright Message Here', Redux_TEXT_DOMAIN),
                'desc' => __(' ', Redux_TEXT_DOMAIN),
                'std' => '<div class="span3">
                <h3>About us</h3>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, aut, ullam quis ipsa mollitia dolore tempora! Deleniti, aspernatur, aliquid, rerum, error molestiae ex voluptates.
                <ul class="social_links_footer">
                    <li><a href="#"><i class="fa_icon icon-pinterest icon-2x"></i></a></li>
                    <li><a href="#"><i class="fa_icon icon-skype icon-2x"></i></a></li>
                    <li><a href="#"><i class="fa_icon icon-twitter icon-2x"></i></a></li>
                    <li><a href="#"><i class="fa_icon icon-dribbble icon-2x"></i></a></li>

                </ul>
                <div class="clear"></div>
                </div>
                <div class="span3">
                <h3>Support</h3>
                <p>Address: Noorder 1021, Amsterdam, Germany</p>
                <p>Telephone: 0292 1209</p>
                <p>Fax: 9837 1839</p>
                </div>
                <div class="span3">
                <h3>Latin America</h3>
                <p>Address: Frontway 2587, Montevideo, Uruguay</p>
                <p>Telephone: 0292 1209</p>
                <p>Fax: 9837 1839</p>
                </div>'
            ),

             array(
                'id' => 'upload_map_img',
                'type' => 'upload',
                'title' => __('Custom Map Image', Redux_TEXT_DOMAIN), 
                'sub_desc' => __("You can upload the image that will represent your location", Redux_TEXT_DOMAIN),

            ),
            array(
                'id' => 'map_url',
                'type' => 'text',
                'title' => __('Google map image link', Redux_TEXT_DOMAIN),
                'sub_desc' => __('Google map destination after click in the image', Redux_TEXT_DOMAIN),
                'desc' => __(' ', Redux_TEXT_DOMAIN),
                'msg' => '',
                'std' => 'https://maps.google.com/maps?q=Envato%20Pty%20Ltd,%20Elizabeth%20Street,%20Melbourne,%20Victoria,%20Australia'
            ),










        )
    );


      
    $tabs = array();

    if (function_exists('wp_get_theme')){
        $theme_data = wp_get_theme();
        $item_uri = $theme_data->get('ThemeURI');
        $description = $theme_data->get('Description');
        $author = $theme_data->get('Author');
        $author_uri = $theme_data->get('AuthorURI');
        $version = $theme_data->get('Version');
        $tags = $theme_data->get('Tags');
    }else{
        $theme_data =  wp_get_theme(trailingslashit(get_stylesheet_directory()) . 'style.css');
        $item_uri = $theme_data['URI'];
        $description = $theme_data['Description'];
        $author = $theme_data['Author'];
        $author_uri = $theme_data['AuthorURI'];
        $version = $theme_data['Version'];
        $tags = $theme_data['Tags'];
     }
    
    $item_info = '<div class="redux-opts-section-desc">';
    $item_info .= '<p class="redux-opts-item-data description item-uri">' . __('<strong>Theme URL:</strong> ', Redux_TEXT_DOMAIN) . '<a href="' . $item_uri . '" target="_blank">' . $item_uri . '</a></p>';
    $item_info .= '<p class="redux-opts-item-data description item-author">' . __('<strong>Author:</strong> ', Redux_TEXT_DOMAIN) . ($author_uri ? '<a href="' . $author_uri . '" target="_blank">' . $author . '</a>' : $author) . '</p>';
    $item_info .= '<p class="redux-opts-item-data description item-version">' . __('<strong>Version:</strong> ', Redux_TEXT_DOMAIN) . $version . '</p>';
    $item_info .= '<p class="redux-opts-item-data description item-description">' . $description . '</p>';
    $item_info .= '<p class="redux-opts-item-data description item-tags">' . __('<strong>Tags:</strong> ', Redux_TEXT_DOMAIN) . implode(', ', $tags) . '</p>';
    $item_info .= '</div>';

    $tabs['item_info'] = array(
        'icon' => trailingslashit(get_template_directory_uri()).'functions/options/img/icons/key.png',
        'icon_class' => 'icon-large',
        'title' => __('Theme Information', Redux_TEXT_DOMAIN),
        'content' => $item_info
    );
    
    if(file_exists(trailingslashit(dirname(__FILE__)) . 'README.html')) {
        $tabs['docs'] = array(
            'icon' => 'book',
            'icon_class' => 'icon-large',
            'title' => __('Documentation', Redux_TEXT_DOMAIN),
        );
    }

    global $Redux_Options;
    $Redux_Options = new Redux_Options($sections, $args, $tabs);

}
add_action('init', 'setup_framework_options', 0);

/*
 * 
 * Custom function for the callback referenced above
 *
 */
function my_custom_field($field, $value) {
    print_r($field);
    print_r($value);
}

/*
 * 
 * Custom function for the callback validation referenced above
 *
 */
function validate_callback_function($field, $value, $existing_value) {
    $error = false;
    $value =  'just testing';
    /*
    do your validation
    
    if(something) {
        $value = $value;
    } elseif(somthing else) {
        $error = true;
        $value = $existing_value;
        $field['msg'] = 'your custom error message';
    }
    */
    
    $return['value'] = $value;
    if($error == true) {
        $return['error'] = $field;
    }
    return $return;
}
