<?php




function town_scripts_basic() {  
    
    /* ------------------------------------------------------------------------ */
    /* Enqueue Scripts */
    /* ------------------------------------------------------------------------ */

    wp_enqueue_script('jquery.prettyPhoto', get_template_directory_uri() . '/js/jquery.prettyPhoto.js',array('jquery'),'1.0',true);
    wp_enqueue_script('jquery.scrollTo', get_template_directory_uri() . '/js/jquery.scrollTo.js',array('jquery'),'1.0',true);
    wp_enqueue_script('carousel', get_template_directory_uri() . '/js/carousel.js',array('jquery'),'1.0',true); 
    wp_enqueue_script('retina', get_template_directory_uri() . '/js/retina.js',array('jquery'),'1.0',true);
    wp_enqueue_script('jquery.nav', get_template_directory_uri() . '/js/jquery.nav.js',array('jquery'),'1.0',true);
    wp_enqueue_script('modernizr.custom', get_template_directory_uri() . '/js/modernizr.custom.js',array('jquery'),'1.0',true);
    wp_enqueue_script('jquery.isotope.min', get_template_directory_uri() . '/js/jquery.isotope.min.js',array('jquery'),'1.0',true);
    wp_enqueue_script('contact', get_template_directory_uri() . '/js/contact-form.js',array('jquery'),'1.0',true);
    wp_enqueue_script('custom', get_template_directory_uri() . '/js/custom.js',array('jquery'),'1.0',true);

}

add_action( 'wp_enqueue_scripts', 'town_scripts_basic' );  


add_action( 'wp_enqueue_scripts', 'town_style_css' );
function town_styles_basic()  
{  
    /* ------------------------------------------------------------------------ */
    /* Register Stylesheets */
    /* ------------------------------------------------------------------------ */

    wp_register_style( 'reset', get_template_directory_uri() . '/css/reset.css', array(), '1', 'all' );
    wp_register_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.css', array(), '1', 'all' );
    wp_register_style( 'bootstrap-responsive', get_template_directory_uri() . '/css/bootstrap-responsive.css', array(), '1', 'all' );
    wp_register_style( 'prettyPhoto', get_template_directory_uri() . '/css/prettyPhoto.css', array(), '1', 'all' );
    wp_register_style( 'responsive', get_template_directory_uri() . '/css/responsive.css');
    wp_register_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.css');

    

    /* ------------------------------------------------------------------------ */
    /* Enqueue Stylesheets */
    /* ------------------------------------------------------------------------ */
    
    wp_enqueue_style( 'reset', get_stylesheet_uri(), array(), '1', 'all' );
    wp_enqueue_style( 'bootstrap', get_stylesheet_uri(), array(), '1', 'all' );
    wp_enqueue_style( 'bootstrap-responsive', get_stylesheet_uri(), array(), '1', 'all' );
    wp_enqueue_style( 'prettyPhoto', get_stylesheet_uri(), array(), '1', 'all' );
    wp_enqueue_style( 'responsive', get_stylesheet_uri(), array(), '1', 'all' );
    wp_enqueue_style( 'font-awesome', get_stylesheet_uri(), array(), '1', 'all' );


}  
add_action( 'wp_enqueue_scripts', 'town_styles_basic', 1 ); 

function my_class_names($classes) {
    // add 'class-name' to the $classes array
    $classes[] = 'global';
    // return the $classes array
    return $classes;
}

//Now add test class to the filter
add_filter('body_class','my_class_names');

/**
 * Enqueue the Open Sans font.
 */
function mytheme_fonts() {
    $protocol = is_ssl() ? 'https' : 'http';
    wp_enqueue_style( 'mytheme-opensans', "$protocol://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300" );}
add_action( 'wp_enqueue_scripts', 'mytheme_fonts' );


/**
 * Enqueue the PT Sans font.
 */


function mytheme_fontss() {
    $protocol = is_ssl() ? 'https' : 'http';
    wp_enqueue_style( 'mytheme-ptsans', "$protocol://fonts.googleapis.com/css?family=PT+Sans:400,700" );}
add_action( 'wp_enqueue_scripts', 'mytheme_fontss' );



function town_style_css() {
  wp_enqueue_style( 'mytheme-style', get_bloginfo( 'stylesheet_url' ), array(), '1' );
}



function custom_theme_setup() {
 
    // Retrieve the directory for the localization files
    $lang_dir = get_template_directory() . '/languages';
     
    // Set the theme's text domain using the unique identifier from above
    load_theme_textdomain('wp_town', $lang_dir);
 
} // end custom_theme_setup
add_action('after_setup_theme', 'custom_theme_setup');





/* META BOX FOR PAGES */ 


define('MY_WORDPRESS_FOLDER',$_SERVER['DOCUMENT_ROOT']);
define('MY_THEME_FOLDER',str_replace("\\",'/',dirname(__FILE__)));
define('MY_THEME_PATH','/' . substr(MY_THEME_FOLDER,stripos(MY_THEME_FOLDER,'wp-content')));


add_action('admin_init','my_meta_init');
 
function my_meta_init()
{
    
    // review the function reference for parameter details
    // http://codex.wordpress.org/Function_Reference/add_meta_box
 
    // add a meta box for each of the wordpress page types: posts and pages
    foreach (array('page') as $type) 
    {
        add_meta_box('my_all_meta', 'Page Options', 'my_meta_setup', $type, 'normal', 'high');
    }
     
    // add a callback function to save any data a user enters in
    add_action('save_post','my_meta_save');
}
 
function my_meta_setup()
{
    global $post, $meta_boxes;  
     
    // using an underscore, prevents the meta variable
    // from showing up in the custom fields section
    $meta = get_post_meta($post->ID,'metas',TRUE);
    
    // instead of writing HTML here, lets do an include
    include(MY_THEME_FOLDER . '/meta.php');
  
    // create a custom nonce for submit verification later
    wp_nonce_field( basename( __FILE__ ), 'custom_meta_box_nonce' );
}
  
function my_meta_save($post_id) 
{
    // authentication checks
 
    // make sure data came from our meta box

    
    if (isset($_POST['post_type']) == 'page') 
    {
        if (!isset($_POST['custom_meta_box_nonce']) || !wp_verify_nonce($_POST['custom_meta_box_nonce'], basename(__FILE__)));

    }

    if (isset($_POST['post_type']) == 'page') 
    {
        if (!current_user_can('edit_page', $post_id)) return $post_id;
    }
    else
    {
        if (!current_user_can('edit_post', $post_id)) return $post_id;
    }

 
    $current_data = get_post_meta($post_id, 'metas', TRUE);  
  


    if (isset($_POST['post_type']) == 'page') 
    {

         if (isset($_POST['metas']))  {  

    $new_data = $_POST['metas'];  

     my_meta_clean($new_data);
     
    if ($current_data) 
    {
        if (is_null($new_data)) delete_post_meta($post_id,'metas');
        else update_post_meta($post_id,'metas',$new_data);
    }
    elseif (!is_null($new_data))
    {
        add_post_meta($post_id,'metas',$new_data,TRUE);
    }
    return $post_id;

} else 

 { 

   //nothing

}
        
}

}

function my_meta_clean(&$arr)
{
    if (is_array($arr))
    {
        foreach ($arr as $i => $v)
        {
            if (is_array($arr[$i])) 
            {
                my_meta_clean($arr[$i]);
 
                if (!count($arr[$i])) 
                {
                    unset($arr[$i]);
                }
            }
            else
            {
                if (trim($arr[$i]) == '') 
                {
                    unset($arr[$i]);
                }
            }
        }
 
        if (!count($arr)) 
        {
            $arr = NULL;
        }
    }
}








add_filter('get_avatar','change_avatar_css');

function change_avatar_css($class) {
$class = str_replace("class='avatar", "class='author_gravatar", $class) ;
return $class;
}


/* ------------------------------------------------------------------------ */
/* Zifa Main Menu */
/* ------------------------------------------------------------------------ */

if (function_exists('add_theme_support')) {
    add_theme_support('menus');
}

function town_addmenus() {

    register_nav_menu( 'town_nav_menu', __( 'The Main Menu', '' ) );

}
add_action( 'init', 'town_addmenus' );
 
function town_nav() {
    if ( function_exists( 'wp_nav_menu' ) )
        wp_nav_menu( 'container=&container_class=&fallback_cb=town_nav_fallback' );
    else
        town_nav_fallback();
}
 
function town_nav_fallback() {
    wp_page_menu( 'show_home=0&include=999' );
}   


/***********************************************************************************************/
/*  Define Constants */
/***********************************************************************************************/

define('THEMEROOT', get_stylesheet_directory_uri());
define('IMAGES', THEMEROOT.'/images');


/***********************************************************************************************/
/*  Slug */
/***********************************************************************************************/

function the_slug($postID="") {

    global $post;
    $postID = ( $postID != "" ) ? $postID : $post->ID;
    $post_data = get_post($postID, ARRAY_A);
    $slug = $post_data['post_name'];
    return $slug;

}


/***********************************************************************************************/
/*  Image Sizes */
/***********************************************************************************************/


if( function_exists( 'add_theme_support' ) ) {
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 859, 346, true ); // Normal post thumbnails
    add_image_size( 'portfolio', 360, 250, true); // for the portfolio template
    add_image_size( 'portfolio-large', 1024, 768, false); // for the single portfolio page
    add_image_size( 'blog', 859, 346, true); // for large post thumb
    add_image_size( 'post-thumb-mini', 60, 60, true); // for recent projects
}

	/*
    add_action('init', 'create_portfolio');
    function create_portfolio() {
        $portfolio_args = array(
            'label' => __('Portfolio', 'wp_town'),
            'singular_label' => __('Portfolio', 'wp_town'),
            'public' => true,
            'show_ui' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'rewrite' => true,
            'supports' => array('title', 'editor', 'thumbnail')
        );
        register_post_type('portfolio',$portfolio_args);
    }
	*/


function mytheme_comment($comment, $args, $depth) {
        $GLOBALS['comment'] = $comment;
        extract($args, EXTR_SKIP);
        if ( 'div' == $args['style'] ) {
            $tag = 'div';
            $add_below = 'comment';
        } else {
            $tag = 'li';
            $add_below = 'div-comment';
        }
        ?>
        <?php echo $tag ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
        <?php if ( 'div' != $args['style'] ) : ?>
        <div id="div-comment-<?php comment_ID() ?>" class="comment-body">
        <?php endif; ?>
        <div class="comment-author vcard">
        <?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['avatar_size'] ); ?>
        <?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
        </div>
        <?php if ($comment->comment_approved == '0') : ?>
        <em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.', 'wp_town') ?></em>
        <br />
        <?php endif; ?>
        <div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
        <?php
        /* translators: 1: date, 2: time */
        printf( __('%1$s at %2$s', 'wp_town'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit),', 'wp_town'),'  ','' );
        ?>
        </div>
        <?php comment_text() ?>
        <div class="reply">
        <?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
        </div>
        <?php if ( 'div' != $args['style'] ) : ?>
        </div>
        <?php endif; ?>
        <?php
        }


if ( function_exists('register_sidebar') ) {
    register_sidebar(array(
        'name' => 'My Sidebar',
        'description' => __('Set the different types of widget for your sidebar.', 'wp_town'),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget' => '</li>',
        'before_title' => '<h2 class="widgettitle">',
        'after_title' => '</h2>',
    ));
}




function add_styles()
{
    $bp_myOptions = get_option('bp_framework'); 
    if (isset($bp_myOptions['color_schemes']) && $bp_myOptions['color_schemes'] == '1') { ?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri();?>/css/skins/orange.css">
    <?php
    } elseif (isset($bp_myOptions['color_schemes']) && $bp_myOptions['color_schemes'] == '2') { ?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri();?>/css/skins/blue.css">
    <?php }
    elseif (isset($bp_myOptions['color_schemes']) && $bp_myOptions['color_schemes'] == '3') {?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri();?>/css/skins/red.css">
    <?php }
    elseif (isset($bp_myOptions['color_schemes']) && $bp_myOptions['color_schemes'] == '4') {?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri();?>/css/skins/gray.css">
    <?php }
    elseif (isset($bp_myOptions['color_schemes']) && $bp_myOptions['color_schemes'] == '5') {?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri();?>/css/skins/green.css">
    <?php }
    elseif (isset($bp_myOptions['color_schemes']) && $bp_myOptions['color_schemes'] == '6') {?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri();?>/css/skins/yellow.css">
    <?php }
}
add_action('wp_head', 'add_styles');

function add_styles_custom()
{
$bp_myOptions = get_option('bp_framework'); 

$custom_css = isset( $bp_myOptions['custom_css'] ) ? esc_attr( $bp_myOptions['custom_css'] ) : '';
$body_bg_image = isset( $bp_myOptions['body_bg_image'] ) ? esc_attr( $bp_myOptions['body_bg_image'] ) : '';
$upload_map_img = isset( $bp_myOptions['upload_map_img'] ) ? esc_attr( $bp_myOptions['upload_map_img'] ) : '';

?>

<?php // If Google Font Switch is active
if(!empty($bp_myOptions['switch_google_font']) && $bp_myOptions['switch_google_font'] == '1') { ?>
    <?php if(!empty($bp_myOptions['body_text_font'])) { ?>
        <link href='http://fonts.googleapis.com/css?family=<?php echo $bp_myOptions['body_text_font']; ?>' rel='stylesheet' type='text/css'>
    <?php } ?>

    <?php if(!empty($bp_myOptions['heading_text_font'])) { ?>
        <link href='http://fonts.googleapis.com/css?family=<?php echo $bp_myOptions['heading_text_font']; ?>' rel='stylesheet' type='text/css'>
    <?php } ?>


<?php }?>


<style type="text/css">
<?php echo $custom_css; ?>


<?php 
if(!empty($bp_myOptions['upload_map_img'])) { ?>
    #map_google {background:url("<?php echo $upload_map_img;?>");}
<?php }?>



<?php 
if(!empty($bp_myOptions['body_bg_image'])) { ?>
    body {background:url("<?php echo $body_bg_image;?>");}
<?php }?>


<?php  
if(!empty($bp_myOptions['background_color'])) { ?>
   body {background-color:<?php echo $bp_myOptions['background_color']; ?>;}
<?php }?>

<?php  
if(!empty($bp_myOptions['text_color'])) { ?>
   .blog-item p, body {color:<?php echo $bp_myOptions['text_color']; ?>;}
<?php }?>

<?php  
if(!empty($bp_myOptions['body_text_color'])) { ?>
   p, #portfolio .pfolio_desc p {color:<?php echo $bp_myOptions['body_text_color']; ?>;}
<?php }?>

<?php  
if(!empty($bp_myOptions['color_page_title'])) { ?>
   .page_title {color:<?php echo $bp_myOptions['color_page_title']; ?>}
<?php }?>

<?php  
if(!empty($bp_myOptions['color_page_sub_title'])) { ?>
   .page_subtitle {color:<?php echo $bp_myOptions['color_page_sub_title']; ?>}
<?php }?>

.main_nav li a {color:<?php echo $bp_myOptions['color_nav_menu']; ?>; font-size:<?php echo $bp_myOptions['top_nav_font_size'];?>px;}
.main_nav li a:hover, .main_nav li.active a {color:<?php echo $bp_myOptions['color_nav_hover']; ?>;}



<?php // If Normal Font Switch is active
if(!empty($bp_myOptions['switch_normal_font']) && $bp_myOptions['switch_normal_font'] == '1') { ?>
   
 body {font-family:<?php echo $bp_myOptions['default_font_selection']; ?>;}

<?php }?>



<?php 
if(!empty($bp_myOptions['body_font_size'])) { ?>
    body {font-size:<?php echo $bp_myOptions['body_font_size'];?>px;}
<?php }?>

<?php 
if(!empty($bp_myOptions['header_color'])) { ?>
    #header {background-color:<?php echo $bp_myOptions['header_color'];?>}
<?php }?>

<?php 
if(!empty($bp_myOptions['footer_color'])) { ?>
   #footer {background-color: <?php echo $bp_myOptions['footer_color'];?>}
<?php }?>

<?php 
if(!empty($bp_myOptions['h1_font_size'])) { ?>
   body h1 {font-size:<?php echo $bp_myOptions['h1_font_size'];?>px;}
<?php }?>

<?php 
if(!empty($bp_myOptions['h2_font_size'])) { ?>
   body h2 {font-size:<?php echo $bp_myOptions['h2_font_size'];?>px;}
<?php }?>

<?php 
if(!empty($bp_myOptions['h3_font_size'])) { ?>
   body h3 {font-size:<?php echo $bp_myOptions['h3_font_size'];?>px;}
<?php }?>

<?php 
if(!empty($bp_myOptions['h4_font_size'])) { ?>
   body h4 {font-size:<?php echo $bp_myOptions['h4_font_size'];?>px;}
<?php }?>

<?php 
if(!empty($bp_myOptions['h5_font_size'])) { ?>
   body h5 {font-size:<?php echo $bp_myOptions['h5_font_size'];?>px;}
<?php }?>

<?php 
if(!empty($bp_myOptions['h6_font_size'])) { ?>
   body h6 {font-size:<?php echo $bp_myOptions['h6_font_size'];?>px;}
<?php }?>

<?php 
if(!empty($bp_myOptions['copyright_font_size'])) { ?>
   #footer p {font-size:<?php echo $bp_myOptions['copyright_font_size'];?>px;}
<?php }?>




 


<?php 

if(!empty($bp_myOptions['body_text_font'])) { 
    $font_name = $bp_myOptions['body_text_font'];
    $font_name = str_replace("+", " ", $font_name);
    $font_name = explode(":", $font_name);
    $font_name = $font_name[0]; 
 }



if(!empty($bp_myOptions['heading_text_font'])) { 
    
    $font_name_h = $bp_myOptions['heading_text_font'];
    $font_name_h = str_replace("+", " ", $font_name_h);
    $font_name_h = explode(":", $font_name_h);
    $font_name_h = $font_name_h[0]; 
 }


?>

<?php // If Google Font Switch is active
if(!empty($bp_myOptions['switch_google_font']) && $bp_myOptions['switch_google_font'] == '1') { ?>

    <?php if(!empty($bp_myOptions['body_text_font'])) { ?>
        body{font-family: <?php echo $font_name; ?>, Arial;}
    <?php } ?>

    <?php if(!empty($bp_myOptions['heading_text_font'])) { ?>
        h1,h2,h3,h4,h5,h6{font-family: <?php echo $font_name_h; ?>, Arial;}
    <?php } ?>


   
<?php }?>

</style>

<?php
}

add_action('wp_head', 'add_styles_custom');




/**
 * Makes our wp_nav_menu() fallback -- wp_page_menu() -- show a home link.
 *
 * @since Twenty Twelve 1.0
 */
function town_page_menu_args( $args ) {
    if ( ! isset( $args['show_home'] ) )
        $args['show_home'] = true;
    return $args;
}
add_filter( 'wp_page_menu_args', 'town_page_menu_args' );

function my_tag_cloud_args($in){
    return 'smallest=10&amp;largest=20&amp;number=25&amp;orderby=name&amp;unit=px';
}
add_filter( 'widget_tag_cloud_args', 'my_tag_cloud_args');

function enqueue_comment_reply() {
   if ( get_option( 'thread_comments' ) ) { 
       wp_enqueue_script( 'comment-reply' ); 
   }
}
// Hook into comment_form_before
add_action( 'comment_form_before', 'enqueue_comment_reply' );





//Pagination

if ( ! function_exists( 'my_pagination' ) ) :
    function my_pagination() {
        global $wp_query;

        $big = 999999999; // need an unlikely integer
        
        echo paginate_links( array(
            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'format' => '?paged=%#%',
            'current' => max( 1, get_query_var('paged') ),
            'total' => $wp_query->max_num_pages,
            'type' => 'list'
        ) );
    }
endif;



// Pagination on the rest of the site : Category, Archive, Search


add_action( 'pre_get_posts',  'set_posts_per_page'  );
function set_posts_per_page( $query ) {


$bp_myOptions = get_option('bp_framework'); 
if(!empty($bp_myOptions['post_per_page_search'])) {  $post_search = $bp_myOptions['post_per_page_search']; }

  global $wp_the_query;

  if ( ( ! is_admin() ) && ( $query === $wp_the_query ) && ( $query->is_search() ) ) {
    $query->set( 'posts_per_page', $post_search );
    $query->set( 'post_type', 'post' );
    
  }
  elseif ( ( ! is_admin() ) && ( $query === $wp_the_query ) && ( $query->is_archive() ) ) {
    $query->set( 'posts_per_page', $post_search );
    $query->set( 'post_type', 'post' );
  }
  elseif ( ( ! is_admin() ) && ( $query === $wp_the_query ) && ( $query->is_category() ) ) {
    $query->set( 'posts_per_page', $post_search );
    $query->set( 'post_type', 'post' );
  }

  return $query;
}



/***********************************************************************************************/
/*  Shortcodes Generator */
/***********************************************************************************************/


include( get_template_directory() . '/functions/zilla-shortcodes/zilla-shortcodes.php' );



?>