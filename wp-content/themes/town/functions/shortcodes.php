<?php 

// Team container 
function bp_team_container($atts, $content=null) {
   return '<div class="team_container">'.do_shortcode($content).'</div>';
}
add_shortcode('team_container', 'bp_team_container');

// Team
function bp_teammate( $atts, $content = null ) {
  extract( shortcode_atts( array(
    'name' => '',
    'job' =>'',
    'image' => '',
    'facebook_url' => '',
    'twitter_url' => '',
    'skype_url' => '',
    'dribbble_url' => '',
    'pinterest_url' => '',
    'tumblr_url' => '',
    'youtube_url' => '',
    'google_url' => '',
    'linkedin_url' => '',
  ), $atts ) );
  $output='<div class="team_img_container" style="background-image: url('.$image.');"></div>
              <div class="team_description"> ';

  if($name!='') $output.= '<h3>'.$name.'</h3>';
  if($job!='') $output.= '<p>'.$job.'</p>'; 

  $output.='<div class="social">
                  <ul>';

if($facebook_url!='') $output.= '<li><a href="'.$facebook_url.'"><i class="fa_icon icon-facebook icon-2x"></i></a></li>'; 
if($pinterest_url!='') $output.= '<li><a href="'.$pinterest_url.'"><i class="fa_icon icon-pinterest icon-2x"></i></a></li>'; 
if($skype_url!='') $output.= '<li><a href="'.$skype_url.'"><i class="fa_icon icon-skype icon-2x"></i></a></li>'; 
if($twitter_url!='') $output.= '<li><a href="'.$twitter_url.'"><i class="fa_icon icon-twitter icon-2x"></i></a></li>'; 
if($dribbble_url!='') $output.= '<li><a href="'.$dribbble_url.'"><i class="fa_icon icon-dribbble icon-2x"></i></a></li>';
if($tumblr_url!='') $output.= '<li><a href="'.$tumblr_url.'"><i class="fa_icon icon-tumblr icon-2x"></i></a></li>'; 
if($youtube_url!='') $output.= '<li><a href="'.$youtube_url.'"><i class="fa_icon icon-youtube icon-2x"></i></a></li>'; 
if($google_url!='') $output.= '<li><a href="'.$google_url.'"><i class="fa_icon icon-google-plus icon-2x"></i></a></li>'; 
if($linkedin_url!='') $output.= '<li><a href="'.$linkedin_url.'"><i class="fa_icon icon-linkedin icon-2x"></i></a></li>'; 


   $output.='</ul></div></div>';

  return $output;
}
add_shortcode('teammate', 'bp_teammate');




// Button Rec
function bp_button_rec($atts, $content=null) {
    extract( shortcode_atts( array(
    'link' => '',
    'text' => '',
    ), $atts )
    );

    $output='
     <div class="button_rec uppercase"><a href="'.$link.'">'.$text.'</a></div>
';
    return $output;
    }
    add_shortcode('button_rec', 'bp_button_rec');   


// Testimonial tab
function bp_testimonial_tab($atts, $content=null) {
    extract( shortcode_atts( array(
    'tab' => '',
    ), $atts )
    );

    $output='
     <p class="testimonial" id="'.$tab.'"> '.do_shortcode($content).'</p>
';
    return $output;
    }
    add_shortcode('testimonial_tab', 'bp_testimonial_tab'); 

// Testimonial content
function bp_testimonial_content($atts, $content=null) {
    extract( shortcode_atts( array(
    'tab_link' => '',
    'img' => '',
    'name' => '',
    'company' => '',
    ), $atts )
    );

    $output='
    <div class="testi_column">
            <div class="col_wrapper">
            <a href="#'.$tab_link.'">
              <img src="'.$img.'" alt="">
              <p class="testimonials_name">'.$name.'</p>
              <p class="testimonials_company">'.$company.'</p>
            </a>
          </div>
          </div>
';
    return $output;
    }
    add_shortcode('testimonial_content', 'bp_testimonial_content'); 
    

// Testimonial Trigger
function bp_testimonial_trigger($atts, $content=null) {
   return '<div class="triggers_testimonials">' . do_shortcode($content) . '</div>';
}
add_shortcode('testimonial_trigger', 'bp_testimonial_trigger');  
    

// Page Title
function bp_page_title($atts, $content=null) {
   return '<h1 class="page_title">' . do_shortcode($content) . '</h1>';
}
add_shortcode('page_title', 'bp_page_title');

// Page Title Small
function bp_page_title_small($atts, $content=null) {
   return '<h1 class="page_title small">' . do_shortcode($content) . '</h1>';
}
add_shortcode('page_title_small', 'bp_page_title_small');

// Page Subtitle
function bp_page_subtitle($atts, $content=null) {
   return '<h1 class="page_subtitle">' . do_shortcode($content) . '</h1>';
}
add_shortcode('page_subtitle', 'bp_page_subtitle');

// Page Description
function bp_page_description($atts, $content=null) {
   return '<div class="page_description">' . do_shortcode($content) . '</div>';
}
add_shortcode('page_description', 'bp_page_description');

// page_line
function bp_page_line($atts, $content=null) {
   return '<div class="page_line"></div>';
}
add_shortcode('page_line', 'bp_page_line');

// About container
function bp_about_container($atts, $content=null) {
   return '<div class="about_container">' . do_shortcode($content) . '</div>';
}
add_shortcode('about_container', 'bp_about_container');

// Parallax Container
function bp_parallax_container($atts, $content=null) {
   return '<div class="parallax_container">' . do_shortcode($content) . '</div>';
}
add_shortcode('parallax_container', 'bp_parallax_container');



// Parallax Icons

function bp_parallax_column($atts, $content=null) {
    extract( shortcode_atts( array(
    'icon_name' => '',
    'type' => '',
    'icon_size' => '',
    'heading' => '',
    'text' => '',
    'column_size' => '',
    'img' =>'',
    ), $atts )
    );
      //if($dribbble!='') $output.= '<a href="'.$dribbble.'" class="social_icon dribbble"></a>';
    $output='
    <div class="parallax_colum'.$column_size.'"><div class="parallax_column_container">';


          if($type=='logo_slide') $output.='<img src=" '.$img.' " alt="" />';  

          if($type=='icon_slide') $output.='<div class="circle_icon"><i class="fa_icon '.$icon_name.' '.$icon_size.'"></i></div>
          <h2>'.$heading.'</h2>
          <p class="display">'.$text.'</p>';


      $output.='</div></div>';    

    return $output;
    }
    add_shortcode('parallax_column', 'bp_parallax_column'); 

// Over About
function bp_over_about($atts, $content=null) {
   return '<div class="over_about">
    <div class="over_container">' . do_shortcode($content) . '</div></div>';
}
add_shortcode('over_about', 'bp_over_about');

// Skill bar
function bp_skill_bar($atts, $content=null) {
   return '<div class="skillbar"><div class="bar"><span class="percent">' . do_shortcode($content) . '</span></div></div>';
}
add_shortcode('skill_bar', 'bp_skill_bar');

// Services Icons container
function bp_service_icons($atts, $content=null) {
   return '<div class="services_icon_container hi-icon-effect-1 hi-icon-effect-1a">' . do_shortcode($content) . '</div>';
}
add_shortcode('service_icons_container', 'bp_service_icons');

// Bp Icon
function bp_icon($atts, $content=null) {
    extract( shortcode_atts( array(
    'link' => '',
    'icon_name' => '',
    'icon_size' => '',
    ), $atts )
    );

    $output='
     <div class="icon_wrapper"><a href="'.$link.'" class="hi-icon"><i class="fa_icon '.$icon_name.'  '.$icon_size.'"></i></a></div>
';
    return $output;
    }
    add_shortcode('icon', 'bp_icon');    































// Clear
function bp_clear($atts, $content=null) {
   return '<div class="clear"></div>';
}
add_shortcode('clear', 'bp_clear');

// // Container
// function bp_center($atts, $content=null) {
//    return '<div class="container">' . do_shortcode($content) . '</div><!--container-->';
// }
// add_shortcode('center', 'bp_center');

// // Row
// function bp_row($atts, $content=null) {
//    return '<div class="row">' . do_shortcode($content) . '</div><!--row-->';
// }
// add_shortcode('row', 'bp_row');

// One Column
function bp_one_column($atts, $content=null) {
   return '<div class="span12">' . do_shortcode($content) . '</div><!--span12-->';
}
add_shortcode('one_column', 'bp_one_column');

// Two Column
function bp_two_column($atts, $content=null) {
   return '<div class="span6">' . do_shortcode($content) . '</div>';
}
add_shortcode('two_column', 'bp_two_column');

// Three Column
function bp_three_column($atts, $content=null) {
   return '<div class="span4">' . do_shortcode($content) . '</div>';
}
add_shortcode('three_column', 'bp_three_column');

// Four Column
function bp_four_column($atts, $content=null) {
   return '<div class="span3">' . do_shortcode($content) . '</div>';
}
add_shortcode('four_column', 'bp_four_column');

// Six Column
function bp_six_column($atts, $content=null) {
   return '<div class="span2">' . do_shortcode($content) . '</div>';
}
add_shortcode('six_column', 'bp_six_column');

// Span shortcodes for people familiar with bootstrap framework

function bp_column( $atts, $content = null ) {
    extract(shortcode_atts(array(
      "size" => ''
    ), $atts));
    $output='<div';
    if($size!='') $output.=' class="span'.$size.'"';
     $output.='>'.do_shortcode($content).'</div>';

    return $output;
  }
add_shortcode('column', 'bp_column');





// Shortcode for span
function bp_span( $atts, $content = null ) {
  extract( shortcode_atts( array(
    'color' => '',
    'weight' => '',
    'class' => '',
  ), $atts ) );
  $output='<span ';

  if($class!='') $output.= 'class="'.$class.'"'; 

  if($color!='' || $weight!='') $output.= 'style="'; //Open Style
      if($color!='') $output.= 'color:'.$color.';';
      if($weight!='') $output.= 'font-weight:'.$weight.';';
  if($color!='' || $weight!='') $output.='"'; //Close Style
  $output.='>'.do_shortcode($content).'</span>';
  return $output;
}
add_shortcode('span', 'bp_span');



// Shortcode for H1
function bp_h1( $atts, $content = null ) {
  extract( shortcode_atts( array(
    'color' => '',
    'weight' => '',
    'align' => '',
    'class'=> '',
  ), $atts ) );
  $output='<h1 ';

  if($align!='' || $class!='') $output.= 'class="';
     if($class!='') $output.= ' '.$class. '';
     if($align!='') $output.= ' ' .$align.'';
  if($align!='' || $class!='') $output.='"'; //Close Class
  if($color!='' || $weight!='') $output.= 'style="'; //Open Style
      if($color!='') $output.= 'color:'.$color.';';
      if($weight!='') $output.= 'font-weight:'.$weight.';';
  if($color!='' || $weight!='') $output.='"'; //Close Style
  $output.='>'.do_shortcode($content).'</h1>';
  return $output;
}
add_shortcode('h1', 'bp_h1');


// Shortcode for H2
function bp_h2( $atts, $content = null ) {
  extract( shortcode_atts( array(
    'color' => '',
    'weight' => '',
    'align' => '',
  ), $atts ) );
  $output='<h2 ';

  if($align!='') $output.= 'class="'.$align.'"'; 
  if($color!='' || $weight!='') $output.= 'style="'; //Open Style
      if($color!='') $output.= 'color:'.$color.';';
      if($weight!='') $output.= 'font-weight:'.$weight.';';
  if($color!='' || $weight!='') $output.='"'; //Close Style
  $output.='>'.do_shortcode($content).'</h2>';
  return $output;
}
add_shortcode('h2', 'bp_h2');


// Shortcode for H3
function bp_h3( $atts, $content = null ) {
  extract( shortcode_atts( array(
    'color' => '',
    'weight' => '',
    'align' => '',
  ), $atts ) );
  $output='<h3 ';

  if($align!='') $output.= 'class="'.$align.'"'; 
  if($color!='' || $weight!='') $output.= 'style="'; //Open Style
      if($color!='') $output.= 'color:'.$color.';';
      if($weight!='') $output.= 'font-weight:'.$weight.';';
  if($color!='' || $weight!='') $output.='"'; //Close Style
  $output.='>'.do_shortcode($content).'</h3>';
  return $output;
}
add_shortcode('h3', 'bp_h3');


// Shortcode for H4
function bp_h4( $atts, $content = null ) {
  extract( shortcode_atts( array(
    'color' => '',
    'weight' => '',
    'align' => '',
  ), $atts ) );
  $output='<h4 ';

  if($align!='') $output.= 'class="'.$align.'"'; 
  if($color!='' || $weight!='') $output.= 'style="'; //Open Style
      if($color!='') $output.= 'color:'.$color.';';
      if($weight!='') $output.= 'font-weight:'.$weight.';';
  if($color!='' || $weight!='') $output.='"'; //Close Style
  $output.='>'.do_shortcode($content).'</h4>';
  return $output;
}
add_shortcode('h4', 'bp_h4');


// Shortcode for H5
function bp_h5( $atts, $content = null ) {
  extract( shortcode_atts( array(
    'color' => '',
    'weight' => '',
    'align' => '',
  ), $atts ) );
  $output='<h5 ';

  if($align!='') $output.= 'class="'.$align.'"'; 
  if($color!='' || $weight!='') $output.= 'style="'; //Open Style
      if($color!='') $output.= 'color:'.$color.';';
      if($weight!='') $output.= 'font-weight:'.$weight.';';
  if($color!='' || $weight!='') $output.='"'; //Close Style
  $output.='>'.do_shortcode($content).'</h5>';
  return $output;
}
add_shortcode('h5', 'bp_h5');

// Shortcode for H6
function bp_h6( $atts, $content = null ) {
  extract( shortcode_atts( array(
    'color' => '',
    'weight' => '',
    'align' => '',
  ), $atts ) );
  $output='<h6 ';

  if($align!='') $output.= 'class="'.$align.'"'; 
  if($color!='' || $weight!='') $output.= 'style="'; //Open Style
      if($color!='') $output.= 'color:'.$color.';';
      if($weight!='') $output.= 'font-weight:'.$weight.';';
  if($color!='' || $weight!='') $output.='"'; //Close Style
  $output.='>'.do_shortcode($content).'</h6>';
  return $output;
}
add_shortcode('h6', 'bp_h6');



// Divider
function bp_divider($atts, $content=null) {
  extract( shortcode_atts( array(
    'size' => '',
  ), $atts ) );
  $output='<div class="divider ';

  if($size!='') $output.= ''.$size.''; 
   $output.='"></div>';
  return $output;
}
add_shortcode('divider', 'bp_divider');



// Images
function bp_image( $atts, $content = null ) {
    extract( shortcode_atts( array(
    'title' => '',
    ), $atts )
    );
    return '<img src="'.do_shortcode($content).'" alt="" />';
}
add_shortcode( 'image', 'bp_image' );


// Big Social Icons 
function bp_big_social($atts, $content=null) {
   return '<div class="big_social">'.do_shortcode($content).'</div><!--big_social-->';
}
add_shortcode('big_social', 'bp_big_social');

// Skills 
function bp_skills($atts, $content=null) {
   return '<div id="skill">'.do_shortcode($content).'</div><!--skills-->';
}
add_shortcode('skills', 'bp_skills');





// Carousel slider
function bp_carousel($atts, $content=null) {
    extract( shortcode_atts( array(
    'number' => '',
    ), $atts )
    );
   $output='<div class="slidewrap';
      if($number!='') $output.= ''.$number.''; 
   $output.='">';
   $output.='<div class="slider">'.do_shortcode($content).'</div>';
   $output.='</div>';//Sliderwrap
  
  return $output;
}
add_shortcode('carousel', 'bp_carousel');


// Carousel slides
function bp_slides($atts, $content=null) {
   return '<div class="slide">'.do_shortcode($content).'</div>';
}
add_shortcode('slide', 'bp_slides');

// Circle
function bp_circle_services($atts, $content=null) {
   return '<div class="circle">
              <div class="services_img_circle">
               '.do_shortcode($content).'
              </div>
          </div>';
}
add_shortcode('circle_services', 'bp_circle_services');


//Tabs Triggers Container

function bp_tabs_triggers($atts, $content=null) {
   return '<div class="tabs_triggers">'.do_shortcode($content).'</div>';
}
add_shortcode('tabs_triggers', 'bp_tabs_triggers');

// Tab triggers 
function bp_trigger($atts, $content=null) {
    extract( shortcode_atts( array(
    'text' => '',
    'link' => '',
    'last' => '',
    'active' =>'',
    ), $atts )
    );

    $output='
    <a href="#'.$link.'">
      <div class="services_tab skin';
          if($active!='') $output.= ' active';
          if($last!='') $output.= ' last';
      $output.='">';

      $output.='
      <div class="center">'.do_shortcode($content).'</div>
      
      <div class="descripcion_services">
          <p>'.$text.'</p>
      </div>
      </div>
    </a>';//href

    return $output;
    }
    add_shortcode('trigger', 'bp_trigger');


//Services Tab Container

function bp_tabs_container($atts, $content=null) {
   return '<div class="services_tab_container skin">'.do_shortcode($content).'</div>';
}
add_shortcode('service_tab_group', 'bp_tabs_container');


// Services tabs  
function bp_services_tabs($atts, $content=null) {
    extract( shortcode_atts( array(
    'text' => '',
    'id' => '',
    'column' => '',
    'active' =>'',
    ), $atts )
    );

    $output='<div id="'.$id.'" class="tab_content"><div class="row">';
    $output.=''.do_shortcode($content).'';
    $output.='</div></div>';
    return $output;
    }
    add_shortcode('tab', 'bp_services_tabs');


// Services Tabs content
function bp_tabs_content($atts, $content=null) {
    extract( shortcode_atts( array(
    'text' => '',
    'id' => '',
    'column' => '',
    'active' =>'',
    ), $atts )
    );

    $output='
    <div class="column_wrapper">
      <div class="center">';
      $output.=''.do_shortcode($content).'';
      $output.='</div>
    <div class="divider small"></div>
    </div>';

    return $output;
    }
    add_shortcode('tab_content', 'bp_tabs_content');


// Shortcode for videos
function bp_video( $atts ) {
  extract( shortcode_atts( array(
    'type' => 'youtube',
    'url' => '',
    'width' => '100%',
    'height' => 'auto',
    'player_id' => 'player1',
    'style' => ''
  ), $atts ) );
  $output='<div class="video" style="width:'.$width.'; height:'.$height.'; '.$style.'">';
  if($type=='vimeo') {
    $output.='<iframe width="'.$width.'" height="'.$height.'" src="http://player.vimeo.com/video/'.$url.'?title=0&amp;byline=0&amp;portrait=0&amp;color=&amp;autoplay=0&amp;loop=0" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
  } else if($type=='youtube') {
    $output.='<iframe  width="'.$width.'" height="'.$height.'" src="http://www.youtube.com/embed/'.$url.'?autohide=2&amp;disablekb=0&amp;fs=1&amp;hd=0&amp;loop=0&amp;rel=0&amp;showinfo=0&amp;showsearch=0&amp;wmode=transparent" frameborder="0"></iframe>';
  }
  $output.='</div>';
  return $output;
}
add_shortcode('video', 'bp_video');

// Shortcode for blockquotes
function bp_blockquote( $atts, $content = null) {
  extract( shortcode_atts( array(
    'blockquote_style' => '',
    'align' => '',
    'text_align' => '',
    'cite' => '',
    'style' => ''
  ), $atts ) );
  $style_output='';
  if($style!='' || $text_align!='') {
    $style_output='style="';
  }
  if($style!='') {
    $style_output.=$style.' ';
  }
  if($text_align=='left' || $text_align=='right' || $text_align=='center' || $text_align=='justify') {
    $style_output.='text-align:'.$text_align.'; ';
  }
  if($text_align=='center') {
    $style_output.='background-position:top center; ';
  }
  if($style!='' || $text_align!='') {
    $style_output.='"';
  }
  $output='<blockquote class="';
  if($blockquote_style=='quote') {
    $output.=$blockquote_style.' ';
  }
  if($align=='left' || $align=='right') {
    $output.=$align;
  }
  $output.='" '.$style_output.'>';
  $content = do_shortcode( shortcode_unautop( $content ) );
  $content = preg_replace('#^<\/p>|<p>$#', '', $content);
  $output.=wpautop(wptexturize($content));
  if($cite!='') $output.='<small><cite title="'.$cite.'">'.$cite.'</cite></small>';
  $output.='</blockquote>';
  return $output;
}
add_shortcode('blockquote', 'bp_blockquote');



function bp_buttons( $atts, $content = null ) {
    extract(shortcode_atts(array(
    'link'  => '#',
    'target'  => '',
    'size'  => '',
    'align' => '',
    'color' => '',
    ), $atts));
 
  $target = ($target == '_blank') ? ' target="_blank"' : '';

  $out = '<a' .$target. ' class="button ' .$color.' '.$size.' '.$align. '" href="' .$link. '"><span>' .do_shortcode($content). '</span></a>';

    return $out;
}
add_shortcode('button', 'bp_buttons');

 ?>