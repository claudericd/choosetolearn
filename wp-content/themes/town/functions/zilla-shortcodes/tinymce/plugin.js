(function($) {
"use strict";   
 


 			//Shortcodes
            tinymce.PluginManager.add( 'zillaShortcodes', function( editor, url ) {

				editor.addCommand("zillaPopup", function ( a, params )
				{
					var popup = params.identifier;
					tb_show("Insert Zilla Shortcode", url + "/popup.php?popup=" + popup + "&width=" + 800);
				});
     
                editor.addButton( 'zilla_button', {
                    type: 'splitbutton',
                    icon: false,
					title:  'Zilla Shortcodes',
					onclick : function(e) {},
					menu: [
					
					{text: 'Columns',onclick:function(){
						editor.execCommand("zillaPopup", false, {title: 'asdasd',identifier: 'columns'})
					}},
					{text: 'Columns Bootstrap',onclick:function(){
						editor.execCommand("zillaPopup", false, {title: 'Buttons',identifier: 'bootstrap_columns'})
					}},


					{text: 'Headings',onclick:function(){
						editor.execCommand("zillaPopup", false, {title: 'Heading',identifier: 'heading'})
					}},
					{text: 'Page Title',onclick:function(){
						editor.execCommand("zillaPopup", false, {title: 'Page Title',identifier: 'page_title'})
					}},



					{text: 'Span',onclick:function(){
						editor.execCommand("zillaPopup", false, {title: 'Span',identifier: 'span'})
					}},
					{text: 'Divider',onclick:function(){
						editor.execCommand("zillaPopup", false, {title: 'Divider',identifier: 'divider'})
					}},
					{text: 'Image',onclick:function(){
						editor.execCommand("zillaPopup", false, {title: 'Image',identifier: 'image_url'})
					}},
					{text: 'Skills Bars',onclick:function(){
						editor.execCommand("zillaPopup", false, {title: 'Skills Bars',identifier: 'skills_bars'})
					}},




					{text: 'Team',onclick:function(){
						editor.execCommand("zillaPopup", false, {title: 'Team',identifier: 'team'})
					}},
					{text: 'Carousel',onclick:function(){
						editor.execCommand("zillaPopup", false, {title: 'Carousel',identifier: 'carousel'})
					}},
					
					{text: 'Video Youtube',onclick:function(){
						editor.execCommand("zillaPopup", false, {title: 'Video Youtube',identifier: 'video_youtube'})
					}},
					{text: 'Video Vimeo',onclick:function(){
						editor.execCommand("zillaPopup", false, {title: 'Video Vimeo',identifier: 'video_vimeo'})
					}},



					
					//List your shortcodes like this
					]

                
        	  });
         
          });
         

 
})(jQuery);