<?php

/*-----------------------------------------------------------------------------------*/
/*	Button Config
/*-----------------------------------------------------------------------------------*/

$zilla_shortcodes['title_bar'] = array(
	'no_preview' => true,
	'params' => array(
		'sub_heading' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Sub Heading', 'textdomain'),
			'desc' => __('Add Sub Heading', 'textdomain')
		),
		'heading' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Heading', 'textdomain'),
			'desc' => __('Add Heading', 'textdomain')
		)
		
	),
	'shortcode' => '[title_bar sub_heading="{{sub_heading}}" heading="{{heading}}"]',
	'popup_title' => __('Insert Title Bar', 'textdomain')
);


/*-----------------------------------------------------------------------------------*/
/*	Link
/*-----------------------------------------------------------------------------------*/

$zilla_shortcodes['excerpt_link'] = array(
	'no_preview' => true,
	'params' => array(
		'title' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Display Text', 'textdomain'),
			'desc' => __('Add Text to Display.', 'textdomain')
		),
		'url' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('URL', 'textdomain'),
			'desc' => __('Add URL here.', 'textdomain')
		)
		
	),
	'shortcode' => '[link title="{{title}}"] {{url}} [/link]',
	'popup_title' => __('Insert link shortcode.', 'textdomain')
);

/************************************************************************
* MEMEBER INFO
*************************************************************************/

$zilla_shortcodes['member_bar'] = array(
	'no_preview' => true,
	'params' => array(
		'firstname' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('First Name', 'textdomain'),
			'desc' => __('And team members first name here.', 'textdomain')
		),
		'lastname' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Last Name', 'textdomain'),
			'desc' => __('And team members last name here.', 'textdomain')
		),
		'position' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Position', 'textdomain'),
			'desc' => __('Add team members position/rank here', 'textdomain')
		),
		
	),
	'shortcode' => '[member_info position="{{position}}" firstname="{{firstname}}" lastname="{{lastname}}"]',
	'popup_title' => __('Insert Member Bar', 'textdomain')
);

/************************************************************************
* Contact INFO
*************************************************************************/

$zilla_shortcodes['contact_info'] = array(
	'no_preview' => true,
	'params' => array(
		'name' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Name', 'textdomain'),
			'desc' => __('You can add name here.', 'textdomain')
		),
		'phone' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Phone', 'textdomain'),
			'desc' => __('Phone number can go here', 'textdomain')
		),
		'address' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Address', 'textdomain'),
			'desc' => __('Enter your address here', 'textdomain')
		),
		'email' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Email', 'textdomain'),
			'desc' => __('Email address here ie: you@yourmail.com', 'textdomain')
		),
		
	),
	'shortcode' => '[contact_info name="{{name}}" phone="{{phone}}" address="{{address}}" email="{{email}}"]',
	'popup_title' => __('Insert Contact Info', 'textdomain')
);


/************************************************************************
* Contact Form
*************************************************************************/

$zilla_shortcodes['nzs_contact'] = array(
	'no_preview' => true,
	'params' => array(
		'name_label' => array(
			'std' => 'Name',
			'type' => 'text',
			'label' => __('Name Label', 'textdomain'),
			'desc' => ''
		),
		'email_label' => array(
			'std' => 'Email',
			'type' => 'text',
			'label' => __('Email Label', 'textdomain'),
			'desc' => ''
		),
		'message_label' => array(
			'std' => 'Message',
			'type' => 'text',
			'label' => __('Message Label', 'textdomain'),
			'desc' => ''
		),
		'btn_label' => array(
			'std' => 'Send',
			'type' => 'text',
			'label' => __('Button Label', 'textdomain'),
			'desc' => ''
		),
		
	),
	'shortcode' => '[nzs_contact_form name_label="{{name_label}}" email_label="{{email_label}}" message_label="{{message_label}}" btn_label="{{btn_label}}"]',
	'popup_title' => __('Insert Contact Form', 'textdomain')
);

/************************************************************************
* Padding Box
*************************************************************************/

$zilla_shortcodes['padding_box'] = array(
	'no_preview' => true,
	'params' => array(
		'top' => array(
			'std' => '0',
			'type' => 'text',
			'label' => __('Top Padding', 'textdomain'),
			'desc' => __('Add a number for top padding.', 'textdomain')
		),
		'right' => array(
			'std' => '0',
			'type' => 'text',
			'label' => __('Right Padding', 'textdomain'),
			'desc' => __('Add a number for right padding.', 'textdomain')
		),
		'bottom' => array(
			'std' => '0',
			'type' => 'text',
			'label' => __('Bottom Padding', 'textdomain'),
			'desc' => __('Add a number for bottom padding.', 'textdomain')
		),
		'left' => array(
			'std' => '0',
			'type' => 'text',
			'label' => __('Left Padding', 'textdomain'),
			'desc' => __('Add a number for left padding.', 'textdomain')
		),
		'content' => array(
			'std' => '',
			'type' => 'textarea',
			'label' => __('Content', 'textdomain'),
			'desc' => __('Add content here.', 'textdomain')
		)
		
	),
	'shortcode' => '[padding_box top="{{top}}" right="{{right}}" bottom="{{bottom}}" left="{{left}}"] {{content}} [/padding_box]',
	'popup_title' => __('Insert Padding Box', 'textdomain')
);


/************************************************************************
* Service
*************************************************************************/

$zilla_shortcodes['service'] = array(
	'no_preview' => true,
	'params' => array(
		'title' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Heading', 'textdomain'),
			'desc' => __('Enter a heading here.', 'textdomain')
		),
		'icon_url' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Icon Image', 'textdomain'),
			'desc' => __('Enter Image URL for icon/image.', 'textdomain')
		),
		'content' => array(
			'std' => '',
			'type' => 'textarea',
			'label' => __('Description', 'textdomain'),
			'desc' => __('Add a description here.', 'textdomain')
		)
		
	),
	'shortcode' => '[nzs_service title="{{title}}" icon_url="{{icon_url}}"] {{content}} [/nzs_service]',
	'popup_title' => __('Insert Service', 'textdomain')
);

/************************************************************************
* Heading
*************************************************************************/
$zilla_shortcodes['heading'] = array(
	'no_preview' => true,
	'params' => array(
		'title' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Heading', 'textdomain'),
			'desc' => __('Enter a heading here.', 'textdomain')
		),
		'color' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Color', 'textdomain'),
			'desc' => __('Enter a color here ex: #D9D9D9.', 'textdomain')
		),
		'align' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Align', 'textdomain'),
			'desc' => __('Enter your align ex: center, right, left.', 'textdomain')
		),
		'class' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Class', 'textdomain'),
			'desc' => __('Enter a custom class ex: uppercase', 'textdomain')
		),
		'weight' => array(
		'type' => 'select',
		'label' => __('Font Weight', 'textdomain'),
		'desc' => __('Enter a font weight.', 'textdomain'),
		'options' => array(
				''=> 'normal',
				'bold'=> 'bold',
			)
		
		),

		'headings' => array(
		'type' => 'select',
		'label' => __('Heading  Tag', 'textdomain'),
		'desc' => __('Select a heading tag.', 'textdomain'),
		'options' => array(
				'h1'=> 'H1',
				'h2'=> 'H2',
				'h3'=> 'H3',
				'h4'=> 'H4',
				'h5'=> 'H5',
				'h6'=> 'H6',
			)
		
		),
	),
	'shortcode' => '[{{headings}} color="{{color}}" weight="{{weight}}" align="{{align}}" class="{{class}}"] {{title}} [/{{headings}}]',
	'popup_title' => __('Insert Heading', 'textdomain')
);

/************************************************************************
* Video Youtube
*************************************************************************/

$zilla_shortcodes['video_youtube'] = array(
	'no_preview' => true,
	'params' => array(
		'video_link' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Video Link', 'textdomain'),
			'desc' => __('Enter Youtube embed code, paste just the red part, to find the embed code go to your youtube video and click in "Share this video": http://youtu.be/<span style="color:red">T6DJcgm3wNY</span>', 'textdomain')
		),
		'height' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Height', 'textdomain'),
			'desc' => __('Enter the height, just the number, like "500"', 'textdomain')
		),
		
	),

	'shortcode' => '[video type="youtube" height="{{height}}" url="{{video_link}}"][/video]',
	'popup_title' => __('Insert Video', 'textdomain')
);


/************************************************************************
* Video Vimeo
*************************************************************************/

$zilla_shortcodes['video_vimeo'] = array(
	'no_preview' => true,
	'params' => array(
		'video_link' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Video Link', 'textdomain'),
			'desc' => __('Enter your Vimeo ID code, paste just the red part.": http://vimeo.com/<span style="color:red">58396623</span>', 'textdomain')
		),
		'height' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Height', 'textdomain'),
			'desc' => __('Enter the height, just the number, like "500"', 'textdomain')
		),
		
	),

	'shortcode' => '[video type="vimeo" height="{{height}}" url="{{video_link}}"][/video]',
	'popup_title' => __('Insert Video', 'textdomain')
);



/************************************************************************
* Divider 
*************************************************************************/

$zilla_shortcodes['divider'] = array(
	'no_preview' => true,
	'params' => array(
		'size' => array(
			'std' => '',
			'type' => 'select',
			'label' => __('Divider', 'textdomain'),
			'desc' => __('Select the size of your divider', 'textdomain'),
			'options' => array(
				'normal' => 'Normal',
				'big' => 'Big',
				'small' => 'Small'
			)
		),
		
	),
	'shortcode' => '[clear][divider size="{{size}}"]',
	'popup_title' => __('Insert Divider', 'textdomain')
);

/************************************************************************
* test 
*************************************************************************/

$zilla_shortcodes['alert'] = array(
	'no_preview' => true,
	'params' => array(
		'size' => array(
			'std' => '',
			'type' => 'select',
			'label' => __('Divider', 'textdomain'),
			'desc' => __('Select the size of your divider', 'textdomain'),
			'options' => array(
				'normal' => 'Normal',
				'big' => 'Big',
				'small' => 'Small'
			)
		),
		
	),
	'shortcode' => '[clear][divider size="{{size}}"]',
	'popup_title' => __('Insert Divider', 'textdomain')
);

/************************************************************************
* Light Box
*************************************************************************/
$zilla_shortcodes['light_box'] = array(
	'no_preview' => true,
	'params' => array(
		'thumb' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Thumb Image', 'textdomain'),
			'desc' => __('Add thumbnail src', 'textdomain')
		),
		'image' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Full Image', 'textdomain'),
			'desc' => __('Add Image src', 'textdomain')
		),
		'alt' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Alt Text', 'textdomain'),
			'desc' => __('Add alt text', 'textdomain')
		),
		'align' => array(
			'type' => 'select',
			'label' => __('Align', 'textdomain'),
			'desc' => __('Select alignment.', 'textdomain'),
			'options' => array(
				'center' => 'Center',
				'left' => 'Left',
				'right' => 'Right'
			)
		)
	),
	'shortcode' => '[lightbox thumb_img="{{thumb}}" full_img="{{image}}" alt_text="{{alt}}" align="{{align}}"]',
	'popup_title' => __('Insert Light Box Shortcode', 'textdomain')
);





/*-----------------------------------------------------------------------------------*/
/*	Button Config
/*-----------------------------------------------------------------------------------*/

$zilla_shortcodes['button'] = array(
	'no_preview' => true,
	'params' => array(
		'url' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Button URL', 'textdomain'),
			'desc' => __('Add the button\'s url eg http://example.com', 'textdomain')
		),
		'style' => array(
			'type' => 'select',
			'label' => __('Button Style', 'textdomain'),
			'desc' => __('Select the button\'s style, ie the button\'s colour', 'textdomain'),
			'options' => array(
				'main-btn' => 'Default',
				'gray' => 'Gray',
				'black' => 'Black',
				'green' => 'Green',
				'yellow' => 'Yellow',
				'blue' => 'Blue',
				'red' => 'Red',
				'orange' => 'Orange',
	
			)
		),
		'target' => array(
			'type' => 'select',
			'label' => __('Button Target', 'textdomain'),
			'desc' => __('_self = open in same window. _blank = open in new window', 'textdomain'),
			'options' => array(
				'_self' => '_self',
				'_blank' => '_blank',
			)
		),
		'content' => array(
			'std' => 'Button Text',
			'type' => 'text',
			'label' => __('Button\'s Text', 'textdomain'),
			'desc' => __('Add the button\'s text', 'textdomain'),
		)
	),
	'shortcode' => '[button link="{{url}}" color="{{style}}" target="{{target}}"] {{content}} [/button]',
	'popup_title' => __('Insert Button Shortcode', 'textdomain')


);



/*-----------------------------------------------------------------------------------*/
/*	Columns Config
/*-----------------------------------------------------------------------------------*/

$zilla_shortcodes['columns'] = array(
	'params' => array(),
	'shortcode' => ' {{child_shortcode}} ', // as there is no wrapper shortcode
	'popup_title' => __('Insert Columns Shortcode', 'textdomain'),
	'no_preview' => true,
	
	// child shortcode is clonable & sortable
	'child_shortcode' => array(
		'params' => array(
			'column' => array(
				'type' => 'select',
				'label' => __('Column Type', 'textdomain'),
				'desc' => __('Select the type, ie width of the column.', 'textdomain'),
				'options' => array(
					'one_column' => 'One Column',
					'two_column' => 'Two Columns',
					'three_column' => 'Three Columns',
					'four_column' => 'Four Columns',
					'six_column' => 'Six Columns',
					
				)
			),
			'content' => array(
				'std' => '',
				'type' => 'textarea',
				'label' => __('Column Content', 'textdomain'),
				'desc' => __('Add the column content.', 'textdomain'),
			)
		),
		'shortcode' => '[{{column}}] {{content}} [/{{column}}] ',
		'clone_button' => __('Add Column', 'textdomain')
	)
);

/*-----------------------------------------------------------------------------------*/
/*	Columns Bootstrap Span 
/*-----------------------------------------------------------------------------------*/

$zilla_shortcodes['bootstrap_columns'] = array(
	'params' => array(),
	'shortcode' => ' {{child_shortcode}} ', // as there is no wrapper shortcode
	'popup_title' => __('Insert Columns Shortcode', 'textdomain'),
	'no_preview' => true,
	
	// child shortcode is clonable & sortable
	'child_shortcode' => array(
		'params' => array(
			'column' => array(
				'type' => 'select',
				'label' => __('Column Type', 'textdomain'),
				'desc' => __('If you are familiar with bootstrap framework you can use SPAN columns.', 'textdomain'),
				'options' => array(
					'12' => 'Span12',
					'11' => 'Span11',
					'10' => 'Span10',
					'9' => 'Span9',
					'8' => 'Span8',
					'7' => 'Span7',
					'6' => 'Span6',
					'5' => 'Span5',
					'4' => 'Span4',
					'3' => 'Span3',
					'2' => 'Span2',
					'1' => 'Span1',
					
					
				)
			),
			'content' => array(
				'std' => '',
				'type' => 'textarea',
				'label' => __('Column Content', 'textdomain'),
				'desc' => __('Add the column content.', 'textdomain'),
			)
		),
		'shortcode' => '[column size="{{column}}"] {{content}} [/column] ',
		'clone_button' => __('Add Column', 'textdomain')
	)
);





/*-----------------------------------------------------------------------------------*/
/* 	Span 
/*-----------------------------------------------------------------------------------*/

$zilla_shortcodes['span'] = array(
	'no_preview' => true,
	'params' => array(
		'color' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Color', 'textdomain'),
			'desc' => __('Enter a color here ex: #D9D9D9.', 'textdomain')
		),
		'class' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Class', 'textdomain'),
			'desc' => __('Enter a color here ex: #D9D9D9.', 'textdomain')
		),
		'weight' => array(
		'type' => 'select',
		'label' => __('Font Weight', 'textdomain'),
		'desc' => __('Select a heading tag.', 'textdomain'),
		'options' => array(
				''=> 'normal',
				'bold'=> 'bold',
			)
		),
		'content' => array(
			'std' => '',
			'type' => 'textarea',
			'label' => __('Content', 'textdomain'),
			'desc' => __('Add a content here.', 'textdomain')
		),

	),
	'shortcode' => '[span color="{{color}}" weight="{{weight}}" class="{{class}}"] {{content}} [/span]',
	'popup_title' => __('Insert Span', 'textdomain')
);




/*-----------------------------------------------------------------------------------*/
/* 	Page title 
/*-----------------------------------------------------------------------------------*/

$zilla_shortcodes['page_title'] = array(
	'no_preview' => true,
	'params' => array(
		'page_title' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Page title', 'textdomain'),
			'desc' => __('Enter your page title here', 'textdomain')
		),
		'subtitle' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Page Subtitle', 'textdomain'),
			'desc' => __('Enter your page sub  t itle here', 'textdomain')
		),
	

	),
	'shortcode' => '[page_title] {{page_title}} [/page_title][page_subtitle] {{subtitle}} [/page_subtitle][page_line]',

	'popup_title' => __('Insert Page Title', 'textdomain')
);



/*-----------------------------------------------------------------------------------*/
/* 	Image
/*-----------------------------------------------------------------------------------*/

$zilla_shortcodes['image_url'] = array(
	'no_preview' => true,
	'params' => array(
		'image_url' => array(
			'std' => '',
			'type' => 'text',
			'label' => __('Url of the image', 'textdomain'),
			'desc' => __('Enter a valid URL here', 'textdomain')
		),
		
	

	),
	'shortcode' => '[image]{{image_url}}[/image]',

	'popup_title' => __('Insert a URL', 'textdomain')
);



/*-----------------------------------------------------------------------------------*/
/*	Skills Bars
/*-----------------------------------------------------------------------------------*/

$zilla_shortcodes['skills_bars'] = array(
	'params' => array(),
	'shortcode' => '[skills] {{child_shortcode}} [/skills]', // as there is no wrapper shortcode
	'popup_title' => __('Insert Columns Shortcode', 'textdomain'),
	'no_preview' => true,
	
	// child shortcode is clonable & sortable
	'child_shortcode' => array(
		'params' => array(
			'percent_bar' => array(
				'type' => 'select',
				'label' => __('Column Type', 'textdomain'),
				'desc' => __('Select the width of the bar of the skill', 'textdomain'),
				'options' => array(
					'10%' => '10%',
					'20%' => '20%',
					'30%' => '30%',
					'40%' => '40%',
					'50%' => '50%',
					'60%' => '60%',
					'70%' => '70%',
					'80%' => '80%',
					'90%' => '90%',
					'100%' => '100%',
				)
			),
			'skill_title' => array(
				'std' => '',
				'type' => 'text',
				'label' => __('Skill bar title', 'textdomain'),
				'desc' => __('Add a title', 'textdomain'),
			)
		),
		'shortcode' => '[h4]{{skill_title}}[/h4][skill_bar]{{percent_bar}}[/skill_bar]',
		'clone_button' => __('Add New Skill bar', 'textdomain')
	)
);


/*-----------------------------------------------------------------------------------*/
/*	Team
/*-----------------------------------------------------------------------------------*/

$zilla_shortcodes['team'] = array(
	'no_preview' => true,
	'params' => array(

	),
	'shortcode' => '[team_container]<br>[four_column][teammate image="URL OF YOUR IMAGE" name="MARK THOMAS" job="CEO" pinterest_url="#" skype_url="#" twitter_url="#" dribbble_url="#"][/four_column][four_column][teammate image="URL OF YOUR IMAGE" name="MARK THOMAS" job="CEO" pinterest_url="#" skype_url="#" twitter_url="#" dribbble_url="#"][/four_column][four_column][teammate image="URL OF YOUR IMAGE" name="MARK THOMAS" job="CEO" pinterest_url="#" skype_url="#" twitter_url="#" dribbble_url="#"][/four_column][four_column][teammate image="URL OF YOUR IMAGE" name="MARK THOMAS" job="CEO" pinterest_url="#" skype_url="#" twitter_url="#" dribbble_url="#"][/four_column]<br>[/team_container]
',
	'popup_title' => __('After insert, make sure to complete the shortcode with your info', 'textdomain')
);

/*-----------------------------------------------------------------------------------*/
/*	Team
/*-----------------------------------------------------------------------------------*/

$zilla_shortcodes['carousel'] = array(
	'no_preview' => true,
	'params' => array(
		
		
	),
	'shortcode' => '[carousel number="NUMBER OF CAROUSEL EX 1,2,3,4"]<br><br>[slide] YOUR FIRST CONTENT HERE [/slide]<br><br>[slide] YOUR SECOND CONTENT HERE [/slide]<br><br>[/carousel]
',
	'popup_title' => __('Make sure to edit NUMBER OF CAROUSELS after insert the shortcode', 'textdomain')
);


/************************************************************************
* Slider
*************************************************************************/

$zilla_shortcodes['nzs_slider'] = array(
	'params' => array(),
	'shortcode' => '[nzs_slider] {{child_shortcode}} [/nzs_slider]', // as there is no wrapper shortcode
	'popup_title' => __('Insert Slider Shortcode', 'textdomain'),
	'no_preview' => true,
	
	// child shortcode is clonable & sortable
	'child_shortcode' => array(
		'params' => array(
			'title' => array(
				'std' => '',
				'type' => 'text',
				'label' => __('Alt Text', 'textdomain'),
				'desc' => __('You can add title text here.', 'textdomain'),
			),
			'slide_img' => array(
				'std' => '',
				'type' => 'text',
				'label' => __('Slide Image', 'textdomain'),
				'desc' => __('Add image URL for slider.', 'textdomain'),
			),
			'f_img' => array(
				'std' => '',
				'type' => 'text',
				'label' => __('Large Image', 'textdomain'),
				'desc' => __('Add large image URL (optional)', 'textdomain'),
			)
		),
		'shortcode' => '[nzs_slides title="{{title}}" full_img="{{f_img}}" slide_img="{{slide_img}}"] ',
		'clone_button' => __('Add Another Slide', 'textdomain')
	)
);

$zilla_shortcodes['nzs_price'] = array(
	'params' => array(
		'title' => array(
				'std' => 'Basic',
				'type' => 'text',
				'label' => __('Plan Name', 'textdomain'),
				'desc' => __('You can enter a first name for plan here.', 'textdomain'),
			),
		'sub_title' => array(
				'std' => 'Plan',
				'type' => 'text',
				'label' => __('Sub Name', 'textdomain'),
				'desc' => __('You can enter a last name for plan here.', 'textdomain'),
			),
		'featured' => array(
			'type' => 'select',
			'label' => __('Featured?', 'textdomain'),
			'desc' => __('Select rather this plan is featured.', 'textdomain'),
			'options' => array(
				'' => 'Not Featured',
				'best' => 'Featured'
			)
		),
		'link' => array(
				'std' => 'http://',
				'type' => 'text',
				'label' => __('Button Link', 'textdomain'),
				'desc' => __('Enter a link for button here.', 'textdomain'),
			),
		'btn_text' => array(
				'std' => 'Buy Now!',
				'type' => 'text',
				'label' => __('Button Text', 'textdomain'),
				'desc' => __('What would you like button to say?', 'textdomain'),
			),
		),
	'shortcode' => '[price_plan first_word="{{title}}" last_word="{{sub_title}}" link="{{link}}" featured="{{featured}}" btn_text="{{btn_text}}"]  {{child_shortcode}} [/price_plan]', // as there is no wrapper shortcode
	'popup_title' => __('Pricing Plan Shortcode', 'textdomain'),
	'no_preview' => true,
	
	// child shortcode is clonable & sortable
	'child_shortcode' => array(
		'params' => array(
			'content' => array(
				'std' => '',
				'type' => 'text',
				'label' => __('Plan Option', 'textdomain'),
				'desc' => __('Enter plan option info', 'textdomain'),
			)
		),
		'shortcode' => '[price_option] {{content}} [/price_option]',
		'clone_button' => __('Add Plan Option', 'textdomain'),
		
	)
);
?>
