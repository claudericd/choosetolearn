(function ()
{
	// create zillaShortcodes plugin
	tinymce.create("tinymce.plugins.zillaShortcodes",
	{
		init: function ( ed, url )
		{
			ed.addCommand("zillaPopup", function ( a, params )
			{
				var popup = params.identifier;
				
				// load thickbox
				tb_show("Insert Shortcode", url + "/popup.php?popup=" + popup + "&width=" + 800);
			});
		},
		createControl: function ( btn, e )
		{
			if ( btn == "zilla_button" )
			{	
				var a = this;
				
				var btn = e.createSplitButton('zilla_button', {
                    title: "Insert Shortcode",
					image: ZillaShortcodes.plugin_folder +"/tinymce/images/icon.png",
					icons: false
                });

                btn.onRenderMenu.add(function (c, b)
				{	
					c=b.addMenu({title: "Columns"});
						a.addWithPopup( c, "Columns", "columns");
						a.addWithPopup( c, "Bootstrap Span", "bootstrap_columns" );

					a.addWithPopup( b, "Heading", "heading" );
					


					a.addImmediate( b, "Page Title", "[page_title] PUT YOUR PAGE TITLE HERE [/page_title][page_subtitle] PUT YOUR PAGE SUBTITLE HERE [/page_subtitle][page_line]" );
					

					a.addWithPopup( b, "Span", "span");
					a.addWithPopup( b, "Divider", "divider");
					a.addImmediate( b, "Image", "[image] URL of your image [/image]");
					a.addImmediate( b, "Skills Bars", "[skills][h4]SEO[/h4][skill_bar]60%[/skill_bar][h4]Jquery[/h4][skill_bar]80%[/skill_bar][h4]WordPress[/h4][skill_bar]95%[/skill_bar][h4]HTML5[/h4][skill_bar]90%[/skill_bar][h4]CSS3[/h4][skill_bar]80%[/skill_bar][/skills]");
					a.addImmediate( b, "Team", "[team_container]<br>[four_column][teammate image='URL OF YOUR IMAGE' name='MARK THOMAS' job='CEO' pinterest_url='#' skype_url='#' twitter_url='#' dribbble_url='#'][/four_column][four_column][teammate image='URL OF YOUR IMAGE' name='MARK THOMAS' job='CEO' pinterest_url='#' skype_url='#' twitter_url='#' dribbble_url='#'][/four_column][four_column][teammate image='URL OF YOUR IMAGE' name='MARK THOMAS' job='CEO' pinterest_url='#' skype_url='#' twitter_url='#' dribbble_url='#'][/four_column][four_column][teammate image='URL OF YOUR IMAGE' name='MARK THOMAS' job='CEO' pinterest_url='#' skype_url='#' twitter_url='#' dribbble_url='#'][/four_column]<br>[/team_container]");

					a.addImmediate( b, "Carousel", "[carousel number='NUMBER OF CAROUSEL EX 1,2,3,4']<br><br>[slide] YOUR FIRST CONTENT HERE [/slide]<br><br>[slide] YOUR SECOND CONTENT HERE [/slide]<br><br>[/carousel]" );
					
						
					c=b.addMenu({title: "Video"});
						a.addWithPopup( c, "Youtube", "video_youtube");
						a.addWithPopup( c, "Vimeo", "video_vimeo" );
				
					//c=b.addMenu({title: "Pricing Table"});
					//a.addImmediate( c, "Pricing Table Holder", "[price_table] Put Plans In Here [/price_table]");
							
					a.addWithPopup( b, "Buttons", "button" );
					a.addImmediate( b, "Buttons Rectangle", "[button_rec link='#' text='Read More']" );
					a.addImmediate( b, "Clear Floats", "[clear]" );
				

				});
                
                return btn;
			}
			
			return null;
		},
		addWithPopup: function ( ed, title, id ) {
			ed.add({
				title: title,
				onclick: function () {
					tinyMCE.activeEditor.execCommand("zillaPopup", false, {
						title: title,
						identifier: id
					})
				}
			})
		},
		addImmediate: function ( ed, title, sc) {
			ed.add({
				title: title,
				onclick: function () {
					tinyMCE.activeEditor.execCommand( "mceInsertContent", false, sc )
				}
			})
		},
		getInfo: function () {
			return {
				longname: 'Zilla Shortcodes',
				author: 'Orman Clark',
				authorurl: 'http://themeforest.net/user/ormanclark/',
				infourl: 'http://wiki.moxiecode.com/',
				version: "1.0"
			}
		}
	});
	
	// add zillaShortcodes plugin
	tinymce.PluginManager.add("zillaShortcodes", tinymce.plugins.zillaShortcodes);
})();