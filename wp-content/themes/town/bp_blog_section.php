
<?php 
$bp_myOptions = get_option('bp_framework');
$meta = get_post_meta($post->ID,'metas',true);
$thumbnail_object = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large');
$post_per_page ='';
if(!empty($bp_myOptions['post_per_page'])) { 
	$post_per_page = $bp_myOptions['post_per_page'];
}
?>




<div id="<?php echo the_slug(); ?>" class="section" <?php if(!empty($thumbnail_object[0])) {?> style="background:url('<?php echo $thumbnail_object[0] ?> ')  <?php }?> ">
	<div class="container">
		<div class="row">
			<div class="blog_items_container">
			<?php the_content(); ?>
			
<?php 	$blog_post_counter = 0 ;	?>



<?php

$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

query_posts(array(
	'post_type'      => 'post', // You can add a custom post type if you like
	'paged'          => $paged,
	'posts_per_page' => $post_per_page,
));

if ( have_posts() ) : ?>

<?php while ( have_posts() ) : the_post(); ?>


<?php 
$home=get_post_meta($post->ID, 'home', true);

$image_attributes = get_post_meta($post->ID, 'post_main_img', true);
$thumbnail_object_blog = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large');
$user_email = get_the_author_meta('user_email');

 ?>



 <div class="blog_container">
				<div class="span4">
					<div class="blog-item">
							 <?php if(!empty($thumbnail_object_blog[0])) {?> 


              <div class="img-container-blog" style="background-image: url(<?php echo $thumbnail_object_blog[0] ?>);">
                <div class="the-author-img">
                   <?php echo get_avatar( $user_email, $size = '50'); ?>
                </div>
              </div>


            <?php } ?>
							<div class="blog-boddy">
								<div class="the-title"><h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1></div>
								<p><?php the_excerpt(); ?></p>
								<div class="metas">
									<div class="the-date"><a href="#"><span class="calendar gray icon"></span><?php the_time('F j, Y'); ?></a></div>
									<div class="the-comments"><a href="#"><span class="comments gray icon"></span><?php comments_popup_link( '0', '1', '%', 'wp_comments_xiara' )?></a></div>
									<a href="<?php the_permalink(); ?>" class="read_more_small"><i class="fa_icon icon-plus"></i></a>
									<div class="clear"></div>
								</div><!--metas-->
							</div>
						</div><!--blog-item-->
				</div><!--span4-->
	</div>


	<?php $blog_post_counter ++; ?>
<?php
if ($blog_post_counter == 3) {
  echo "<div class='clear'></div>";
   	$blog_post_counter = 0;
}
?>


<?php endwhile; ?>





<?php

$argss = array(
'post_type' => 'page',
'meta_query' => array(
array(
'key' => 'metas',
'value' => 'blog_full_page', // Value on Meta Box 
'compare' => 'LIKE' 
)
)
);

// The Query
query_posts( $argss );

// The Loop
while ( have_posts() ) : the_post();
  

$blog_full_id = get_the_ID();

?>




<?php 

endwhile;

// Reset Query
wp_reset_query();
?>


        <div class="clear"></div>
        <div class="divider"></div>
<?php if(isset($bp_myOptions['show_blog_link_in_home']) && $bp_myOptions['show_blog_link_in_home'] == '1') { ?>
<div class="span12 center"><a href="<?php  if(!empty($blog_full_id)) { echo get_page_link($blog_full_id); }?>" class="button"><?php if(!empty($bp_myOptions['name_of_blog_button'])) {   echo $bp_myOptions['name_of_blog_button']; } ?></a></div>
<?php } ?>

<?php else : ?>

   
	
<?php endif; ?>


</div><!--blog_items_container-->
</div><!--row-->
</div><!--container-->
</div><!--blog-->

<?php wp_reset_query(); ?>