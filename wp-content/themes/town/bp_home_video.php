<?php $video_value = get_post_meta($post->ID, 'home_youtube', true); ?>

<!-- *** SLIDER STARTS *** -->
<div  id="<?php echo the_slug(); ?>" class="video_slide" style="padding-top: 50px;">
	<div class="video_caption">
		
		<?php the_content(); ?>

	</div>
	<div style="z-index: -99; width: 100%; height: 100%">
	  <iframe frameborder="0" height="100%" width="100%" src="<?php echo $video_value;?>">
	  </iframe>
	</div>
</div>
	<!-- *** SLIDER ENDS *** -->
