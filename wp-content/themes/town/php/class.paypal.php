<?
class PAYPAL {
	
	public $paypal = array();
	public $ipn = false;
	public $token = false;
	public $pdt = false;
	public $status = false;
	public $langue = 'fr';
	public $logfile = 'paypal.txt';
	
	public function paypal(){
		//$this->paypal_tables();
		$this->paypal_default();
		$this->langue = $_POST['langue'] ? $_POST['langue'] : $this->langue;
		$act = $_REQUEST['act'];
		$act = !$act&&$_REQUEST['paypal_checkout'] ? 'paypal_checkout' : $act;
		if(!empty($act) && preg_match('/^paypal_/',$act) && empty($_REQUEST['tx'])){
			$ac = preg_replace('/^paypal_/','',$act);
			call_user_func(array($this,$ac));
		}else if(!empty($_REQUEST['tx'])){
			$this->pdt();
		}
	}
	
	public function paypal_default(){
		$this->paypal['cmd'] = '_cart';
		$this->paypal['charset'] = 'utf-8';
		$this->paypal['return'] = 'http://'.$_SERVER['HTTP_HOST'];
		$this->paypal['notify_url'] = 'http://'.$_SERVER['HTTP_HOST'].'/wp/wp-content/themes/town/php/class.paypal.ipn.php';
		$this->paypal['business'] = 'info@choosetolearn.ca';
		$this->paypal['invoice'] = 'CTL-'.time();
		$this->paypal['upload'] = '1';
		$this->paypal['currency_code'] = 'CAD';
		$this->paypal['custom'] = 'l=fr&sid='.session_id();
		$this->token = '__hGJSkc6vRrXa7yVsjdFA0NU8CBFbafiQ6lvcgKTSXAcsUqrBcI2cTGO_K';
	}

	public function form($append = '<input type="submit" value="Choisir" class="submit" />'){
		$echo .= '<form enctype="multipart/form-data" action="?action=paypal_checkout" method="post" id="form-cart" class="form-cart">';
		$echo .= $this->form_paypal_input();
		$echo .= $append;
		$echo .= '</form>';
		echo $echo;
	}
	
	public function form_paypal_input(){
		foreach($this->paypal as $k => $v){
			if(array_key_exists($k,$_POST)){
				continue;
			}
			if($k=='business'){
				$v = urldecode($v);
			}
			$echo .= '<input type="hidden" name="'.$k.'" value="'.$v.'">';
		}
		return $echo;
	}
	
	public function checkout(){
		global $settings, $DB;
		header('Content-Type: text/html; charset=utf-8');
		$POSTS = $_POST;
		$POSTS['invoice_date'] = date('Y-m-d');
		//$POSTS['invoice'] = 'OMKI-1404327190';
		$this->paypal['custom'] = 'l='.$_POST['langue'].'&sid='.session_id();
		//$this->paypal['invoice'] = $_POST['invoice'];
		
		$IPN = '';
		
		$echo .= '<html><head>';
		$echo .= '<link rel="stylesheet" href="/wp/wp-content/themes/town/css/css.paypal.css" type="text/css" />';
		$echo .= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
		$echo .= '</head><body><div class="checkout">';
		$echo .= '<form action="https://www.paypal.com/cgi-bin/webscr" method="post" name="theform">';
		foreach($POSTS as $pk => $pv){
			$IPN .= '&'.$pk.'='.$pv;
			$echo .= '<input type="hidden" name="'.$pk.'" value="'.urldecode($pv).'" />';
		}
		$echo .= $this->form_paypal_input();
		$IPN = rawurlencode($IPN);
		
		$POSTS['invoice_date'] = date('d/m/Y');
		$POSTS['logo'] = 'http://'.$_SERVER['HTTP_HOST'].'/wp/wp-content/themes/town/images/logo.'.$_POST['langue'].'.jpg';
		$POSTS['newsletter'] = $POSTS['newsletter']!="" ? $POSTS['newsletter'] : ($_POST['langue']=='en'?'No, do not subscribe':'Non, ne pas inscrire') ;
		$this->save($POSTS);
		
		$echo .= '<img src="'.$POSTS['logo'].'" class="logo-checkout"><br>';
		$echo .= $this->__('Traitement en cours, veuillez patienter...');
		$echo .= '<br><img src="/wp/wp-content/themes/town/images/loading_checkout.gif" class="loading-checkout">';
		$echo .= '</form><script type="text/javascript">setTimeout("document.theform.submit();",3000);</script>';
		$echo .= '</div></body></html>';
		echo $echo;
		exit();
	}
	
	public function transaction_details($custom=array()){
		error_reporting(0);		
		ob_start();?>
        <? if($this->status){?>
        <p class="paypal-complete"><?=$this->__("Votre paiement a été complété.");?></p>
        <? }else{?>
        <p class="paypal-waiting"><?=$this->__("Votre paiement est en cours d'autorisation. Vous recevrez un courriel lorsqu'il sera complété.");?></p>
        <? }?>
		<? $echo = ob_get_contents();
		ob_end_clean();
		
		return $echo;
	}
	
	public function save($custom=array()){
		global $CART;
		// get template
		$template = file_get_contents(DOCUMENT_ROOT.'/wp/wp-content/themes/town/receipts/template.'.$this->langue.'.html');
		
		if(empty($template)){return false;}
		
		$template = $this->set_template($template,$custom);
		
		$file = DOCUMENT_ROOT.'/wp/wp-content/themes/town/receipts/'.$custom['invoice'].'.html';
		if(file_exists($file)){
			unlink($file);
		}
		
		if($fp = fopen($file,"w")){
			fwrite($fp,$template);
			fclose($fp);
		}else{
			echo 'ERROR : fopen '.$file;
		}
		
	}
	
	public function ipn_get_custom($name,$custom){
		$custom = rawurldecode($custom);
		$customs = explode('&',$custom);
		foreach($customs as $k => $v){
			$vars = explode('=',$v);
			if($vars[0]==$name){
				return $vars[1];
			}
		}
		return false;
	}
	public function pdt(){
		global $DB,$INVOICE, $PDT;
		//include_once($_SERVER['DOCUMENT_ROOT'].'/php/php_paypal_pdt.php');
		
		// PDT
		$req = 'cmd=_notify-synch';
		$tx_token = $_GET['tx'];
		$auth_token = $this->token;
		$req .= "&tx=$tx_token&at=$auth_token";
		$DB->logs('PDT req : '.$req);
		// post back to PayPal system to validate
		$header .= "POST /cgi-bin/webscr HTTP/1.1\r\n";
		$header .= "Content-Type: application/x-www-form-urlencoded\r\n";
		$header .= "Host: www.paypal.com\r\n";
		$header .= "Content-Length: " . strlen($req) . "\r\n";
		$header .= "Connection: close\r\n\r\n";
		$fp = fsockopen ('www.paypal.com', 80, $errno, $errstr, 30);
		
		if (!$fp) {
			// HTTP ERROR
		} else {
			fputs ($fp, $header . $req);
			// read the body data
			$res = '';
			$headerdone = false;
			while (!feof($fp)) {
				$line = fgets ($fp, 1024);
				if (strcmp($line, "\r\n") == 0) {
				// read the header
					$headerdone = true;
				}else if ($headerdone){
					// header has been read. now read the contents
					$res .= $line;
				}
			}
			$PDT = $res;
			// parse the data
			$lines = explode("\n", trim($res));
			$keyarray = array();
			if (strcmp ($lines[0], "SUCCESS") == 0) {
				$PAYPAL_STATUS = "SUCCESS";
				for ($i=1; $i<count($lines);$i++){
					list($key,$val) = explode("=", $lines[$i]);
					$keyarray[urldecode($key)] = urldecode($val);
				}
				//$this->update($keyarray);
				$INVOICE = $keyarray['invoice'];
				$this->pdt = $keyarray['invoice'];
				$this->status = strtolower($_GET['st'])=='completed' ? true : false;
			}else if (strcmp ($lines[0], "FAIL") == 0) {
				$PAYPAL_STATUS = "FAIL";
				$INVOICE = false;
			}
		}
		
		fclose ($fp);
		
		
		
		$DB->logs('-----PDT-----');
		$DB->logs('invoice : '.$INVOICE);
		$DB->logs('datas : '.$PDT);
		$DB->logs('Agent : '.$_SERVER['HTTP_USER_AGENT']);
		//$DB->query_db('DELETE FROM cart WHERE Session="'.session_id().'"');
	}
	public function update($custom=array()){
		$file = $_SERVER['DOCUMENT_ROOT'].'/wp/wp-content/themes/town/receipts/'.$custom['invoice'].'.html';
		$template = file_get_contents($file);	
		if(empty($template)){return false;}
		
		if(!preg_match('/'.preg_quote('|*txn_id*|').'/',$template)){return false;}
		
		$custom['invoice_date'] = date('d/m/Y');
		$template = $this->set_template($template,$custom);
		
		$fp = fopen($file,"w");
		fwrite($fp,$template);
		fclose($fp);
		
	}
	
	public function set_template($tmpl,$vars){
		$str = $tmpl;
		foreach($vars as $k => $v){
			$str = preg_replace('/'.preg_quote('|*'.$k.'*|').'/',$v,$str);
		}
		return $str;
	}
	
	public function send_invoice($datas,$to){
		global $DB, $MAIL;
		if(!$MAIL){return false;}
		
		$langue = $this->ipn_get_custom('l',$datas['custom']);
		
		$invoice = $datas['invoice'];
		$sujet = $langue=='fr' ? 'Inscription à une conférence' : 'Conference Suscription';
		
		$file = $_SERVER['DOCUMENT_ROOT'].'/wp/wp-content/themes/town/receipts/'.$invoice.'.html';
		$template = file_get_contents($file);
		
		$params = array();
		$params['message'] = $template;
		$params['sujet'] = $sujet;
		$params['to'] = $to;
		
		$b = $MAIL->send($params);
		
		if($b){
		}
	}
	
	public function notify($datas,$from){
		global $DB, $MAIL;
		if(!$MAIL){return false;}
		
		$langue = $this->ipn_get_custom('l',$datas['custom']);
		
		$invoice = $datas['invoice'];
		$sujet = $langue=='fr' ? 'Inscription à une conférence' : 'Conference Subscription';
		
		$file = $_SERVER['DOCUMENT_ROOT'].'/wp/wp-content/themes/town/receipts/'.$invoice.'.html';
		$template = file_get_contents($file);
		
		$params = array();
		$params['message'] = $template;
		$params['sujet'] = $sujet;
		$params['from'] = $from;
		
		$b = $MAIL->send($params);
		
		if($b){
		}
	}
	
	public function paypal_tables(){
		global $DB;
		$paypal_table = "paypal";
		$DB->query = "SELECT * FROM ".$paypal_table;
		if(!$DB->query_db()){
			$paypal_query = 'CREATE TABLE IF NOT EXISTS `'.$paypal_table.'` (';
			$paypal_query .= ' `Date` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL, ';
			$paypal_query .= ' `Date_transaction` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL, ';
			$paypal_query .= ' `Courriel` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL, '; //custom
			$paypal_query .= ' `Prenom` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL, '; //first_name
			$paypal_query .= ' `Nom` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL, '; //last_name
			$paypal_query .= ' `Facture` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL, '; //invoice
			$paypal_query .= ' `Commentaires` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL, '; //memo
			$paypal_query .= ' `Taxes` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL, '; //tax
			$paypal_query .= ' `Type_paiement` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL, '; //payment_type
			$paypal_query .= ' `Status_paiement` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL, '; //payment_status
			$paypal_query .= ' `Transaction_id` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL, '; //txn_id
			$paypal_query .= ' `Taux_conversion` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL, '; //exchange_rate
			$paypal_query .= ' `Frais_paypal` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL, '; //mc_fee
			$paypal_query .= ' `Montant_total` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL, '; //mc_gross
			$paypal_query .= ' `Raw` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL, '; //mc_gross
			$paypal_query .= ' `Custom` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL, '; //mc_gross
			$paypal_query .= ' `ID` INT NOT NULL AUTO_INCREMENT PRIMARY KEY';
			$paypal_query .= ' )';
			$paypal_query .= ' ENGINE = myisam;';
			$DB->query = $paypal_query;
			$DB->query_db();
		}
		
		// Check if table exist
		$paypal_table2 = "paypal_item";
		$DB->query = "SELECT * FROM ".$paypal_table2;
		$DB->query_db();
		if(!$DB->query_db()){
			$paypal_query2 = 'CREATE TABLE IF NOT EXISTS `'.$paypal_table2.'` (';
			$paypal_query2 .= ' `Item_name` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL, '; //item_name
			$paypal_query2 .= ' `Item_number` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL, '; //item_number
			$paypal_query2 .= ' `Item_quantity` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL, '; //quantity
			$paypal_query2 .= ' `Facture` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL, '; //invoice
			$paypal_query2 .= ' `Taxes` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL, '; //tax
			$paypal_query2 .= ' `Transaction_id` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL, '; //txn_id
			$paypal_query2 .= ' `Frais_paypal` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL, '; //mc_fee
			$paypal_query2 .= ' `Montant_total` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,'; //mc_gross
			$paypal_query2 .= ' `ID` INT NOT NULL AUTO_INCREMENT PRIMARY KEY';
			$paypal_query2 .= ' )';
			$paypal_query2 .= ' ENGINE = myisam;';
			$DB->query = $paypal_query2;
			$DB->query_db();
		}
	}
	
	public function __($str){
		$s = array();
		$s['en'] = array(
						 'Traitement en cours, veuillez patienter...' => 'Processing in progress... please wait...',
						 "Votre paiement est en cours d'autorisation. Vous recevrez un courriel lorsqu'il sera complété." => "Your payment is in progress. You will receive an email when completed.",
						 "Votre paiement a été complété." => "Your payment is completed."
						 );
		if(array_key_exists($this->langue,$s)){
			if(array_key_exists($str,$s[$this->langue])){
				$str = $s[$this->langue][$str];
			}
		}
		return $str;
	}
	
	
	/***********************************************************/
	/***********************************************************/
	public function logs($msg){
		$this->set_log(DOCUMENT_ROOT.'/wp/wp-content/themes/town/php/logs/'.$this->logfile, $msg);
	}
	public function reset_log(){
		if(file_exists($_SERVER['DOCUMENT_ROOT'].'/wp/wp-content/themes/town/php/logs/'.$this->logfile)){
			unlink(DOCUMENT_ROOT.'/wp/wp-content/themes/town/php/logs/'.$this->logfile);
		}
	}
	public function set_log($path, $desc){
		$data = "\n".date("Y-m-d G:i:s")." -> ";
		$data .= $desc; 
		$fp = fopen($path,"a");
		fwrite($fp,$data);
		fclose($fp);
	}
	
}

?>