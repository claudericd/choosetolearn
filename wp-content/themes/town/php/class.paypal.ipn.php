<?
header('Content-Type: text/html; charset=utf-8');
session_start();
include_once('class.mail.php');
include_once('class.paypal.php');
global $PAYPAL,$MAIL;
$MAIL = new _MAIL();
$PAYPAL = new PAYPAL();
$PAYPAL->logfile = $_REQUEST['txn_id'] ? $_REQUEST['txn_id'].'.'.$_REQUEST['txn_id']: $PAYPAL->logfile;


define('DOCUMENT_ROOT',$_SERVER['DOCUMENT_ROOT']);

//$PAYPAL->send_invoice(array('invoice'=>'CTL-1407959995','custom'=>'l=fr'),array('info@tsansfacon.com','Webmaster'));

$sandbox = false;
$saved = false;
$seller = $PAYPAL->paypal['business'];
// read the post from PayPal system and add 'cmd'
$req = 'cmd=_notify-validate';

foreach ($_POST as $key => $value) {
$value = urlencode(stripslashes($value));
$req .= "&$key=$value";
}
$PAYPAL->logs('-----IPN for '.$_POST['txn_id'].'-----');
$PAYPAL->logs('req : '.$req);
// post back to PayPal system to validate
$header .= "POST /cgi-bin/webscr HTTP/1.1\r\n";
$header .= "Content-Type: application/x-www-form-urlencoded\r\n";
$header .= "Host: www.paypal.com\r\n";
$header .= "Content-Length: " . strlen($req) . "\r\n";
$header .= "Connection: close\r\n\r\n";
$PAYPAL->logs('headers : '.$header );
if($sandbox){
	$fp = fsockopen ('ssl://www.sandbox.paypal.com', 443, $errno, $errstr, 30);
}else{
	$fp = fsockopen ('ssl://www.paypal.com', 443, $errno, $errstr, 30);
}

$res2 = '';
if (!$fp) {
	// HTTP ERROR
} else {
	fputs ($fp, $header . $req);
	while (!feof($fp)){
		$res = fgets ($fp, 1024);
		$res = trim($res); //NEW & IMPORTANT
		$res2 .= $res."\n";
		//error_log($res);
		$PAYPAL->logs($res.' :: '.preg_match('/^VERIFIED/',$res));
		if (preg_match('/^VERIFIED/',$res)) {
			$PAYPAL->logs("VERIFIED");
			// check that receiver_email is your Primary PayPal email
			$continu = $_POST['receiver_email']==$seller ? true : false;
			//error_log($_POST['receiver_email'].'=='.$seller);
			if(!$continu){ break; }
			
			$PAYPAL->logs('updating invoice');
			$PAYPAL->update($_POST);			
			
			$POSTS = $_POST;
			
			$to = $POSTS['payer_email'];
			
			$PAYPAL->logs('try invoice : '.$_POST['invoice'].', '.$_POST['payer_email']);
			$e = $PAYPAL->send_invoice($POSTS,array($to,$_POST['first_name'].' '.$_POST['last_name']));
			$PAYPAL->logs('send invoice : '.$e);
			
			$PAYPAL->logs('try notify : '.$_POST['invoice'].', '.$_POST['payer_email']);
			$e = $PAYPAL->notify($POSTS,array($to,$_POST['first_name'].' '.$_POST['last_name']));
			$PAYPAL->logs('send notify : '.$e);
			
		}else if (strcmp ($res, "INVALID") == 0) {
		// log for manual investigation
		}
	
		
	}
	$PAYPAL->logs('Keys : '.implode(', ',array_keys($_POST)));
	$PAYPAL->logs('Values : '.implode(', ',array_values($_POST)));
	$PAYPAL->logs('Socket : '.$res2);
	fclose ($fp);
}
?>
