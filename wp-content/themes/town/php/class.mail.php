<?

class _MAIL {
	
	public $sent = false;

	public function send($options = array()){
		$params = array('message' => '','sujet' => '','to' => array('info@choosetolearn.ca',"Choose to learn"),'from' => array('info@choosetolearn.ca',"Choose to learn"));
		$params = array_merge($params,$options);
		
		require_once('class.smtp.php');
		require_once('class.pop3.php');
		require_once('class.phpmailer.php');
		
		$mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch	
		$mail->IsSMTP(); // telling the class to use SMTP
		
		try {
		  $mail->Host       = "mail.pointome.net"; // SMTP server
		  $mail->SMTPDebug  = false;                     // enables SMTP debug information (for testing)
		  $mail->SMTPAuth   = true;                  // enable SMTP authentication
		  $mail->SMTPSecure = "tls";
		  $mail->CharSet 	= 'UTF-8';
		  $mail->Host       = "mail.pointome.net"; // sets the SMTP server
		  $mail->Port       = 26;                    // set the SMTP port for the GMAIL server
		  $mail->Username   = "smtpchoosetolearn@pointome.net"; // SMTP account username
		  $mail->Password   = "9EZl{U11J19J";        // SMTP account password
		  $mail->SetFrom($params['from'][0], $params['from'][1]);
		  
		  $mail->Subject = $params['sujet'];
		  $mail->MsgHTML($params['message']);		  
		  
		  if(!empty($params['attachments'])){
			  foreach($params['attachments'] as $k => $v){
			  	$mail->AddAttachment($v['path'], $v['name'],'base64',$v['mime']);
			  }
		  }
		  
		  if(!empty($params['to'])){
			  $mail->ClearAddresses();
			  $mail->AddAddress($params['to'][0], $params['to'][1]);
			  $mail->Send();
		  }
		  
		  $_SESSION['mailsend'] = "success";
		  $this->sent = true;
		  return true;
		} catch (phpmailerException $e) {
			$_SESSION['mailsend'] = "error";
			$this->sent = false;
			return false;
		} catch (Exception $e) {
			$_SESSION['mailsend'] = "error";
			$this->sent = false;
			return false;
		}
	}
	
	public function set_template($tmpl,$vars){
		$str = $tmpl;
		foreach($vars as $k => $v){
			$str = preg_replace('/'.preg_quote('|*'.$k.'*|').'/',$v,$str);
		}
		return $str;
	}
	
	public function message($params = array()){
		if($_SESSION['mailsend']){
			$return = '<p class="mail-message mail-message-'.$_SESSION['mailsend'].'">'.$_REQUEST['msg'].'</p>';
			$_SESSION['mailsend'] = '';
			return $return;
		}
	}

}

?>