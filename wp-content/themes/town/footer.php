<?php $bp_myOptions = get_option('bp_framework'); ?>



<?php // If Twitter Switch is active
if(!empty($bp_myOptions['switch_twitter_footer']) && $bp_myOptions['switch_twitter_footer'] == '1') { ?>

  <!-- *** TWEET FEED START *** -->
<!-- <div id="tweet_feed">
	<div class="container">
		<div class="row">
			<div class="span12">
				<div id="twitterfeed"></div>
			</div>
		</div>
	</div>
</div> -->
<!-- *** TWEET FEED END *** -->


   
<?php }?>


<!--FOOTER START -->
<footer id="footer">
	<div class="container" id="<?php echo (ICL_LANGUAGE_CODE=='fr'?'liste-denvoi':'newsletter')?>">
		<div class="row">
			<div class="footer-content">
			<p>
			<?php  
            if(!empty($bp_myOptions['footer_content'])) { ?>
                     <?php echo $bp_myOptions['footer_content']; ?>
            <?php }?>
            </p>
        	<p><a href="https://www.facebook.com/choosetolearn?fref=ts" target="_blank"><img src="/wp/wp-content/themes/town/images/facebook_32.png" alt="Facebook" /></a>&nbsp;&nbsp;&nbsp;<a href="http://ca.linkedin.com/pub/caroline-hui/40/73a/61/" target="_blank"><img src="/wp/wp-content/themes/town/images/linkedin_32.png" alt="Linked In" /> </a></p>
			</div>	 
				
		<div class="mailchimp-form">
        	<!-- Begin MailChimp Signup Form -->
            <link href="//cdn-images.mailchimp.com/embedcode/classic-081711.css" rel="stylesheet" type="text/css">
            <style type="text/css">
                #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif;  width:100%;}
				#mc_embed_signup .button {
				  background-color: #93a9bb;
				font-family: 'Open Sans',sans-serif;
				font-size: 18px;
				font-weight: 300;
				letter-spacing: normal;
				}
				
                /* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
                   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
            </style>
            <div id="mc_embed_signup">
            <form action="//choosetolearn.us3.list-manage.com/subscribe/post?u=35ba49771a73abe61ddf1eeea&amp;id=53e1cce9c2" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                <h2><?php _e('Subscribe to our mailing list', 'my_theme'); ?></h2>
            <div class="indicates-required"><span class="asterisk">*</span> <?php _e('indicates required', 'my_theme'); ?></div>
            <div class="mc-field-group">
                <label for="mce-EMAIL"><?php _e('Email Address', 'my_theme'); ?>  <span class="asterisk">*</span>
            </label>
                <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
            </div>
            <div class="mc-field-group">
                <label for="mce-FNAME"><?php _e('First Name', 'my_theme'); ?> </label>
                <input type="text" value="" name="FNAME" class="" id="mce-FNAME">
            </div>
            <div class="mc-field-group">
                <label for="mce-LNAME"><?php _e('Last Name', 'my_theme'); ?> </label>
                <input type="text" value="" name="LNAME" class="" id="mce-LNAME">
            </div>
            <div class="mc-field-group input-group">
                <strong><?php _e('Consent', 'my_theme'); ?> </strong>
                <ul><li><input type="checkbox" value="1" name="group[7797][1]" id="mce-group[7797]-7797-0"><label for="mce-group[7797]-7797-0"><?php _e('I want to receive news about Choose to Learn', 'my_theme'); ?></label></li>
            </ul>
            </div>
                <div id="mce-responses" class="clear">
                    <div class="response" id="mce-error-response" style="display:none"></div>
                    <div class="response" id="mce-success-response" style="display:none"></div>
                </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                <div style="position: absolute; left: -5000px;"><input type="text" name="b_35ba49771a73abe61ddf1eeea_53e1cce9c2" tabindex="-1" value=""></div>
                <div class="clear"><input type="submit" value="<?php _e('Subscribe', 'my_theme'); ?>" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
            </form>
            </div>
            <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
            <script type='text/javascript'>
            (function($) {
            window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';
            }(jQuery));
            var $mcj = jQuery.noConflict(true);
            </script>
            <!--End mc_embed_signup-->
        </div>	
			
		</div>
	</div>
</footer> 	<!--FOOTER END -->


<div class="loading_icon"></div>

<?php wp_footer(); ?>





</body>
</html>