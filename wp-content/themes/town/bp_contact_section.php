
<?php 
$bp_myOptions = get_option('bp_framework');
$meta = get_post_meta($post->ID,'metas',true);
$thumbnail_object = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large');

?>


<?php	if(!empty($meta['page_title'])) { ?>  
		
		<div id="<?php echo the_slug(); ?>"class="page_title">
			<div class="container">
				<div class="row">
					<div class="span12">
						<h1><?php echo $meta['page_title']; ?></h1>
					</div>
				</div><!--row-->
			</div><!--container-->
		</div><!--page_title-->

<?php }?>

<div <?php if(empty($meta['page_title'])) { ?> id="<?php echo the_slug(); ?>" <?php }?>class="section" <?php if(!empty($thumbnail_object[0])) {?> style="background:url('<?php echo $thumbnail_object[0] ?> ')  <?php }?> ">
	<div class="container">
		<div class="row">

		<h1 class="page_title"><?php the_title(); ?></h1>
        
		<?php the_content(); ?>


		
		<?php if(isset($emailSent) && $emailSent == true) { ?>

				<div class="thanks">
					<h1><?php _e('Thanks', 'my_theme'); ?>, <?php $name;?></h1>
					<p><?php _e('Your email was successfully sent. I will be in touch soon.', 'my_theme'); ?></p>
				</div>

			<?php } else { ?>
				
					<?php if(isset($hasError) || isset($captchaError)) { ?>
						<p class="error"><?php _e('There was an error submitting the form.', 'my_theme'); ?><p>
					<?php } ?>


		
				<div id="response">
					

						<form method="post" class="reply-input" action="<?php the_permalink(); ?>">
                        	<input type="hidden" id="form-error-empty" value="<?php _e('You forgot to enter fill this field.', 'my_theme'); ?>" />
                            <input type="hidden" id="form-error-wrong" value="<?php _e('You entered an invalid value.', 'my_theme'); ?>" />
                            <input type="hidden" id="form-success" value="<?php _e('Your message has been sent successfully, We will contact you shortly.', 'my_theme'); ?>" />
							<div class="span12"><h3><?php _e('Send us a message', 'wp_town'); ?></h3></div>
							<div class="span6">
								

									<div class="input-block">
										<input type="text" name="contactName" id="contactName" class="requiredField name" value="<?php _e('Your name (required)', 'wp_town'); ?><?php if(isset($_POST['contactName'])) echo $_POST['contactName'];?>" onblur="if(this.value=='')this.value='<?php _e('Your name (required)', 'wp_town'); ?>'" onfocus="if(this.value=='<?php _e('Your name (required)', 'wp_town'); ?>')this.value=''"/>

										
									</div>

									<div class="input-block">
										<input type="text" required="" name="email" id="email" class="requiredField email" value="<?php _e('Your email (required)', 'wp_town'); ?><?php if(isset($_POST['email']))  echo $_POST['email'];?>" onblur="if(this.value=='')this.value='<?php _e('Your email (required)', 'wp_town'); ?>'" onfocus="if(this.value=='<?php _e('Your email (required)', 'wp_town'); ?>')this.value=''"/>

										
									</div>

									<div class="input-block">
										<input type="text" required="" name="subject" id="subject"  class="requiredField subject" value="<?php _e('Subject', 'my_theme'); ?><?php if(isset($_POST['subject']))  echo $_POST['subject'];?>" onblur="if(this.value=='')this.value='<?php _e('Subject', 'my_theme'); ?>'" onfocus="if(this.value=='<?php _e('Subject', 'my_theme'); ?>')this.value=''"/>

										
									</div>

							</div><!--span6-->

							<div class="span6">
								<div class="textarea-block">
							<textarea required="" name="comments" id="commentsText" class="textarea" class="requiredField" cols="88" rows="8" onblur="if(this.value=='')this.value='<?php _e('Your message', 'wp_town'); ?>'" 
onfocus="if(this.value=='<?php _e('Your message', 'wp_town'); ?>')this.value=''"><?php _e('Your message', 'wp_town'); ?><?php if(isset($_POST['comments'])) { if(function_exists('stripslashes')) { echo stripslashes($_POST['comments']); } else { echo $_POST['comments']; } } ?></textarea>
								<div class="clear"></div>
							
							</div>
							<li class="buttons"><input type="hidden" name="submitted" id="submitted" class="submit_buttom" value="true" /><input class="submit_buttom" type="submit" id="submitted" value="<?php _e('Submit', 'my_theme'); ?>"></li>

							</div>
							

							

							

							<div class="clear"></div>
						</form>

					</div><!--response-->




			<?php } ?>

			<?php  

				if(!empty($bp_myOptions['contact_sidebar'])) { echo $bp_myOptions['contact_sidebar'];}
					
			 ?>
		</div><!--row-->
		</div><!--container-->
		</div><!--section-->