��    6      �  I   |      �     �     �     �     �     �  	   �     �     �                 
   )     4     :     I  ,   \     �  	   �     �     �     �     �     �     �     �     �          (  
   .     9  
   E     P     V  	   e     o     |     �     �  	   �     �     �  8   �  5   �     2  %   N     t  $   �  "   �  '   �          !  E   .     t  �  �     6
     >
     U
     m
     u
     �
     �
     �
     �
  
   �
     �
     �
  	   �
     �
  
   �
  7        ?     D     H     `     |     �     �  
   �     �     �     �     �     �       
        &     -     @     O     [     a  	   i  
   s  #   ~     �  K   �  D   �     @  &   _     �     �     �  #   �     �          /     N               3   %   +                            
         2       /   -      '   .          5   "   0   #              )      1              6   &          	                4          *           !          $       (             ,                                Address CANCELLATION POLICY CONFERENCE<br>TO COME CONTENT City/Province Comments: Consent Email Email Address Email: FEATURE CONFERENCE First Name GOALS HIDE BIOGRAPHY I WANT TO REGISTER I want to receive news about Choose to Learn LOCATION Last Name NEXT<br>CONFERENCE NO CONFERENCE TO COME NO PREVIOUS CONFERENCE Name Name: Organisation PLACE PREVIOUS<br>CONFERENCES PRINT THIS CONFERENCE Phone Phone Work Postal Code Profession RATES READ BIOGRAPHY READ MORE REGISTRATION Subject Subject: Submit Subscribe Subscribe to our mailing list THEMES Yes, I whish to receive newsletters from Choose To Learn Yes, please add me to Choose to Learn's mailing list. You emailed Choose to learn You entered an invalid email address. You entered an invalid value. You forgot to enter fill this field. You forgot to enter your comments. You forgot to enter your email address. You forgot to enter your name. Your Comment Your message has been sent successfully, We will contact you shortly. indicates required Project-Id-Version: Town
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-02-10 13:06-0500
PO-Revision-Date: 2016-02-10 13:07-0500
Last-Translator: Tommy <info@tsansfacon.com>
Language-Team: 
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Basepath: .
X-Poedit-KeywordsList: _e;_;__
X-Poedit-SourceCharset: utf-8
X-Generator: Poedit 1.7.4
X-Poedit-SearchPath-0: ..
 Adresse POLITIQUE D'ANNULATION CONFÉRENCE<br>À VENIR CONTENU Ville/Province Commentaires : Consentement Courriel Courriel Courriel : CONFÉRENCE EN VEDETTE Prénom OBJECTIFS MASQUER LA BIOGRAPHIE M'INSCRIRE Je désir recevoir des courriels de Choisir d'apprendre LIEU Nom PROCHAINE<br>CONFERENCE AUCUNE CONFÉRENCE À VENIR AUCUNE CONFÉRENCE PRÉCÉDENTE Nom Nom : Entreprise LIEU CONFÉRENCES<br>PRÉCÉDENTES IMPRIMER CETTE CONFÉRENCE Téléphone Téléphone travail Code postal Profession COÛTS LIRE LA BIOGRAPHIE EN SAVOIR PLUS INSCRIPTION Sujet Sujet : Soumettre S'inscrire Abonnez-vous à notre liste d'envoi THÉMATIQUES Oui, je souhaite recevoir des infolettres de la part de Choisir d'apprendre Oui, SVP ajouter mon nom à la liste d'envoi de Choisir d'Apprendre. Message à Choisir d'apprendre Vous devez inscrire uncourriel valide. Ce champ est invalide. Vous devez remplir ce champ. Vous devez inscrire un message. Vous devez inscrire votre courriel. Vous devez inscrire votre nom. Votre commentaire Votre message a été envoyé. champs requis 