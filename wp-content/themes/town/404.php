<!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>

<?php $bp_myOptions = get_option('bp_framework'); ?>

<title><?php wp_title('|', true, 'right');  ?> <?php bloginfo('name') ?> | <?php bloginfo('description') ?></title>

<meta charset="<?php bloginfo( 'charset' ); ?>">

<?php if(isset($bp_myOptions['switch_responsive']) && $bp_myOptions['switch_responsive'] == '1') { ?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<?php } ?>

<link rel="stylesheet" href="<?php get_stylesheet_uri(); ?>">
<link rel="shortcut icon" href="<?php if(!empty($bp_myOptions['upload_favicon'])) {   echo $bp_myOptions['upload_favicon']; }?>">

<?php if(!empty($bp_myOptions['analytics_track'])) { 
  echo $bp_myOptions['analytics_track']; 
}
 ?>


<?php wp_head(); ?>

</head>

<body <?php body_class(''); ?>>

<header id="header">

  <div class="row-fluid">
    <div class="logo_container_fix">

        <?php 
            $header_image ='';
            if(!empty($bp_myOptions['upload_logo'])) { 
            $header_image = $bp_myOptions['upload_logo'];  
            }

            if ( !empty( $header_image ) ) : ?>
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>#home" id="logo"><img src="<?php echo esc_url( $header_image ); ?>" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="" /></a>  
            <?php else : ?>
             <a href="<?php echo esc_url( home_url( '/' ) ); ?>" id="logo_second">Logo</a>
        <?php endif; ?>
        </div><!--logo end-->



   <div class="main_nav_single">
            <ul>
                <li><a href="<?php echo home_url(); ?>"> <?php _e('Go back home', 'wp_town'); ?></a></li>
            </ul><!--End UL--> 
          </div>

    

  </div><!--row-fluid-->
</header><!--header-->

<div class="section">
    <div class="container">
    <div class="item-data">
    <div class="helper">

    <div class="row-fluid">
      <div class="span12">
      	   <div class="errorcontainer">
	       			<h1 class="huge">404</h1>
					<p><strong><?php _e('Oh!', 'wp_town'); ?></strong> <?php _e('The page you were looking for cannot be found!', 'wp_town'); ?></p>
					<p><strong> <?php _e('But do not worry,', 'wp_town'); ?></strong> <?php _e('we have got you covered! :)', 'wp_town'); ?></p>
					<p><a href="<?php echo home_url(); ?>" class="button black"><?php _e('Home page', 'wp_town'); ?></a></p>
				</div>
      </div>

        
    </div><!--item data--> 

  </div><!--helper-->
    </div><!--item-data-->
      
  </div>
  </div><!--section-->
		

<!-- <?php comment_form(); ?> -->
<?php get_footer(); ?>