<?php
/*
Template Name: All Blog Post
*/
?>


<!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>

<?php $bp_myOptions = get_option('bp_framework'); 
$post_per_page_full ='';
if(!empty($bp_myOptions['post_per_page_full'])) { 
	$post_per_page_full = $bp_myOptions['post_per_page_full'];
}
?>
<?php $separate_page = '';?>

<title><?php wp_title('|', true, 'right');  ?> <?php bloginfo('name') ?> | <?php bloginfo('description') ?></title>

<meta charset="<?php bloginfo( 'charset' ); ?>">

<?php if(isset($bp_myOptions['switch_responsive']) && $bp_myOptions['switch_responsive'] == '1') { ?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<?php } ?>

<link rel="stylesheet" href="<?php get_stylesheet_uri(); ?>">
<link rel="shortcut icon" href="<?php if(!empty($bp_myOptions['upload_favicon'])) {   echo $bp_myOptions['upload_favicon']; }?>">

<?php 
  if(!empty($bp_myOptions['analytics_track'])) { 
  echo $bp_myOptions['analytics_track']; 
}

 ?>


<?php

$argss = array(
'post_type' => 'page',
'meta_query' => array(
array(
'key' => 'metas',
'value' => 'blog_full_page', // Value on Meta Box 
'compare' => 'LIKE' 
)
)
);

// The Query
query_posts( $argss );

// The Loop
while ( have_posts() ) : the_post();

$blog_full_id = get_the_ID();

?>

<?php 

endwhile;

// Reset Query
wp_reset_query();
?>

<?php wp_head(); ?>

</head>

<body <?php body_class('gray blog_post'); ?>>

 
    <div id="header">

  <div class="row-fluid">
    <div class="logo_container_fix">

        <?php 
            $header_image ='';
            if(!empty($bp_myOptions['upload_logo'])) { 
            $header_image = $bp_myOptions['upload_logo'];  
            }

            if ( !empty( $header_image ) ) : ?>
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>#home" id="logo"><img src="<?php echo esc_url( $header_image ); ?>" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="" /></a>  
            <?php else : ?>
             <a href="<?php echo esc_url( home_url( '/' ) ); ?>" id="logo_second">Logo</a>
        <?php endif; ?>
        </div><!--logo end-->



   <div class="main_nav_single">
            <ul>
                <li><a href="<?php echo home_url(); ?>"><?php _e('Go back home', 'wp_town'); ?></a></li>
            </ul><!--End UL--> 
          </div>

    

  </div><!--row-fluid-->
</div><!--header-->


    <div class="clear"></div>

 
       


	
<div class="container section gray">

  <div class="row">

      <div class="span12">
          <h1 class="page_title"><strong><?php if(!empty($bp_myOptions['blog_title'])) {   echo $bp_myOptions['blog_title']; } ?></strong></h1>
          <h2 class="page_subtitle"><?php if(!empty($bp_myOptions['blog_subtitle'])) {   echo $bp_myOptions['blog_subtitle']; } ?></h2>
          <div class="page_line"></div>
      </div><!--span12-->


        <div class="clear"></div>
        <div class="divider"></div>



<div class="span8">


<div class="article-container">

<?php

$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

query_posts(array(
	'post_type'      => 'post', // You can add a custom post type if you like
	'paged'          => $paged,
	'posts_per_page' => $post_per_page_full,
));

if ( have_posts() ) : ?>

<?php while ( have_posts() ) : the_post(); ?>

<?php 
$image_attributes = get_post_meta($post->ID, 'post_main_img', true);
$thumbnail_object_blog = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large');
$user_email = get_the_author_meta('user_email'); ?>


      
          <div class="blog-item">
                  <?php if(!empty($thumbnail_object_blog[0])) {?> 


              <div class="img-container-blog" style="background-image: url(<?php echo $thumbnail_object_blog[0] ?>);">
                <div class="the-author-img">
                   <?php echo get_avatar( $user_email, $size = '50'); ?>
                </div>
              </div>


            <?php } ?>
              <div class="blog-boddy">
                <div class="the-title"><h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1></div>
                <p><?php the_excerpt(); ?></p>
                <div class="metas">
                  <div class="the-date"><a href="#"><span class="calendar gray icon"></span><?php the_time('F j, Y'); ?></a></div>
                  <div class="the-comments"><a href="#"><span class="comments gray icon"></span><?php comments_popup_link( '0', '1', '%', 'wp_comments_xiara' )?></a></div>
                  <a href="<?php the_permalink(); ?>" class="read_more_small"><i class="fa_icon icon-plus"></i></a>
                  <div class="clear"></div>
                </div><!--metas-->
              </div>
            </div><!--blog-item-->



<?php endwhile; ?>



<?php else : ?>

<h2>No contenido</h2>
	
<?php endif; ?>


<div class="article-nav clearfix">

	<?php my_pagination(); ?>

</div> <!-- end clearfix -->

</div> <!-- end article-container -->




</div> <!-- end span8 -->

<?php get_sidebar(); ?>

</div> <!-- end row -->

</div> <!-- end container -->


</div><!--section-->

   
         


<!--FOOTER START -->
<div id="footer">
  <div class="container">
    <div class="row">
      
    <?php  
    if(!empty($bp_myOptions['footer_content'])) { ?>
         <?php echo $bp_myOptions['footer_content']; ?>
    <?php }?>
         

    <a class="map" href=" <?php  if(!empty($bp_myOptions['map_url'])) { ?>   <?php echo $bp_myOptions['map_url']; ?><?php }?>" target="_blank" title="Map">

    <?php  if(!empty($bp_myOptions['upload_map_img'])) { ?><div id="map_google"></div> <?php }?>
    
    <div class="clear"></div></a>
        
        
      
    </div>
  </div>
</div>  <!--FOOTER END -->



<div class="loading_icon"></div>

<?php wp_footer(); ?>


</body>
</html>