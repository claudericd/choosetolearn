<?php 


/* INCLUDE OPTIONS & SHORTCODES */

require_once(TEMPLATEPATH . '/functions/general_options.php');
require_once(TEMPLATEPATH . '/functions/shortcodes.php');
require_once(TEMPLATEPATH . '/portfolio-post-type.php');

// Theme option 

get_template_part('/functions/theme', 'options');

add_action('after_setup_theme', 'my_theme_setup');
function my_theme_setup(){
    load_theme_textdomain('my_theme', get_template_directory() . '/languages_bak');
}


function get_conferenciers($atts){
	$childargs = array(
	'post_type' => 'conferenciers',
	'numberposts' => -1,
	'orderby' => 'title',
	'order' => 'ASC',
	'post__in' => explode(',',$atts['id'])
	);
	$child_posts = get_posts($childargs);
	//echo $atts['id'];
	foreach ( $child_posts as $conferencier ){
		$meta_conferencier = get_post_custom($conferencier->ID);
		$echo .= '<div class="conference-description"><div class="conferencier">';
		$echo .= '<img src="'.$meta_conferencier['wpcf-photo-du-conferencier'][0].'"/>';
		$echo .= '<div class="conferencier-text"><h3>'.$conferencier->post_title.'</h3><span class="more">'.__('READ BIOGRAPHY', 'my_theme').'</span><span class="more" style="display:none;">'.__('HIDE BIOGRAPHY', 'my_theme').'</span><div>'.nl2br($conferencier->post_content).'</div></div>';
		$echo .= '</div></div>';
	}
	return $echo;
}
add_shortcode('conferenciers', 'get_conferenciers'); 


function my_connection_types() {
    p2p_register_connection_type( array(
        'name' => 'posts_to_pages',
        'from' => 'conferenciers',
        'to' => 'conferences'
    ) );
}
add_action( 'p2p_init', 'my_connection_types' );


function remove_core_updates(){
global $wp_version;return(object) array('last_checked'=> time(),'version_checked'=> $wp_version,);
}
add_filter('pre_site_transient_update_core','remove_core_updates');
add_filter('pre_site_transient_update_plugins','remove_core_updates');
add_filter('pre_site_transient_update_themes','remove_core_updates');


?>