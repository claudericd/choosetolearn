<?php
/*
Template Name: Index
*/
?>
<?php get_header();?> 
<?php
$args=array(
    'post_type' => 'page',
    'order' => 'ASC',
    'orderby' => 'menu_order',
    'posts_per_page' => '-1',
	'post__not_in' => array(267,264,272,274),
    'meta_query' => array(
						array(
						'key' => 'separate',
						'compare' => 'NOT EXISTS' 
						)
	)
);
$main_query = new WP_Query($args); 



if( have_posts() ) : 

/* Start the Loop */ 
while ($main_query->have_posts()) : $main_query->the_post();

$background=get_post_meta($post->ID, 'background', true);

$meta = get_post_meta($post->ID,'metas',true);

$home ='';
if(!empty($meta['name'])) { 
	$home = ($meta['name'] == 'home');
}

$contact ='';
if(!empty($meta['name'])) { 
	$contact = $meta['name'] == 'contact';
}

$portfolio ='';
if(!empty($meta['name'])) { 
	$portfolio = $meta['name'] == 'portfolio';
}

$temoignages ='';
if(!empty($meta['name'])) { 
	$temoignages = $meta['name'] == 'temoignages';
}

$about ='';
if(!empty($meta['name'])) { 
	$about = $meta['name'] == 'about';
}

global $cslug;
$cslug = the_slug();
$conferences ='';
if(!empty($meta['name'])) { 
	$conferences = $meta['name'] == 'conferences';
}

$home_video ='';
if(!empty($meta['name'])) { 
	$home_video = $meta['name'] == 'home_video';
}

$blog ='';
if(!empty($meta['name'])) { 
	$blog = $meta['name'] == 'blog';
}

$parallax ='';
if(!empty($meta['name'])) { 
	$parallax = $meta['name'] == 'parallax';
}
$welcome = '';
if(!empty($meta['name'])) { 
	$welcome = $meta['name'] == 'welcome';
}


$thumbnail_object = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full');
if ( ! isset( $content_width ) ) 
    $content_width = 1170;
?>

<?php $bp_myOptions = get_option('bp_framework'); ?>
<?php add_theme_support( 'automatic-feed-links' ); ?>

	 <?php if ( $home ) : ?>

        <div  id="<?php echo the_slug(); ?>" <?php if(!empty($thumbnail_object[0])) {?> style="background:url('<?php echo $thumbnail_object[0] ?> ')  <?php }?> ">
			
		<?php the_content(); ?>
			
		</div>  

	    <?php elseif ($welcome):?>
			
			<?php get_template_part('bp_welcome'); ?>     

		<?php elseif ($home_video):?>
			
			<?php get_template_part('bp_home_video'); ?>   
		
		<?php elseif ($temoignages):?>
			
			<?php get_template_part('bp_temoignages'); ?> 
            
        <?php elseif ($about):?>
			
			<?php get_template_part('bp_about'); ?> 
            
        <?php elseif ($conferences):?>
			
			<?php get_template_part('bp_conferences'); ?>    			

        <?php elseif ($portfolio): ?> 

        			<?php get_template_part('bp_portfolio_section'); ?>

        <?php elseif ( $blog ) : ?>
                   
        			<?php get_template_part('bp_blog_section'); ?>

        <?php elseif ( $parallax ) : ?>
                   
        			<?php get_template_part('bp_parallax_section'); ?>

   		<?php elseif ( $contact ) : ?>


   <?php if($bp_myOptions['switch_captcha'] == '0') { ?>
       
		<?php get_template_part('bp_contact_section'); ?>

	<?php } ?>

<?php if($bp_myOptions['switch_captcha'] == '1') { ?>
       
		<?php get_template_part('bp_contact_captcha'); ?>

	<?php } ?>
       				
  
    


    <?php else : ?>

    
	
			
		<div id="<?php echo the_slug(); ?>" class="section" <?php if(!empty($thumbnail_object[0])) {?> style="background:url('<?php echo $thumbnail_object[0] ?> ')  <?php }?> ">
			<div class="container">
				<div class="row">
					<?php the_content(); ?> 
				</div>
			</div>
			
		</div>            
            
        <?php endif; ?>

	<div class="clear"></div>

<?php endwhile; ?>


<?php else : ?>

		<div  id="<?php echo the_slug(); ?>" class="section" <?php if(!empty($thumbnail_object[0])) {?> style="background:url('<?php echo $thumbnail_object[0] ?> ')  <?php }?> ">
			<div class="container">
				<div class="row">
					
			<?php if ( current_user_can( 'edit_posts' ) ) :
				// Show a different message to a logged-in user who can add posts.
			?>
					<h4 class="entry-title"><?php _e( 'No posts to display', 'wp_town' ); ?></h1>

			<?php else :
				// Show the default message to everyone else.
			?>
				<header class="entry-header">
					<h1 class="entry-title"><?php _e( 'Nothing Found', 'wp_town' ); ?></h1>
				</header>

				<div class="entry-content">
					<p><?php _e( 'Apologies, but no results were found. Perhaps searching will help find a related post.', 'wp_town' ); ?></p>
					<?php get_sidebar(); ?>
				</div><!-- .entry-content -->
			<?php endif; // end current_user_can() check ?>				</div>
			</div>
		</div>  

<?php endif; // end have_posts() check ?>

<?php get_footer(); ?>