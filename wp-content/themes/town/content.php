<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>
	
<?php 
$image_attributes = get_post_meta($post->ID, 'post_main_img', true);
$thumbnail_object_blog = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large');
$user_email = get_the_author_meta('user_email');
 ?>
	
<div id="post-<?php the_ID(); ?>" <?php post_class('blog-item'); ?>>
	<div class="blog-item">
          
              <?php if(!empty($thumbnail_object_blog[0])) {?> 


              <div class="img-container-blog" style="background-image: url(<?php echo $thumbnail_object_blog[0] ?>);">
                <div class="the-author-img">
                   <?php echo get_avatar( $user_email, $size = '50'); ?>
                </div>
              </div>


            <?php } ?>
            <div class="blog-boddy">
              <div class="the-title"><h1><a href="<?php the_permalink(); ?>"><?php the_title();?> </a></h1></div>
             
                <?php the_content(); ?>


              <div class="metas">
                  <div class="the-date"><a href="#"><span class="calendar gray icon"></span><?php the_time('F j, Y'); ?></a></div>
                  <div class="the-comments"><a href="#"><span class="comments gray icon"></span><?php comments_popup_link( '0', '1', '%', 'wp_comments_xiara' )?></a></div>
                  <div class="clear"></div>
                </div><!--metas-->
            </div>
          </div>  

      </div>
	
