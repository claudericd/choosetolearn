<?php
get_header(); ?>


<?php $meta = get_post_meta($post->ID,'metas',true); ?>

<div class="clear" style="padding-bottom:110px"></div>
   <?php if(!empty($meta['page_title'])) { ?>  
				
				<div id="<?php echo the_slug(); ?>"class="page_title">
					<div class="container">
						<div class="row">
							<div class="span12">
								<h1><?php echo $meta['page_title']; ?></h1>
							</div>
						</div><!--row-->
					</div><!--container-->
				</div><!--page_title-->
		<?php }?>

<div class="container">
	<div class="row">
    <?php if (have_posts()) : while (have_posts()) : the_post();?>
   <?php the_content();?>
   <div class="clear"></div>
    <?php endwhile; endif; ?>
<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>
</div>
</div>

<?php get_footer(); ?>