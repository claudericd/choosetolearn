jQuery(document).ready(function() {
	jQuery('form.reply-input').submit(function() {
		jQuery('form.reply-input .error').remove();
		var hasError = false;
		jQuery('.requiredField').each(function() {
			if(jQuery.trim(jQuery(this).val()) == '') {
				var labelText = jQuery(this).next('label').text();
				jQuery(this).parent().append('<span class="error">'+jQuery('form.reply-input #form-error-empty').val()+'</span>');
				hasError = true;
			} else if(jQuery(this).hasClass('email')) {
				var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
				if(!emailReg.test(jQuery.trim(jQuery(this).val()))) {
					var labelText = jQuery(this).next('label').text();
					jQuery(this).parent().append('<span class="error">'+jQuery('form.reply-input #form-error-wrong').val()+'</span>');
					hasError = true;
				}
			}
		});
		if(!hasError) {
			
			var formInput = jQuery(this).serialize();
			jQuery.post(jQuery(this).attr('action'),formInput, function(data){
				alert(jQuery('form.reply-input #form-success').val());
				window.location.href="";
			});
		}
		
		return false;
		
	});
});