/*global jQuery:false */

jQuery(document).ready(function(){

  _CHOOSETOLEARN.init();
  
  jQuery(window).resize(function() {
	  _CHOOSETOLEARN.resize();
  });
  _CHOOSETOLEARN.resize();

});

var _CHOOSETOLEARN = {
	
	ismobile: false,
	
	init: function(){
		var hash = window.location.hash;
		if(jQuery(hash).length>0){
			/*jQuery('.conferences-content').hide();
			jQuery(hash).show();
			jQuery('.main_nav a').each(function(){
				var th = jQuery(this).attr('href');
				if(th==window.location.hash){
					jQuery(this).trigger('click');
					return;
				}
			});*/
		}
		
		jQuery('.rev_slider .line img').each(function(){
			var src = jQuery(this).attr('src');
			src = src.replace(/pointome\.net/,'ca');
			jQuery(this).attr('src',src);
		});
		
		
		var h = jQuery('a.conference-active').height();
		jQuery('a.conference-active').removeClass('conference-active');
		jQuery('a.conference-box').attr('data-height',h);
		jQuery( "<style>a.conference-box:hover,a.conference-active{height:"+h+"px;}</style>" ).appendTo("head");
		
		jQuery('a.conference-box').click(function(){
			var isactive = jQuery(this).hasClass('conference-active');
			if(isactive){
				jQuery(this).removeClass('conference-active');
				jQuery('.conferences-contents-holder').hide();
				jQuery('.conferences-content').hide();
			}else{
				jQuery('a.conference-box').removeClass('conference-active');
				jQuery(this).addClass('conference-active');
				
				jQuery('.conferences-contents-holder').hide();
				jQuery('.conferences-content').hide();
				jQuery('div.'+jQuery(this).attr('id')).show();
				jQuery('.conferences-contents-holder').show();
			}
		});
		jQuery('.conferencier-text .more').click(function(){
			jQuery(this).parent().find('.more').show();
			//jQuery('.conferencier-text .more:eq(0)').show();
			//jQuery('.conferencier-text .more:eq(1)').hide();
			jQuery(this).hide();
			var div = jQuery(this).parent().find('div');
			var isactive = div.is(':hidden');
			if(isactive){
				jQuery('.conferencier-text div').hide();
				div.show();
			}else{
				div.hide();
			}
		});
		jQuery('.conference-archived').click(function(){
			var id = jQuery(this).attr('id');
			var isclose = jQuery('.'+id).is(':hidden');
			jQuery('.conference-archived-info').hide();
			if(isclose){
				jQuery('.'+id).show();
			}
			
		});
		_CHOOSETOLEARN._TEMOIGNAGES.init();
	},
	
	resize: function(){
		_CHOOSETOLEARN.ismobile = jQuery('#header').width()<600;
		
		if(_CHOOSETOLEARN.ismobile){
			_CHOOSETOLEARN._TEMOIGNAGES.max = Math.ceil(jQuery('.temoignages-slider p').length)-1;
		}
	},
	
	_FORM: {
		
		errors: false,
	
		validate: function(form){
		
			jQuery(form).find('input.require').each(function(){
				jQuery(this).removeClass('error');
				var value = jQuery(this).val();
				if(value==''||value==jQuery(this).attr('alt')){
					jQuery(this).addClass('error');
				}else{
					jQuery(this).removeClass('error');
				}
			});
			jQuery(form).find('input.email').each(function(){
				var value = jQuery(this).val();
				var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
				if(!emailReg.test(jQuery.trim(value))){
					jQuery(this).addClass('error');
				}else{
					jQuery(this).removeClass('error');
				}
			});
			
			var c = jQuery(form).find('input[name="amount_1"]:checked').length;
			if(c==0){
				jQuery(form).find('div.amounts').addClass('error');
				jQuery(form).find('div.amounts input').addClass('error');
			}else{
				jQuery(form).find('div.amounts').removeClass('error');
				jQuery(form).find('div.amounts input').removeClass('error');
			}
			
			var errors = jQuery(form).find('input.error').length;
			if(errors>0){
				alert(jQuery(form).find('#form-error').val());
				jQuery(form).attr('ACTION','');
				return false;
			}
			
			jQuery(form).find('input[alt]').each(function(){
				var value = jQuery(this).val();
				if(value==jQuery(this).attr('alt')){
					jQuery(this).val('');
				}
			});
			
			jQuery(form).attr('target','_blank');
			jQuery(form).attr('ACTION','/wp/wp-content/themes/town/php/paypal.php?act=paypal_checkout');
			
			return true;
		}
	
	},
	
	
	print: function(obj){
		jQuery('.print').removeClass('print');
		jQuery('.print-main').removeClass('print-main');
		jQuery(obj).addClass('print-main');
		jQuery(obj).parents().addClass('print');
		window.print();
	},
	
	_TEMOIGNAGES: {
		
		current: 0,
		cid: false,
		height: 0,
		max: 0,
		
		init: function(){
			_CHOOSETOLEARN._TEMOIGNAGES.current = 0;
			_CHOOSETOLEARN._TEMOIGNAGES.max = Math.ceil(jQuery('.temoignages-slider p').length/3)-1;
			jQuery('a.next').click(function(){
				_CHOOSETOLEARN._TEMOIGNAGES.next();
			});
			jQuery('a.prev').click(function(){
				_CHOOSETOLEARN._TEMOIGNAGES.prev();
			});
			jQuery('.temoignage').each(function(){
				var h = jQuery(this).height();
				_CHOOSETOLEARN._TEMOIGNAGES.height = h>_CHOOSETOLEARN._TEMOIGNAGES.height ? h : _CHOOSETOLEARN._TEMOIGNAGES.height;
			});
			jQuery('.temoignages-slider-holder').height(_CHOOSETOLEARN._TEMOIGNAGES.height);
			jQuery('.temoignages-slider p').height(_CHOOSETOLEARN._TEMOIGNAGES.height);
			_CHOOSETOLEARN.log(_CHOOSETOLEARN._TEMOIGNAGES.current+', '+_CHOOSETOLEARN._TEMOIGNAGES.max);
		},
	
		stop: function(){
			jQuery('.temoignages-slider').stop();
		},
		
		next: function(){
			if(_CHOOSETOLEARN._TEMOIGNAGES.current==_CHOOSETOLEARN._TEMOIGNAGES.max){
				_CHOOSETOLEARN._TEMOIGNAGES.current = 0;
				var y = _CHOOSETOLEARN._TEMOIGNAGES.current*jQuery('.temoignages-slider-holder').height();
				jQuery('.temoignages-slider').animate({marginTop:-(y)});
				return false;
			}
			_CHOOSETOLEARN.log('next');
			_CHOOSETOLEARN._TEMOIGNAGES.current = _CHOOSETOLEARN._TEMOIGNAGES.current+1;
			var y = _CHOOSETOLEARN._TEMOIGNAGES.current*jQuery('.temoignages-slider-holder').height();
			jQuery('.temoignages-slider').animate({marginTop:-(y)});
		},
		
		prev: function(){
			if(_CHOOSETOLEARN._TEMOIGNAGES.current==0){
				_CHOOSETOLEARN._TEMOIGNAGES.current = _CHOOSETOLEARN._TEMOIGNAGES.max
				var y = _CHOOSETOLEARN._TEMOIGNAGES.current*jQuery('.temoignages-slider-holder').height();
				jQuery('.temoignages-slider').animate({marginTop:-(y)});
				return false;
			}
			_CHOOSETOLEARN._TEMOIGNAGES.current = _CHOOSETOLEARN._TEMOIGNAGES.current-1;
			var y = _CHOOSETOLEARN._TEMOIGNAGES.current*jQuery('.temoignages-slider-holder').height();
			jQuery('.temoignages-slider').animate({marginTop:-(y)});
		},
		
		loading: function(){
			jQuery('.loading').show();
		},
		
		loading_complete: function(){
			jQuery('.loading').hide();
			_CHOOSETOLEARN._TEMOIGNAGES.cid = false;
		}
	
	},

	log: function(msg){
		if(typeof(console)!='undefined'){
			console.log(msg);
		}
	}
	

}
