/*global $:false */

jQuery(document).ready(function(){

  jQuery( 'a[ title="link" ]' ).addClass("external");

});

 jQuery(document).ready(function(){
 "use strict";
  
  jQuery('.home .main_nav').onePageNav({
    currentClass: 'active',
    changeHash: false,
    scrollSpeed: 750,
    scrollOffset: 40,
      filter: ':not(.external)',
    easing: 'swing',
});



jQuery('.logo_container').onePageNav({
    changeHash: false,
    scrollSpeed: 750,
    scrollOffset: 85,
    filter: ':not(.external)',
    easing: 'swing',
});


jQuery('.home .responsive_nav').onePageNav({
    changeHash: false,
    scrollSpeed: 750,
    scrollOffset: 0,
      filter: ':not(.external)',
    easing: 'swing',
});
  
});




 jQuery(window).load(function(){
  "use strict";

  jQuery('.custom_button').onePageNav({
    changeHash: true,
    scrollSpeed: 750,
    scrollOffset: 40,
    filter: '',
    easing: 'swing',
});
});


//CAROUSEL SLIDER

jQuery(document).ready(function() {
jQuery('.slidewrap').carousel({
  slider: '.slider',
  slide: '.slide',
  slideHed: '.slidehed',
  nextSlide : '.next_slide',
  prevSlide : '.prev_slide',
  addPagination: false,
  addNav : true,
  speed: 500 // ms.
});

jQuery('.slidewrap2').carousel({
  slider: '.slider',
  slide: '.slide',
  slideHed: '.slidehed',
  nextSlide : '.next_slide',
  prevSlide : '.prev_slide',
  addPagination: false,
  addNav : true,
  speed: 500 // ms.
});

jQuery('.slidewrap3').carousel({
  slider: '.slider',
  slide: '.slide',
  slideHed: '.slidehed',
  nextSlide : '.next_slide',
  prevSlide : '.prev_slide',
  addPagination: false,
  addNav : true,
  speed: 500 // ms.
});
jQuery('.slidewrap4').carousel({
  slider: '.slider',
  slide: '.slide',
  slideHed: '.slidehed',
  nextSlide : '.next_slide',
  prevSlide : '.prev_slide',
  addPagination: false,
  addNav : true,
  speed: 500 // ms.
});
jQuery('.slidewrap5').carousel({
  slider: '.slider',
  slide: '.slide',
  slideHed: '.slidehed',
  nextSlide : '.next_slide',
  prevSlide : '.prev_slide',
  addPagination: false,
  addNav : true,
  speed: 500 // ms.
});
jQuery('.slidewrap6').carousel({
  slider: '.slider',
  slide: '.slide',
  slideHed: '.slidehed',
  nextSlide : '.next_slide',
  prevSlide : '.prev_slide',
  addPagination: false,
  addNav : true,
  speed: 500 // ms.
});
jQuery('.slidewrap7').carousel({
  slider: '.slider',
  slide: '.slide',
  slideHed: '.slidehed',
  nextSlide : '.next_slide',
  prevSlide : '.prev_slide',
  addPagination: false,
  addNav : true,
  speed: 500 // ms.
});
});



jQuery(document).ready(function() {
    "use strict";
    // cache container
    var jQuerycontainer = jQuery('#container_portfolio');


    
        // initialize isotope
         jQuerycontainer.isotope({
          masonry: {
            columnWidth: 1
          }
        });


       // filter items when filter link is clicked
        jQuery('#filters a').click(function(){
          var selector = jQuery(this).attr('data-filter');
          jQuerycontainer.isotope({ filter: selector });
          return false;
        });
         
 });





jQuery(document).ready(function() {
   "use strict";
      jQuery('#filters li>a').click(function() {
   jQuery('#filters li').removeClass('active');
      jQuery(this).parent().addClass('active');
      });
});


//AJAX PORTFOLIO

  jQuery(window).load(function(){
  
  (function(){
  
    var container = jQuery( "#portfoliod" );
    var jQueryitems = jQuery('#container_portfolio > div.portfolio_item');
     index = jQueryitems.length;
      jQuery('#container_portfolio .portfolio_item').click(function(){

      if (jQuery(this).hasClass('active')){
      } else 
      { lastIndex = index;
      index = jQuery(this).index();
      jQueryitems.removeClass('active');
      jQuery(this).addClass('active');

      var myUrl = jQuery(this).find('.ajax-trigger').attr("href") + " .item-data"; 

      /*jQuery('html, body').animate({scrollTop:20}, 800)*/ //Use this to scroll to the 20px top of the page
      jQuery('html, body').animate({ scrollTop: jQuery(".portfolio-top").offset().top - 50
      }, 400); //Custom scroll to .portfolio-top div 
      jQuery('.loading_icon').css({ "display": "block", "opacity": "0" }).animate({ "opacity": "0.8" }, 300);
      jQuery('#portfolioData').animate({opacity:0}, 400,function(){  
        jQuery("#portfolioData").load(myUrl,function(){   //FIX THE HEIGHT BUG ON FLEXSLIDER
            var jQueryhelper = jQuery('.helper');
            var height = jQueryhelper.height();
            jQuery('#portfolioData').css("min-height", height);

              jQuery('.rev_slider').show().revolution();
        });
        jQuery('#portfolioData').animate({opacity:1}, 400);

      });
      
      //SLIDE UP
        
      jQuery('#portfoliod').slideUp(400, function(){
        jQuery('.loading_icon').delay(800).animate({ "opacity": "0" }, 100,function(){
          jQuery('.loading_icon').css("display","none");
        });
        jQuery('#portfolioData').css('visibility', 'visible');}).delay(800).slideDown(400,function(){
          jQuery('#portfolioData').animate({opacity:1}, 400);
          jQuery("a[class^='prettyPhoto']").prettyPhoto();

      });


      }

      return false;       
      
      });

    //AJAX CLOSE 
    jQuery('#ajax_close').click(function() {
      jQuery('#portfoliod').slideUp(400);
      jQueryitems.removeClass('benext beprev active') ;
      return false;
        });

  })();
  
});




// RESPONSIVE MENU 

jQuery(document).ready(function(){
"use strict";  
var menu_trigger = jQuery('.menu_trigger');
var areamenu = jQuery('.mobileAreaMenu');
var all_li =jQuery('.mobileAreaMenu li');

jQuery(menu_trigger).click(function() {


if (jQuery(menu_trigger).hasClass('active')){

      jQuery(areamenu).slideUp(200,function(){
        jQuery(areamenu).css("display","");
        jQuery(menu_trigger).removeClass("active");
      });
          
      } else {
        jQuery(areamenu).slideDown(200,function(){
          //
        });

        jQuery(menu_trigger).addClass("active");
      }
});

jQuery('.mobileAreaMenu a').click(function(){
  
  jQuery(areamenu).slideUp(200);
  jQuery(menu_trigger).removeClass("active");
});
});


/* Pretty Photo */

jQuery(window).load(function(){
 "use strict";
    jQuery("a[class^='prettyPhoto']").prettyPhoto();
  });


jQuery(window).load(function() { 
"use strict";
jQuery('#filters li.active a').trigger('click');

}); 

jQuery(document).ready(function() {
"use strict";  
var bars = jQuery('.bar');

var bar_one = jQuery(bars).slice(0,1);
var bar_one_percent = jQuery(bar_one).find('.percent').text();

var bar_two = jQuery(bars).slice(1,2);
var bar_two_percent = jQuery(bar_two).find('.percent').text();

var bar_three = jQuery(bars).slice(2,3);
var bar_three_percent = jQuery(bar_three).find('.percent').text();

var bar_four = jQuery(bars).slice(3,4);
var bar_four_percent = jQuery(bar_four).find('.percent').text();

var bar_five = jQuery(bars).slice(4,5);
var bar_five_percent = jQuery(bar_five).find('.percent').text();

var bar_six = jQuery(bars).slice(5,6);
var bar_six_percent = jQuery(bar_six).find('.percent').text();

var bar_seven = jQuery(bars).slice(5,6);
var bar_seven_percent = jQuery(bar_seven).find('.percent').text();

setTimeout(
  function() 
  {

  jQuery(bar_one).animate({width:bar_one_percent}, 900);
  jQuery(bar_two).animate({width:bar_two_percent}, 900);
  jQuery(bar_three).animate({width:bar_three_percent}, 900);
  jQuery(bar_four).animate({width:bar_four_percent}, 900);
  jQuery(bar_five).animate({width:bar_five_percent}, 900);
  jQuery(bar_six).animate({width:bar_six_percent}, 900);
  jQuery(bar_seven).animate({width:bar_seven_percent}, 900);


  }, 800);

});





/* Testimonials */


jQuery(document).ready(function() {
"use strict";
var alltabs = jQuery('.col_wrapper');

jQuery(".testimonial").hide(); 
jQuery(".testimonial:first").show(); 
  jQuery(".testi_column a").click(function() {

    jQuery(alltabs).removeClass("active");
    jQuery(this).parent('.col_wrapper').addClass("active");
    jQuery(".testimonial").hide(); 

    var activeTab = jQuery(this).attr("href"); 

    jQuery(activeTab).fadeIn(600); 

    return false;

  });

});



/* Twitter Fade */

jQuery(document).ready(function() {
"use strict";
var all_tweets = jQuery('.tweet_list li');
var all_tweets_first = jQuery('.tweet_list li:first-child');
var n_tweeets = jQuery(all_tweets).length;
var i = 2;
var first_slice = 1;
var second_slice = 2;
var time = 5000;
jQuery(all_tweets).hide();
jQuery(all_tweets_first).show();

function tweet_loop() {
    setInterval(function() { // this code is executed every 5000 milliseconds (var time = 5000;)

if( i <= n_tweeets ){
jQuery(all_tweets).fadeOut(800);
jQuery(all_tweets).slice(first_slice,second_slice).delay(950).fadeIn(800);

       i++;
       first_slice ++;
       second_slice++;

} //enf i <= n_tweeets
else {
       i= 1;
      first_slice = 0;
      second_slice = 1;
    }

}, time);
}
jQuery(tweet_loop);
});





 jQuery(document).ready(function() {
  
"use strict";
var height_video = jQuery(window).width();
var height_responsive = (height_video / 1.79011) + 1;
jQuery('.video_slide').css("height",height_responsive);

});


jQuery(window).resize(function() {
  "use strict";
var height_video = jQuery(window).width();
var height_responsive = (height_video / 1.79011) + 1;
jQuery('.video_slide').css("height",height_responsive);

});





// HEIGHT FOOTER RESPONSIVE

jQuery(document).ready(function() {
  
"use strict";
var height_map = jQuery('#map_google').height();
var window_with = jQuery(window).width();
if ( window_with > 767) {
    jQuery('#footer').removeClass("footer_mobile").css("height",height_map);
}
else {
   jQuery('#footer').addClass("footer_mobile").css("height","");
}

});



jQuery(window).resize(function() {
  
"use strict";
var height_map = jQuery('#map_google').height();
var window_with = jQuery(window).width();
if ( window_with > 767) {
    jQuery('#footer').removeClass("footer_mobile").css("height",height_map);
   
}
else {
   jQuery('#footer').addClass("footer_mobile").css("height","");
}

});





// MOVING ARROWS IN PARALLAX FOR CAROUSEL

// jQuery(window).load(function() {

// var parallax = jQuery('.parallax');
// var section = jQuery('.section');

// jQuery(parallax).each(function() {

//   var current_slide = jQuery(this).find('.slidecontrols');
   
//    jQuery(this).append(current_slide)
//    });


// jQuery(section).each(function() {

//   var current_slide = jQuery(this).find('.slidecontrols');
   
//    jQuery(this).append(current_slide)
//    });

// });



